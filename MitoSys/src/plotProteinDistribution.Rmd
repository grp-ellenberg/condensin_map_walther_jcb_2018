---
title: "Protein distribution pre and post NEBD"
subtitle: "Walther at al. 2018"
author: 
  - name: Antonio Politi,  Nike Walther, EMBL Heidelberg
output: html_notebook
---
```{r setup}
# package loads 
library(ggplot2)
library(reshape)
library(cowplot)
library(RColorBrewer)
library(mitosysproteindist)
library(knitr)
outputdir <- '../results'
outputdirpng <- file.path(outputdir, 'png')
outputdirpdf <- file.path(outputdir, 'pdf')
if (!dir.exists(outputdir))
  dir.create(outputdir, recursive = TRUE)
if (!dir.exists(outputdirpng))
  dir.create(outputdirpng, recursive = TRUE)
if (!dir.exists(outputdirpdf))
  dir.create(outputdirpdf, recursive = TRUE)

# List of stage names for MST as obtained by time alignment
MST_phases <- mitosysproteindist::getMSTphases(resamplegrays = FALSE)
VVoxel_ <- 0.2516*0.2516*0.75;
Na <-  0.6022140 # in nM concentration

# Name mapping
poiNamePaper <- list(NCAPD3 = 'CAP-D3', NCAPD2 = 'CAP-D2', NCAPH = 'CAP-H', NCAPH2 = 'CAP-H2')
levPOI <- c('SMC4',  'NCAPD3', 'NCAPH2', 'NCAPD2', 'NCAPH')
colorPOIs <- data.frame(POI = c('NCAPD2', 'NCAPH', 'NCAPD3', 'NCAPH2', 'SMC4'), color =
                          c('magenta', 'lightpink', 'springgreen4' , 'limegreen' ,'gray40') )

colorUse <- list()
for (poi in levPOI) {
  colorUse <- cbind(colorUse, as.character(colorPOIs[colorPOIs$POI == poi, ]$color[1]))
}
```


## Data loading

```{r load data, cache = TRUE}
protD <- read.csv('../data/proteinDistribution_180103_condensins.txt', sep ='\t')
POI_annotation <-  read.csv('../data/POI_annotation_database.txt', sep ='\t')
protD$POI_name <- ''
# find border of stages
stgBorder <- mitosysproteindist::find_mststageborder(df_protD =  protD, MST_phases = MST_phases)
#only use specific clones
protD <- protD[(protD$POI %in% c('SMC4gfpz82z68', 'NCAPD2gfpc272c78',  'NCAPHgfpc86', 'NCAPD3gfpc16', 'NCAPH2gfpc67')), ]
protD$POI <- factor(protD$POI, levels = c('SMC4gfpz82z68', 'NCAPD2gfpc272c78',  'NCAPHgfpc86', 'NCAPD3gfpc16', 'NCAPH2gfpc67'))

# set a short name corresponding to protein this will merge clones of the same protein
for (i in seq(1,dim(POI_annotation)[1])) {
  protD$POI_name[protD$POI == as.character(POI_annotation$poi_name[i])] <- as.character(POI_annotation$shortname[i])
}

protD$POI <- protD$POI_name
protD_ori <- protD
# remove a data set for NCAPD2 that shows a negative value in the inferred chromatin data 
# '160720_2_7_28_16'
protD_ori <- protD_ori[!(protD_ori$CellID %in% '160720_2_7_28_16'), ]
plstage <- mitosysproteindist::create_mststagesplot(stgBorder, colorStg = MST_phases$colorbw, idxlabels = seq(1,20))
# Correction is performed up to stage 15 to ensure continuity of data at stage 14.  
# Only data up to stage 14 gives number of molecules on Chromatin.
protD <- mitosysproteindist::bgcorrect_totalN(df_protD = protD_ori, mststage_nucplasmcorr = 3, mststage_cytplasmcorr = 15, recompute = TRUE, usewt = TRUE, VVoxel = VVoxel_)

```

Example of plot for a single POI
```{r single cells POI_nuc12_N_infer, results='asis', fig.align = "center",  fig.height = 4, fig.width = 12, echo = FALSE}
POI <- unique(protD$POI)
for (apoi in POI[1]) {
  
  p1 <- ggplot(data= protD[protD$POI %in% apoi, ], aes(x = mitoTimeStage, y = POI_nuc12_N, color = CellID)) 
  p1 <- p1 + geom_point() 
  p1 <- p1 + geom_line()
  p1 <- p1 + ggtitle(apoi)
  p1 <- p1 + theme(legend.position="none", text = element_text(size = 8), axis.text = element_text(size = 8))
  
  p2 <- ggplot(data= protD[protD$POI %in% apoi, ], aes(x = mitoTimeStage, y = POI_nuc12_N_infer, color = CellID)) 
  p2 <- p2 + geom_point()
  p2 <- p2 + geom_line() 
  p2 <- p2 + theme(legend.position="none") 
  p2 <- p2 + theme(legend.position="none", text = element_text(size = 8), axis.text = element_text(size = 8))
  
  legend_b <- get_legend(p1 + theme(legend.position="right") + guides(color=guide_legend(ncol=2)))
  p <- plot_grid(p1, p2, legend_b, ncol = 3)
  print(p)
}
```

## Plot of mean protein numbers

```{r lineplot function, echo = FALSE}
lineplot <- function(toPlot, variables, ylabels, ylims, breaktick, sci_notation = TRUE){
  pprot <- list()
  for (ivar in c(1,2)) {
    
    pprot[[ivar]] <- ggplot(data = subset(toPlot, variable == variables[ivar]), aes(x = mitoTimeMin, y = value, color = POI))
    pprot[[ivar]] <- pprot[[ivar]] + stat_summary_bin(fun.data = mean_sdl, bins = 81, geom = 'smooth', 
                                                size = 1, na.rm=TRUE, fun.args = list(mult = 1), alpha = 0.25)
    pprot[[ivar]] <- pprot[[ivar]] + scale_fill_discrete(guide = guide_legend(title = NULL)) 
    pprot[[ivar]] <- pprot[[ivar]] + scale_color_manual(values = as.character(colorUse))
 
    pprot[[ivar]] <- pprot[[ivar]] + scale_y_continuous(expand = c(0,0), labels = function(x) format(x, scientific = sci_notation), 
                                                  breaks = breaktick) 
    pprot[[ivar]] <- pprot[[ivar]] + xlab('Mitotic standard time [min]')
    pprot[[ivar]] <- pprot[[ivar]] + ylab(ylabels[ivar]) 
    
  }
  pprot[[1]] <- pprot[[1]] + scale_x_continuous(expand = c(0,0))
  pprot[[2]] <- pprot[[2]] + scale_x_continuous(expand = c(0, 0), breaks = c(40, 50, 60)) 
  pprot[[1]] <- pprot[[1]] + coord_cartesian(xlim = c(0, stgBorder$end[14]), ylim = c(0, ylims))
  pprot[[2]] <- pprot[[2]] + coord_cartesian(xlim = c(stgBorder$start[15], 60), ylim = c(0, ylims))
  pprot[[1]] <- pprot[[1]] + theme(text = element_text(size = 8), axis.text = element_text(size = 8), 
                                   legend.position = 'none') 
  pprot[[2]] <- pprot[[2]] + theme(text = element_text(size = 8), axis.text = element_text(size = 8), 
                                   axis.text.y = element_blank(), legend.position = 'none') 
  return(pprot)
}
```

Line plot for all mitotic stages and condensin subunits

```{r Line plot POI_nuc12_N_infer POI_nuc12_N, fig.align = "center",  fig.height = 3, fig.width = 6.2, echo = FALSE}
protD <- mitosysproteindist::bgcorrect_totalN(df_protD = protD_ori, mststage_nucplasmcorr = 3, mststage_cytplasmcorr = 15, recompute = TRUE, usewt = TRUE, VVoxel = VVoxel_)
# create bar plot for stages
plstage1 <- mitosysproteindist::create_mststagesplot(stgBorder, colorStg = MST_phases$colorbw[seq(1,6)], 
                                                     idxlabels = seq(1,20), simplified = TRUE)
plstage1 <- plstage1 + coord_flip(ylim = c(0, stgBorder$end[14]))
plstage2 <- mitosysproteindist::create_mststagesplot(stgBorder, colorStg = MST_phases$colorbw[seq(1,6)], 
                                                     idxlabels = seq(1,20), simplified = TRUE)
plstage2 <- plstage2 +  coord_flip(ylim = c(stgBorder$start[15]-0.1,60))

# generate data frame to plot
POI <- unique(protD$POI)
POI <- POI[POI %in% levPOI]
POI <- factor(POI, levels = levPOI)
toPlot <- mitosysproteindist::interpolate_variables(df_protD = protD, POI = POI, 
                                                    plotVariable = c('POI_nuc12_N_infer', 'POI_nuc12_N'))
toPlot$POI <- factor(x = toPlot$POI, levels = levPOI)
pprot <- lineplot(toPlot = toPlot, variables = c('POI_nuc12_N_infer', 'POI_nuc12_N'), 
                  ylabels = c('Number of proteins on chromatin', 'Number of proteins in nucleus'), 
                  ylim = 400000, breaktick = seq(0, 400000, 100000))

                
# the legend
legend_b <- get_legend(pprot[[1]] + theme(legend.position="right", legend.key.size = unit(1, 'lines')))

# combine the plots
left_panel   <- plot_grid(plstage1, pprot[[1]], nrow = 2, align = 'v', axis = 'l', rel_heights = c(0.17, 1))
middle_panel <- plot_grid(plstage2, pprot[[2]], nrow = 2, align = 'v', axis = 'l', rel_heights = c(0.17, 1))

plineplot <- plot_grid(left_panel, middle_panel, legend_b, ncol = 3, 
               rel_widths = c(1, (60-stgBorder$start[15])/(stgBorder$end[14]-0)*1, 0.4))
plineplot
ggsave(file = file.path(outputdirpng, 'condensins_mitoTime_wt.pdf'), plot = plineplot, height = 2, width = 5.2)
ggsave(file = file.path(outputdirpdf, 'condensins_mitoTime_wt.pdf'), plot = plineplot, height = 2, width = 5.2)
```
```{r Bar plot, echo = FALSE}
# Subunits to merge
protD$Subunit <- ''
protD$Subunit[grepl(pattern = 'NCAPD', x = protD$POI )] <- 'kleisins'
protD$Subunit[grepl(pattern = 'NCAPH', x = protD$POI )] <- 'heat'
protD$Subunit[grepl(pattern = 'SMC4', x = protD$POI )] <- 'smc'

# To plot a different variable change the last entry 
plotVariable<-c('POI', 'Subunit', 'mitoTimeStage', 'mitoTimeMin', 'exp', 'frame', 'CellID','POI_nuc12_N_infer')
toPlot <- melt(protD[, plotVariable], id = c('POI','Subunit', 'frame', 'mitoTimeStage', 'mitoTimeMin', 'CellID', 'exp'))
stages <- list(c(2), c(6), c(9), c(13), c(14), c(16))
stage_name <- c('Pro', 'Prom.', 'Meta', 'Early\nana', 'Late\nana', 'Telo')

# Average cells along the stages
mPOIStages<- list()
for (i in seq_along(stages)) {
  forMean <- subset(toPlot, mitoTimeStage %in% stages[i])
  mPOI <- aggregate(value ~ POI + Subunit,  data = forMean, function (x) c(mean_N = mean(x), std_N = sd(x)))
  #convert to proper dataframe
  mPOI <- cbind(mPOI[-ncol(mPOI)], mPOI[[ncol(mPOI)]])
  mPOInrCells <- aggregate(CellID ~ POI + Subunit,  data = forMean, function (x) c(nrCells = length(unique(x))))
  colnames(mPOInrCells) <- c('POI', 'Subunit', 'nrCells')
  mPOInrExp <- aggregate(exp ~ POI + Subunit,  data = forMean, function (x) c(length(unique(x))))
  colnames(mPOInrExp) <- c('POI', 'Subunit', 'nrExp')
  mPOI$stage <- stage_name[i]
  mPOI <- merge(mPOI,mPOInrCells, by = c('POI', 'Subunit'))
  mPOI <- merge(mPOI,mPOInrExp, by = c('POI', 'Subunit'))
  mPOIStages <- rbind(mPOIStages, mPOI)
}

mPOIStages$stage <- factor(mPOIStages$stage, levels = stage_name)
mPOIStages$Subunit <- factor(mPOIStages$Subunit, levels = c('smc', 'hea', 'kleisins'))  
mPOIStages$POI <- factor(mPOIStages$POI, levels = levPOI)


pbar <- ggplot(mPOIStages, aes(x = Subunit, y = mean_N, fill = POI))
pbar <- pbar + geom_bar(stat = 'identity', position = 'stack') 
pbar <- pbar + facet_grid(~ stage, switch = 'x') 
pbar <- pbar + geom_hline(yintercept = 0) 
pbar <- pbar + scale_y_continuous(expand = c(0,0), limits = c(0, 400000), labels = function(x) x) 
pbar <- pbar + scale_fill_manual(values = as.character(colorUse))
pbar <- pbar + theme(legend.key.size =  unit(0.3, 'cm'), text = element_text(size = 8), 
                     axis.text = element_text(size = 8), axis.text.x = element_blank(), 
                     axis.ticks.x = element_blank(), axis.line.x = element_blank(), 
                     strip.background = element_blank(), strip.text = element_text(size = 8, angle = 0))
pbar <- pbar + xlab('') 
pbar <- pbar + ylab('Number of proteins on chromatin') 
ggsave(filename = file.path(outputdirpdf, 'barplot_distribution_wt.pdf'), plot = pbar, width = 4.2, height = 1.95)
ggsave(filename = file.path(outputdirpdf, 'barplot_distribution_wt.png'), plot = pbar, width = 4.2, height = 1.95)     

```

Combined line plot and bar plot for specific stages for chromatin fraction

```{r combine line and bar plots  POI_nuc12_N_infer POI_nuc12_N, fig.align = 'center', fig.height = 2.5, fig.width = 8.1}
p <- plot_grid(pprot[[1]],  pprot[[2]], pbar, ncol = 3, rel_widths = c(1, (60-stgBorder$start[15])/(stgBorder$end[14]-0)*1, 1.5), align = 'hv', axis = 'b')
p
ggsave(file = file.path(outputdirpdf, 'condensins_mitoTime_barPlot_wt.pdf'), plot = p, height = 2.5, width = 8.1)
```


Line plot of cytoplasmic and total cell variables
```{r Line plot cyt and cell N and concentration, fig.align = "center",  fig.height = 3, fig.width = 6.2, echo = FALSE}
# Number of molecules
toPlot <- mitosysproteindist::interpolate_variables(df_protD = protD, POI = POI, 
                                                    plotVariable = c('POI_cell12_N', 'POI_cell12_N'))
toPlot$POI <- factor(x = toPlot$POI, levels = levPOI)
cellNplot  <- lineplot(toPlot, variables = c('POI_cell12_N', 'POI_cell12_N'), 
                       ylabels = c('Total number\nof proteins', 'Total number\nof proteins'), 
                       ylims = 900000, breaktick = seq(0, 1e6, 3e5))

toPlot <- mitosysproteindist::interpolate_variables(df_protD = protD, POI = POI, plotVariable = c('POI_cyt12_N_infer', 'POI_cyt12_N'))
toPlot$POI <- factor(x = toPlot$POI, levels = levPOI)
cytNplot <- lineplot(toPlot, variables = c('POI_cyt12_N_infer', 'POI_cyt12_N'), 
                     ylabels = c('Number of proteins\nin cytoplasm', 'Number of proteins\nin cytoplasm'), 
                     ylims = 900000, breaktick = seq(0, 1e6, 3e5))

# concentrations
toPlot <- mitosysproteindist::interpolate_variables(df_protD = protD, POI = POI, 
                                                    plotVariable = c('POI_nuc12_nM_infer', 'POI_nuc12_nM'))
toPlot$POI <- factor(x = toPlot$POI, levels = levPOI)
nucnMplot  <- lineplot(toPlot, variables = c('POI_nuc12_nM_infer', 'POI_nuc12_nM'), 
                       ylabels = c('Protein concentration\non chromatin [nM]', 'Protein concentration\nin nucleus [nM]'), 
                       ylims = 800, breaktick = seq(0, 800, 200), sci_notation = FALSE )

toPlot <- mitosysproteindist::interpolate_variables(df_protD = protD, POI = POI, 
                                                    plotVariable = c('POI_cyt12_nM_infer', 'POI_cyt12_nM'))
toPlot$POI <- factor(x = toPlot$POI, levels = levPOI)
cytnMplot <- lineplot(toPlot, variables = c('POI_cyt12_nM_infer', 'POI_cyt12_nM'), 
                      ylabels = c('Protein concentration\nin cytoplasm [nM]', 'Protein concentration\nin cytoplasm [nM]'), 
                      ylims = 400, breaktick = seq(0, 400, 100), sci_notation = FALSE)

```

```{r combine line plots cyt and cell, fig.height = 5, fig.width = 4.14}
leftcol <- plot_grid(plstage1,  
              cellNplot[[1]] + theme(axis.text.x = element_blank(), plot.margin = margin(t = 10)) + xlab(''),
              cytNplot[[1]] + theme(axis.text.x = element_blank(), plot.margin = margin(t = 10)) + xlab(''), 
              nucnMplot[[1]] + theme(plot.margin = margin(t = 10), axis.text.x = element_blank()) + xlab(''),  
              cytnMplot[[1]] + theme(plot.margin = margin(t = 10)), 
              nrow = 5, ncol = 1,  align = 'v', axis = 'l', rel_heights = c(0.25, 1,  1,1,1.1))
rightcol <- plot_grid(plstage2,  
              cellNplot[[2]]  + theme(axis.text.x = element_blank(), plot.margin = margin(t = 10, r = 10)) + xlab(''),
              cytNplot[[2]]  + theme(axis.text.x = element_blank(), plot.margin = margin(t = 10, r = 10)) + xlab(''), 
              nucnMplot[[2]] + theme(plot.margin = margin(t = 10, r = 10), axis.text.x = element_blank()) + xlab(''),  
              cytnMplot[[2]] + theme(plot.margin = margin(t = 10, r = 10)), 
              nrow =5, ncol = 1,  align = 'v', axis = 'l', rel_heights = c(0.25, 1,  1,1,1.1))
p<-plot_grid(leftcol, rightcol, ncol = 2,  align = 'h', axis = 'b', rel_widths = c(1, (60-stgBorder$start[15])/(stgBorder$end[14]-0)*1.07))
p
ggsave(file = file.path(outputdirpdf, 'condensins_cyt_cell_conc_wt.pdf'), plot = p, height = 5, width = 4.14)
ggsave(file = file.path(outputdirpng, 'condensins_cyt_cell_conc_wt.png'), plot = p, height = 5, width = 4.14)

```



```{r Line plot POI_nuc12_N_infer POI_nuc12_N detailed mitotic stages, fig.align = "center",  fig.height = 3, fig.width = 6.2}
plstage1 <- mitosysproteindist::create_mststagesplot(stgBorder, colorStg = MST_phases$colorbw, 
                                                     idxlabels = seq(1,20), simplified = FALSE)
plstage1 <- plstage1 + coord_flip(ylim = c(0, stgBorder$end[14]))
plstage2 <- mitosysproteindist::create_mststagesplot(stgBorder, colorStg = MST_phases$colorbw, 
                                                     idxlabels = seq(1,20), simplified = FALSE)
plstage2 <- plstage2 +  coord_flip(ylim = c(stgBorder$start[15]-0.1,60))

toPlot <- mitosysproteindist::interpolate_variables(df_protD = protD, POI = POI, plotVariable = c('POI_nuc12_N_infer', 'POI_nuc12_N'))

toPlot$POI <- factor(x = toPlot$POI, levels = levPOI)
# generate data frame to plot
POI <- unique(protD$POI)
POI <- POI[POI %in% levPOI]
POI <- factor(POI, levels = levPOI)
toPlot <- mitosysproteindist::interpolate_variables(df_protD = protD, POI = POI, 
                                                    plotVariable = c('POI_nuc12_N_infer', 'POI_nuc12_N'))
toPlot$POI <- factor(x = toPlot$POI, levels = levPOI)
pprot <- lineplot(toPlot = toPlot, variables = c('POI_nuc12_N_infer', 'POI_nuc12_N'), 
                  ylabels = c('Number of proteins on chromatin', 'Number of proteins in nucleus'), 
                  ylim = 400000, breaktick = seq(0, 400000, 100000))

                
# the legend
legend_b <- get_legend(pprot[[1]] + theme(legend.position="right", legend.key.size = unit(1, 'lines')))

# combine the plots
left_panel   <- plot_grid(plstage1, pprot[[1]], nrow = 2, align = 'v', axis = 'l', rel_heights = c(0.17, 1))
middle_panel <- plot_grid(plstage2, pprot[[2]], nrow = 2, align = 'v', axis = 'l', rel_heights = c(0.17, 1))

p <- plot_grid(left_panel, middle_panel, legend_b, ncol = 3, 
               rel_widths = c(1, (60-stgBorder$start[15])/(stgBorder$end[14]-0)*1, 0.4))
ggsave(file = file.path(outputdirpdf, 'condensins_mitoTime_stages_wt.pdf'), plot = p, height = 2, width = 5.2)
ggsave(file = file.path(outputdirpng, 'condensins_mitoTime_stages_wt.png'), plot = p, height = 2, width = 5.2)
```

```{r Barplot NCAPH2, fig.height = 2, fig.width = 4}
protD$Subunit <- ''
protD$Subunit[grepl(pattern = 'NCAPD', x = protD$POI )] <- 'kleisins'
protD$Subunit[grepl(pattern = 'NCAPH', x = protD$POI )] <- 'heat'
protD$Subunit[grepl(pattern = 'SMC4', x = protD$POI )] <- 'smc'
plotVariable<-c('POI',  'mitoTimeStage', 'mitoTimeMin', 'frame', 'CellID', 'system','POI_nuc12_N_infer')

toPlot<- melt(protD[protD$POI == 'NCAPH2', plotVariable], 
              id = c('POI', 'frame', 'mitoTimeStage', 'mitoTimeMin', 'CellID', 'system'))

stages <- list(c(1,2), c(3,4,5,6,7), c(8,9,10,11), c(12,13,14), c(15,16,17))
outname <- 'NCAPH_N_stages'
stage_name <- c('Pro', 'Prometa', 'Meta', 'Ana', 'Telo')
mPOIStages <- list()
for (i in seq_along(stages)) {
  forMean <- subset(toPlot, mitoTimeStage %in% stages[[i]])
  nrCells <- length(unique(forMean$CellID))
  mPOI <- aggregate(value ~ POI,  data = forMean, function(x) c(mean = mean(x), std = sd(x, na.rm = TRUE)))
  mPOI$stage <- stage_name[i]
  mPOI$nrCells <- nrCells
  mPOIStages <- rbind(mPOIStages, mPOI)
}
levels(mPOIStages$stage) <- stage_name

mPOIStages$POI <- factor(mPOIStages$POI, levels = c('SMC4', 'NCAPD2', 'NCAPH', 'NCAPD3', 'NCAPH2'))

colorPOIs <- data.frame(POI = c('NCAPD2', 'NCAPH', 'NCAPD3', 'NCAPH2', 'SMC4'), color =
                          c('magenta', 'lightpink', 'springgreen4' , 'limegreen' ,'gray40'))

colorUse <- list()
for (poi in c('SMC4', 'NCAPD2', 'NCAPH', 'NCAPD3', 'NCAPH2')) {
  colorUse <- cbind(colorUse, as.character(colorPOIs[colorPOIs$POI == poi, ]$color[1]))
}
mPOIStages$stage <- factor(mPOIStages$stage, levels = stage_name)
p <- ggplot(mPOIStages, aes(x = stage, y = value[, 'mean'], color = 'gray60', fill = 'gray10')) 
p <- p + geom_bar(stat = 'identity', position = 'stack', width = 0.7)
p <- p + geom_errorbar(aes(ymax = value[, 'mean'] + value[,'std'], ymin = value[, 'mean']), width = 0.3 )
p <- p + scale_x_discrete(labels = sprintf('%s\nn=%d', mPOIStages$stage, mPOIStages$nrCells))
p <- p + scale_y_continuous(expand = c(0,0), labels=function(n){format(n, scientific = FALSE)}, limits = c(0, 60000))
p <- p + scale_fill_manual(values = 'gray90')
p <- p + scale_color_manual(values = 'gray30')
p <- p + theme(legend.position = 'none', text = element_text(size = 10), axis.text =element_text(size = 10))
p <- p + xlab('Mitotic phase') 
p <- p + ylab('Number of proteins in\nchromatin volume') 

p
ggsave(filename = file.path(outputdirpdf, 'NCAPH_N_stages.pdf'), width = 4, height = 2)

```
## Distribution in cytoplasm, nucleus and total
Make an area plot

```{r distribution of cytoplasm nucleus etc. ,  results='asis', fig.align = "center",  fig.height = 2, fig.width = 5.2}
plstage1 <- mitosysproteindist::create_mststagesplot(stgBorder, colorStg = MST_phases$colorbw[seq(1,6)], 
                                                     idxlabels = seq(1,20), simplified = TRUE)
plstage1 <- plstage1 + coord_flip(ylim = c(0, stgBorder$end[14]))
plstage2 <- mitosysproteindist::create_mststagesplot(stgBorder, colorStg = MST_phases$colorbw[seq(1,6)], 
                                                     idxlabels = seq(1,20),   simplified = TRUE)
plstage2 <- plstage2 +  coord_flip(ylim = c(stgBorder$start[15]-0.1,60))

# A dummy variable to match with 2nd graph
protD$dummy <- 0
plotVariable <- c('POI_cyt12_N','POI_nuc12_N_bg_infer', 'POI_nuc12_N_infer', 'dummy')
toPlot <- mitosysproteindist::interpolate_variables(df_protD = protD, POI = POI, plotVariable =  plotVariable)
POI <- unique(toPlot$POI)

# seq_along(POI)
for (nrPOI in seq_along(POI)) {
  
  pprot1 <- ggplot(data = toPlot[toPlot$POI %in% POI[nrPOI],], aes(x = mitoTimeMin, y = value, fill = variable)) 
  pprot1 <- pprot1 + stat_summary_bin(fun.y = mean, bins = 81, geom = 'area', position= 'stack', na.rm = TRUE) 
  pprot1 <- pprot1 + theme_classic()
  pprot1 <- pprot1 + scale_fill_manual(guide = guide_legend(title = NULL), 
                                       labels = c('cytoplasm', 'chromatin soluble', 'chromatin', 'nucleus'), 
                                       values = c('darkcyan', 'cyan', 'wheat', 'darkorange')) 
  pprot1 <- pprot1 + ylab('Number of proteins') 
  pprot1 <- pprot1 + xlab('Mitotic standard time [min]')
  pprot1 <- pprot1 + theme(legend.position = 'none',  axis.text = element_text(size = 8), 
                           text = element_text(size = 8), legend.key.size = unit(1, 'lines')) 
  pprot1 <- pprot1 + scale_x_continuous(expand = c(0, 0)) 
  pprot1 <- pprot1 + coord_cartesian(xlim = c(0, stgBorder$end[14]), ylim = c(0, 800000)) 
  pprot1 <- pprot1 + scale_y_continuous(expand = c(0,0))
  
  
  pprot2 <- ggplot(data= toPlot[toPlot$POI %in% POI[nrPOI],], aes(x = mitoTimeMin, y = value, fill = variable))
  pprot2 <- pprot2 + stat_summary_bin(fun.y = mean, bins = 81, geom = 'area', position= 'stack', na.rm = TRUE)
  pprot2 <- pprot2 + theme_classic()
  pprot2 <- pprot2 + scale_fill_manual(guide = guide_legend(title = NULL), 
                                       labels = c('cytoplasm', 'chromatin bg', 'nucleus'), 
                                       values = c('darkcyan', 'darkorange', 'darkorange', 'darkorange'))  
  pprot2 <- pprot2 + ylab('Number of proteins')
  pprot2 <- pprot2 + xlab('Mitotic standard time [min]') 
  pprot2 <- pprot2 + theme(legend.position = 'none', text = element_text(size = 8), axis.text = element_text(size = 8),
                           axis.text.y = element_blank()) 
  pprot2 <- pprot2 + scale_x_continuous(expand = c(0, 0), breaks = c(40,50,60)) 
  pprot2 <- pprot2 + coord_cartesian(xlim = c(stgBorder$start[15], 60), ylim = c(0, 800000))
  pprot2 <- pprot2 + scale_y_continuous(expand = c(0,0)) 
  
  legend_b <- get_legend(pprot1 + theme(legend.position="right", legend.text = element_text(size = 8),  legend.key.size = unit(0.3, "cm")))

  left_panel <- plot_grid( plstage1, pprot1, nrow = 2, align = 'v', axis = 'l', rel_heights = c(0.17, 1))
  middle_panel <- plot_grid( plstage2, pprot2, nrow = 2, align = 'v', axis = 'l', rel_heights = c(0.17, 1))
  # combine graphs
  p <- plot_grid(left_panel, middle_panel, legend_b, ncol = 3, 
                 rel_widths = c(1, (60-stgBorder$start[15])/(stgBorder$end[14]-0)*1, 0.4), labels = c('', '', ''))
  
  ggsave(plot = p, file = file.path(outputdirpng, paste(POI[nrPOI], 'mitoTime_wt.png', sep ='_')), height = 2, width = 5.2)
  ggsave(plot = p, file = file.path(outputdirpdf, paste(POI[nrPOI], 'mitoTime_wt.pdf', sep ='_')), height = 2, width = 5.2)
}
p
```

## Compute ratio chromatin to total
```{r ratio Chromatin to Total, fig.width  = 3, fig.height = 1.6}
colorPOIs <- data.frame(POI = c('CAP-D2', 'CAP-H', 'CAP-D3', 'CAP-H2', 'SMC4'), color =
                          c('magenta', 'lightpink', 'springgreen4' , 'limegreen' ,'gray40') )

protD <- mitosysproteindist::bgcorrect_totalN(protD_ori, recompute = TRUE, usewt = TRUE, VVoxel = VVoxel_)
protD_meta <- subset(protD, mitoTimeStage %in% c(9,10,11))
protD_ratio <- data.frame('cellline' = protD_meta$POI, 'ratio' = protD_meta$POI_nuc12_N_infer/protD_meta$POI_cell12_N)
levPOI <- c('SMC4', 'CAP-D3', 'CAP-D2', 'CAP-H2', 'CAP-H')
protD_ratio$POI <- 'poi'
cells <- unique(protD_ratio$cellline)
for (icell in seq_along(cells)){
  protD_ratio$POI[protD_ratio$cellline == cells[icell]] <- levPOI[icell] 
}
levPOI <- c('SMC4', 'CAP-D2', 'CAP-H', 'CAP-D3','CAP-H2')
protD_ratio$POI <-factor(protD_ratio$POI, levels = levPOI) 
colorUse <- list()
for (poi in levPOI) {
  colorUse <- cbind(colorUse, as.character(colorPOIs[colorPOIs$POI == poi, ]$color[1]))
}
colorUse <- as.character(colorUse)

# this comes from a different data set
p1 <- ggplot(data = protD_ratio, aes(x = POI, y = ratio,  color = POI)) 
p1 <- p1 + geom_boxplot(outlier.shape = NA, lwd = 0.3)  
p1 <- p1 + ylab('Protein on chromatin /\nTotal protein') 
p1 <- p1 + scale_color_manual(values = colorUse) 
p1 <- p1 + xlab('') 
p1 <- p1 + theme_classic() 
p1 <- p1 + theme(legend.position = 'none', text = element_text(size = 8), axis.text = element_text(size = 8), 
                 axis.text.x = element_text(size = 8, angle = 45, vjust = 0.5))
p1 <- p1 + scale_y_continuous(breaks = c(0.2, 0.4, 0.6, 0.8))
p1
ggsave(plot = p1, file = file.path(outputdirpdf, 'boxplot_ratio_mitosys_wt.pdf'), width = 3, height = 1.6)
```

## Compare different homozygous clones
Compare several N and C terminally tagged clones. Compare the ratios between D and H condensing subunits.

```{r compare clones ratio D/H, fig.height = 3, fig.width = 2.5}
# This data set has WT cell has bg so no need to recompute
protD2 <- read.csv('../data/proteinDistribution_170723_severalClones.txt', sep = '\t', header = TRUE)
protD2$mitoTimeStage <- 9

protD2 <- mitosysproteindist::bgcorrect_totalN(protD2, recompute = TRUE, usewt = TRUE, VVoxel = VVoxel_)

# Compute average of POI_nuc12_N_infer or each clone and POI
mPOI_nuc <- aggregate(POI_nuc12_N_infer ~ POI, data = protD2, function(x) c(mean = mean(x), nrcells = length(x)))
# create data.frame
mPOI_nuc <- cbind(mPOI_nuc[-ncol(mPOI_nuc)], mPOI_nuc[[ncol(mPOI_nuc)]])
# assign N and C terminal tagging id
mPOI_nuc$term <- 'N'
mPOI_nuc$term[which(grepl(x = mPOI_nuc$POI, pattern = '^gfp'))] <- 'C'
# pattern match the clones
NCAPH  <- rbind(mPOI_nuc[grepl('NCAPHgfp(c|z)', x = mPOI_nuc$POI) & !grepl('halo', x = mPOI_nuc$POI), ],
                mPOI_nuc[grepl('gfpNCAPH(c|z)', x = mPOI_nuc$POI) & !grepl('halo', x = mPOI_nuc$POI), ])
NCAPD2 <- rbind(mPOI_nuc[grepl('NCAPD2gfp(c|z)', x = mPOI_nuc$POI) & !grepl('halo', x = mPOI_nuc$POI), ], 
                mPOI_nuc[grepl('gfpNCAPD2(c|z)', x = mPOI_nuc$POI) & !grepl('halo', x = mPOI_nuc$POI), ])
NCAPH2 <- rbind(mPOI_nuc[grepl('NCAPH2gfp(c|z)', x = mPOI_nuc$POI) & !grepl('halo', x = mPOI_nuc$POI), ], 
                mPOI_nuc[grepl('gfpNCAPH2(c|z)', x = mPOI_nuc$POI) & !grepl('halo', x = mPOI_nuc$POI), ])
NCAPD3 <- rbind(mPOI_nuc[grepl('NCAPD3gfp(c|z)', x = mPOI_nuc$POI) & !grepl('halo', x = mPOI_nuc$POI), ], 
                mPOI_nuc[grepl('gfpNCAPD3(c|z)', x = mPOI_nuc$POI) & !grepl('halo', x = mPOI_nuc$POI), ])

# order the values
NCAPH2 <- NCAPH2[order(-NCAPH2$mean), ]
NCAPH <- NCAPH[order(-NCAPH$mean), ]

# Compute the ratios for Condesin I
Cond1 <- data.frame('CAPH' = '', 'CAPD2' = '', 'ratio' = 1)
for (i in seq_along(NCAPD2$POI)) {
  for (j in seq_along(NCAPH$POI))
    Cond1 <- rbind(Cond1, data.frame('CAPH' =  NCAPH$POI[j], 'CAPD2' = NCAPD2$POI[i], 'ratio' =  NCAPD2$mean[i]/NCAPH$mean[j]))
}
Cond1 <- Cond1[-1,]

# Compute the rations for Condensin II
Cond2 <- data.frame('CAPH2' = '', 'CAPD3' = '', 'ratio' = 1)
for (i in seq_along(NCAPD3$POI)) {
  for (j in seq_along(NCAPH2$POI))
    Cond2 <- rbind(Cond2, data.frame('CAPH2' =  NCAPH2$POI[j], 'CAPD3' = NCAPD3$POI[i], 'ratio' =  NCAPD3$mean[i]/NCAPH2$mean[j]))
}
Cond2 <- Cond2[-1,]

# create factors for plotting
Cond2$CAPH2 <- factor(Cond2$CAPH2, levels = NCAPH2$POI)
Cond1$CAPH  <- factor(Cond1$CAPH, levels = NCAPH$POI)
p <- list()
p[[1]] <- ggplot(data = Cond1, aes(x= CAPD2, y= CAPH, fill = ratio)) 
p[[2]] <- ggplot(data = Cond2, aes(x= CAPD3, y= CAPH2, fill = ratio))

for (i in c(1,2)) {
  p[[i]] <- p[[i]] + geom_tile(color = "gray", lwd = 0.5)
  p[[i]] <- p[[i]] + geom_text(aes(label = round(ratio,1)), color = "black", size = 2)
  p[[i]] <- p[[i]] + scale_x_discrete(expand = c(0,0), position = 'top')
  p[[i]] <- p[[i]] + scale_y_discrete(expand = c(0,0))
  p[[i]] <- p[[i]] + scale_fill_continuous(low = 'white', high = 'white')
  p[[i]] <- p[[i]] + xlab('') 
  p[[i]] <- p[[i]] + ylab('')
  p[[i]] <- p[[i]] + theme(panel.border = element_rect(colour='black', size=1), legend.position = 'none', 
                           text = element_text(size = 8), axis.text.x = element_text(angle = 45, hjust = 0),
                           axis.text = element_text(size = 8))
  
}

p[[1]] + theme(plot.margin = margin(r = 30))
p[[2]] + theme(plot.margin = margin(r = 25))
ggsave(plot = p[[1]] + theme(plot.margin = margin(r = 30)), filename = file.path(outputdirpdf, 'caph_capd2_ratio.pdf'), 
       height = 2, width = 1.7)
ggsave(plot = p[[2]] + theme(plot.margin = margin(r = 25)), filename = file.path(outputdirpdf, 'caph2_capd3_ratio.pdf'), 
       height = 3, width = 2.5)

```

Compare the difference between N and C terminally tagged clones (CAP-H and CAP-H2)
```{r Compare distribution of CAP-H and CAP-H2 clones}
# identify N or C terminal tagging 
protD2$term <- 'N' 
protD2$term[which(grepl(x = protD2$POI, pattern = '^gfp'))] <- 'C'

# Identify POI for each cell line
allNCAPH <- rbind(protD2[grepl('NCAPHgfp(c|z)', x = protD2$POI) & !grepl('halo', x = protD2$POI), ], protD2[grepl('gfpNCAPH(c|z)', x = protD2$POI) & !grepl('halo', x = protD2$POI), ])
allNCAPH2 <- rbind(protD2[grepl('NCAPH2gfp(c|z)', x = protD2$POI) & !grepl('halo', x = protD2$POI), ], protD2[grepl('gfpNCAPH2(c|z)', x = protD2$POI) & !grepl('halo', x = protD2$POI), ])
allNCAPD2 <- rbind(protD2[grepl('NCAPD2gfp(c|z)', x = protD2$POI) & !grepl('halo', x = protD2$POI), ], protD2[grepl('gfpNCAPD2(c|z)', x = protD2$POI) & !grepl('halo', x = protD2$POI), ])
allNCAPD3 <- rbind(protD2[grepl('NCAPD3gfp(c|z)', x = protD2$POI) & !grepl('halo', x = protD2$POI), ], protD2[grepl('gfpNCAPD3(c|z)', x = protD2$POI) & !grepl('halo', x = protD2$POI), ])

p <- ggplot(data = allNCAPH)
p <- p + geom_histogram(aes(x = POI_nuc12_N_infer, y = ..count.., fill = term), alpha = 0.5, colour = 'black', binwidth = 10000) 
p <- p + scale_fill_discrete(name = 'CAP-H', labels = c('C-terminus', 'N-terminus'))
p <- p + theme(text = element_text(size = 10), axis.text = element_text(size = 10))
p <- p + xlab('# proteins on chromatin')
p_hist_CAPH <- p + ylab('# cells')
p_hist_CAPH

p <- ggplot(data = allNCAPH2)
p <- p + geom_histogram(aes(x = POI_nuc12_N_infer, y = ..count.., fill = term), alpha = 0.5, colour = 'black', binwidth = 5000) 
p <- p + scale_fill_discrete(name = 'CAP-H2', labels = c('C-terminus', 'N-terminus')) 
p <- p + theme(text = element_text(size = 10), axis.text = element_text(size = 10))
p <- p + xlab('# proteins on chromatin')
p_hist_CAPH2 <- p + ylab('# cells')
p_hist_CAPH2



# Cumulatives
p <- ggplot(data = allNCAPH2)
p <- p + stat_ecdf(aes(x = POI_nuc12_N_infer, colour = term),lwd = 1) 
p <- p + scale_color_discrete(name = 'CAP-H2', labels = c('C-terminus', 'N-terminus')) 
p <- p + theme(legend.position = 'none', text = element_text(size = 10), axis.text = element_text(size = 10))
p <- p + xlab('# proteins on chromatin')
p_cum_CAPH2 <- p + ylab('Cumulative density')
p_cum_CAPH2

p <- ggplot(data = allNCAPH)
p <- p + stat_ecdf(aes(x = POI_nuc12_N_infer, colour = term),  lwd = 1) 
p <- p + scale_color_discrete(name = 'CAP-H', labels = c('C-terminus', 'N-terminus')) 
p <- p + theme(legend.position = 'none', text = element_text(size = 10), axis.text = element_text(size = 10))
p <- p + xlab('# proteins on chromatin')
p_cum_CAPH <- p + ylab('Cumulative density')
p_cum_CAPH
```

Plot distribution of CAP-H and CAP-H2 clones for C and N terminal tagging
```{r plot CAP-H and CAP-H2 clone number distribution. Stat-test and combine plots}
# Perform Kolmogorov-Smirnov test to test diffences between distributions
ks.test(allNCAPH[allNCAPH$term == 'C', ]$POI_nuc12_N_infer, allNCAPH[allNCAPH$term == 'N', ]$POI_nuc12_N_infer)

ks.test(allNCAPH2[allNCAPH2$term == 'C', ]$POI_nuc12_N_infer, allNCAPH2[allNCAPH2$term == 'N', ]$POI_nuc12_N_infer)

# Wilcox test equivalent to non-parametric t-test
wilcox.test(allNCAPH[allNCAPH$term == 'C', ]$POI_nuc12_N_infer, allNCAPH[allNCAPH$term == 'N', ]$POI_nuc12_N_infer)

wilcox.test(allNCAPH2[allNCAPH2$term == 'C', ]$POI_nuc12_N_infer, allNCAPH2[allNCAPH2$term == 'N', ]$POI_nuc12_N_infer)

# Perform Variance test (Ansary Bradley, non parametric. Median must be from same distribution). Similar to F-test
ansari.test(allNCAPH[allNCAPH$term == 'C', ]$POI_nuc12_N_infer, allNCAPH[allNCAPH$term == 'N', ]$POI_nuc12_N_infer)

ansari.test(allNCAPH2[allNCAPH2$term == 'C', ]$POI_nuc12_N_infer, allNCAPH2[allNCAPH2$term == 'N', ]$POI_nuc12_N_infer)



# combine plots
p <- plot_grid( p_cum_CAPH, p_hist_CAPH, p_cum_CAPH2, p_hist_CAPH2,  ncol = 2, nrow = 2, rel_widths = c(0.6,1))
p 
ggsave(plot = p, filename = file.path(outputdirpng, 'caph_caph2_clones_distribution.png'), width = 8, height = 6)
ggsave(plot = p, filename = file.path(outputdirpdf, 'caph_caph2_clones_distribution.pdf'), width = 8, height = 6)
```

## Compute precision of FCS calibrated method 
Compute intra and inter experiment variability

```{r compute precision of method use stage 8-11}
procMeta <- protD[protD$mitoTimeStage %in% c(8,9,10,11), ]

# compute mean of each cell as there are several time points per cell

perCellID <- with(procMeta, aggregate(x = POI_nuc12_N_infer  , by = list(CellID = CellID, POI = POI, exp = exp), FUN = function(x) c('N' = mean(x) )))
colnames(perCellID) <- c('CellID', 'POI', 'exp', 'N')

perExp <- with(perCellID, aggregate(x = N , by = list(exp = exp, POI = POI),  FUN = function(x) c('m' = mean(x), 'sd' = sd(x), 'nrCells' = length(x))))
perExp <- do.call(data.frame, perExp)
colnames(perExp) <- c('exp', 'POI', 'N_meanexp','N_sdexp' )
intraCV <- mean(100*perExp$N_sdexp/perExp$N_meanexp, na.rm = T)

perPOI <- with(perExp, aggregate(x = N_meanexp , by = list(POI = POI),  FUN = function(x) c('m' = mean(x), 'sd' = sd(x), 'nrExp' = length(x))))
perPOI <- do.call(data.frame, perPOI)
colnames(perPOI) <- c( 'POI', 'N_mPOI','N_sdPOI' , 'nrExp')

interCV <- mean(100*perPOI$N_sdPOI/perPOI$N_mPOI, na.rm = T)
```

