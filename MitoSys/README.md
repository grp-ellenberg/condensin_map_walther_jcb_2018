# MitoSys

This folder contains the source code (folder `src` R markdown) to process the protein distribution files obtained from FCS-calibrated confocal time-lapse imaging and generate time-resolved cellular protein concentration and number maps.

An example tab-delimited data set can be found in `data`.

Cellular marker segmentation, time alignment, and FCS-calibration has been performed in the exact same way as in

> Cai Y, Hossain MJ, Heriche JK, Politi AZ, Walther N, Koch B, Wachsmuth M, Nijmeijer B, Kueblbeck M, Martinic M, Ladurner R, Peters JMP, Ellenberg J. An experimental and computational framework to build a dynamic protein atlas of human cell division. bioRxiv 227751. doi: https://doi.org/10.1101/227751 

The code is available on the mitotic_cell_atlas repository and accessible via the following link:
https://git.embl.de/grp-ellenberg/mitotic_cell_atlas

The corresponding project website is: http://www.mitocheck.org/mitotic_cell_atlas/index.html


# Authors:
Antonio Politi, EMBL, Heidelberg