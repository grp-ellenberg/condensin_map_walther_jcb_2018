"""
frapmitoticchr
ImageJ script to extract intensity profile of chromatin and a protein of interest along mitotic chromosomes.
Filepaths to process and certain options are read from a database.
The workflow is:
    Segment chromatin per frame (Gaussian blur, Thresholding using one of the ImageJ methods (Default or Huang work well, fill holes).
    Compute 3D connected components and remove small objects.
    Find main axis of chromatin in each frame and create line roi (several pixels wide) along main axis.
    Optionally apply anisotropic diffusion filter.
    Apply binary mask to chromatin and POI.
    Extract data using the line ROI.
    Save profile data to file.
"""

from ij import IJ
from ij.plugin import ChannelSplitter
from ij.plugin import Duplicator
from ij.plugin.frame import RoiManager
from ij.measure import Measurements
from ij.gui  import Line, WaitForUserDialog
from ij.plugin import ImageCalculator
from ij.io import OpenDialog
from loci.plugins import BF
from loci.plugins.in import ImporterOptions
import os
import csv
from java.awt import Color
from java.lang import Double, Math

# Some global constants. Change accordingly
CHPOI = 1       # Channel with the protein of interest
CHMARKER = 2    # Channel with the cellular marker (e.g. DNA)
NSLICES = [1,9] # start and end z-slice to process
ANIDIFF = 1     # 0 or 1 depending if anisotropic diffusion filter has been applied
FILTER_SIZE = 3 # filter of gaussian blur

def segment(imgM, thr_method = "Huang", filter_size = 3):
    """
    segment single channel stack
    :param imgM: image plus single channel
    :param thr_method: method used to compute threshold
    :param filter_size: size of sigma for Gaussian blut
    :return: binary of segmented img. image plus
    """
    IJ.run(imgM, "Gaussian Blur...", "sigma=%d stack" %filter_size)
    IJ.setAutoThreshold(imgM, "%s dark" % thr_method)
    IJ.run(imgM, "Convert to Mask", "method=Default background=Dark black")
    #IJ.run(imgM, "Open", "stack")
    #IJ.run(imgM, "Close", "stack")
    IJ.run(imgM, "Fill Holes", "stack")
    return(imgM)

def skeleton(imgM):
    """
    Compute skeleton and roi of longest branch
    :param imgM: single channel image Plus
    :return: binary imagePlus of skeleton
    """
    IJ.run(imgM, "Skeletonize", "stack")

    roim = RoiManager.getInstance()
    if roim is None:
        roim = RoiManager()
    roim.runCommand("reset")

    for iS in range(1, imgM.getNSlices()*imgM.getNFrames()):
        imgM.setSlice(iS)
        IJ.run(imgM, "Geodesic Diameter", "label=Segmented_blob distances=[Chessknight (5,7,11)] image=Segmented_blob export")
    return(imgM)

def getfilenames(imgpath, aniDiff):
    """
    Create a list of file names
    :param imgpath: the path to the image
    :param aniDiff: 0 or 1 if anistropic postfix is added to file name
    :return: list of file names
    """
    wd = os.path.dirname(imgpath)
    # try:
    #     os.mkdir(os.path.join(os.path.dirname(wd), 'Results'))
    # except OSError as exc:
    #     if exc.errno != errno.EEXIST:
    #         raise
    # pass

    if aniDiff:
        addtxt = 'aniDiff'
    else:
        addtxt = ''
        
    basename = os.path.splitext(os.path.basename(imgpath))[0]
    file_names = [os.path.join(wd,  '%s_%s_poi.txt' % (basename, addtxt)),
                  os.path.join(wd,  '%s_%s_chr.txt' % (basename, addtxt)),
                  os.path.join(wd,  '%s_%s_mask.txt' % (basename, addtxt)),
                  os.path.join(wd, '%s_%s_mask.tif' % (basename, addtxt)), 
                  os.path.join(wd, '%s_%s_roi.zip' % (basename, addtxt))]
    return(file_names)

def mergeRois(img, rois):
    """
    For each slice of image check if there are multipe rois and merge them
    :param img : image plus of Chromatin or POI
    :param rois: an array of rois
    """
    roim = RoiManager.getInstance()
    if roim is None:
        roim = RoiManager()
    roim.runCommand('reset')
    roiCleanup = []
    for islice in range(1, img.getNSlices()+1):
        roislice = []
        img.setSlice(islice)
        for iroi, roi in enumerate(rois):
            if roi.getPosition() == islice:
                roislice.append(roi)
        if len(roislice) == 0:
            continue
        if len(roislice) > 1:
            for roi in roislice:
                roim.addRoi(roi)
            roim.setSelectedIndexes(range(0, len(roislice)))
            roim.runCommand("Combine")
            roim.runCommand('Add')
            roim.select(len(roislice))
            roi = roim.getRoi(len(roislice))
        else:
            roi = roislice[0]
        roi.setPosition(islice)
        roiCleanup.append(roi)
        roim.runCommand('reset')  
         
    for roi in roiCleanup:
        img.setSlice(roi.getPosition())
        img.setRoi(roi)
        roim.addRoi(roi) 
    
    return(roiCleanup)

def createLineRois(img, rois, length = 90.0, width = 24):
    '''
    Create a line roi using the principal component of 2D fitted ellipse to a polyline roi
    :param img: an image associated to the roi
    :param rois: a list of rois
    :param length: maximal length of line in pixels
    :param width: width of line in pixels
    :return: list of line rois
    '''
    roiLines = list()
    halflength = length/2.0
    for iroi, roi in enumerate(rois):
        img.setSlice(roi.getPosition())
        img.setRoi(roi)
        ell = img.getStatistics(Measurements.ELLIPSE, Measurements.CENTROID)
        anglerad = Math.toRadians(ell.angle)

        startCoord = [ell.xCentroid - halflength*Math.cos(anglerad), ell.yCentroid + halflength*Math.sin(anglerad)]
        endCoord = [ell.xCentroid +  halflength*Math.cos(anglerad), ell.yCentroid -  halflength*Math.sin(anglerad)]
        # avoid flipping of reading direction of line profile
        if (anglerad < Math.PI/2):
            endCoord_old = endCoord
            endCoord = startCoord
            startCoord = endCoord_old
        lroi = Line(round(endCoord[0]), round(endCoord[1]), round(startCoord[0]),round(startCoord[1]))
        lroi.setStrokeWidth(width)
        lroi.setPosition(roi.getPosition())
        roiLines.append(lroi)

    return(roiLines)

def writeoutput(imgs, rois, fnames):
    """
    Write output of intensity in roi pixels
    :param imgs: a list of images
    :param rois: a list of rois
    :param fnames: a list of output paths
    :return: None
    """
    # max length of output vector
    imgs[0].setRoi(rois[0])
    maxL = len(imgs[0].getRoi().getPixels().tolist())
    # maximum nu
    NSlices = imgs[0].getNSlices()
    lLines = list()
    for iroi, roi in enumerate(rois):
        imgs[0].setRoi(roi)
        lLines.append(len(imgs[0].getRoi().getPixels().tolist()))
    maxEffectiveLength = max(lLines)
    for iname, name in enumerate(fnames[0:3]):
        fid = open(name, 'w')
        #fid.write('slice\ttpos\tzpos\t' + '\t'.join(['X%d' % p for p in range(1,maxL) ]) + '\n')
        for iroi, roi in enumerate(rois):
            imgs[iname].setSlice(roi.getPosition())
            imgs[iname].setRoi(roi)
            fid.write('%d\t%d\t%d\t' % (roi.getPosition(),  Math.floor((roi.getPosition()-1)/NSlices)+1, ((roi.getPosition()-1) % NSlices) + 1))
            outstr = '\t'.join(['%.3f' % el for el in imgs[iname].getRoi().getPixels().tolist()]) 
            lString = len(imgs[iname].getRoi().getPixels().tolist())
            if (lString < maxEffectiveLength):
                dL = maxEffectiveLength - lString
                if dL % 2 == 0:    
                    outstr = '\t'.join(['%.3f' % 0  for i in range(1,dL/2+1)]) +  '\t' + outstr
                    outstr = outstr + '\t' +  '\t'.join(['%.3f' % 0 for i in range(1,dL/2+1)])
                else:
                    dL = dL+1
                    outstr = '\t'.join(['%.3f' % 0 for i in range(1,dL/2+1)]) +  '\t' + outstr
                    outstr = outstr +  '\t' + '\t'.join(['%.3f' % 0 for i in range(1,dL/2)])
            fid.write(outstr + '\n')
        fid.close()

def run(imgpath, ChPOI = 1, ChMarker = 2, nSlices = [1, 9], nFrames = [1,1], aniDiff = True, thr_method1 = 'Huang', filter_size = 3, exclude = 1):
    """
    run pipeline
    :param imgpath: path to image
    :param ChPOI: Channel of POI
    :param ChMarker: Channel of marker where to perform segmentation
    :param nSlices: [z_slice_start, z_slice_end]
    :param nFrames: [t_frame_start, t_frame_end]
    :param aniDiff: boolean for applying anisotropic 2D filter before extracting itensities
    :param filter_size: size of Gaussian filter in segment function
    :param exclude: boolean to exclude objects touching the boundary in the first round
    """


    # Sanity checks
    assert nFrames[0] < nFrames[1], 'Number of frames to investigate nFrames must be [frame_start, frame_end] with frame_start < frame_end'
    assert nSlices[0] < nSlices[1], 'Number of slices to investigate nFrames must be [slice_start, slice_end] with slice_start < slice_end'

    IJ.run("Close All", "")


    # initiate RoiManager and other classes needed
    roim = RoiManager.getInstance()
    if roim is None:
        roim = RoiManager()
    dp = Duplicator()
    ic = ImageCalculator()

    # import image
    opt = ImporterOptions()
    opt.setVirtual(True)
    opt.setId(imgpath)
    img = BF.openImagePlus(opt)[0]

    # further sanity checks
    if nFrames[0] < 1:
        nFrames[0] = 1
    if nFrames[1] > img.getNFrames():
        nFrames[1] = img.getNFrames()
    if nSlices[0] < 1:
        nSlices[0] = 1
    if nSlices[1] > img.getNSlices():
        nSlices[1] = img.getNSlices()

    img = dp.run(img, 1, 2, nSlices[0], nSlices[1], nFrames[0],
                 nFrames[1])

    # remove scale
    IJ.run(img, "Set Scale...", "distance=0 known=0 pixel=1 unit=pixel")

    # split channels
    imgC = ChannelSplitter.split(img)

    # flatten dimensions for convenience
    for cimg in imgC:
       IJ.run(cimg, "Hyperstack to Stack", "")


    # segment DNA mass. This will be used to find axis of chromatin per frame
    # Objective is one ROI per main chromatin mass per slice
    # The Huang filter oversegments DNA but avoid discontinuities in Rois.
    # Typically Default filter works fine

    imgM = segment(imgC[ChMarker-1].duplicate(), thr_method = thr_method1, filter_size = filter_size)

    # computed 3D connected components and remove small objects
    imgM.show()
    IJ.run("Connected Components Labeling", "connectivity=6 type=[16 bits]")
    IJ.run("Keep Largest Region", "")
    imgM = IJ.getImage()
    imgM.setTitle('Segmented_3Dblob')
    # remove remaining single small blobs and create roi table and line ROIs
    IJ.run("Set Measurements...", "area center fit shape integrated stack redirect=None decimal=5")
    roim.runCommand("reset")
    if exclude:
        IJ.run(imgM, "Analyze Particles...", "size=200-Infinity pixel show=Masks clear exclude add in_situ stack")
    else:
        IJ.run(imgM, "Analyze Particles...", "size=200-Infinity pixel show=Masks clear add in_situ stack")
    roiMarker = roim.getRoisAsArray()
    roiMarker = mergeRois(imgM, roiMarker)
    roim.runCommand('reset')
    for roi in roiMarker:
        img.setSlice(roi.getPosition())
        img.setRoi(roi)
        roim.addRoi(roi) 
    roim.runCommand('reset')
    roiLines = createLineRois(img = imgM,  rois = roiMarker, length = 90.0, width = 24 )


    
    # Perform a refined segmentation of DNA mass using Otsu threshold and less smoothing
    imgM = segment(imgC[ChMarker-1].duplicate(), thr_method = 'Otsu', filter_size = 2)
    IJ.run(imgM, "Analyze Particles...", "size=100-Infinity pixel show=Masks exclude clear add in_situ stack")
    IJ.run(imgM, "Multiply...", "value=%f stack" % (1.0/255.0))
    # this can have double Roi per object
    roiRefined = roim.getRoisAsArray()

    # Quantification of images
    # Anisotropic diffusion to eliminate some random noise outside object
    if aniDiff:
        IJ.run(imgC[ChPOI-1], "Anisotropic Diffusion 2D", "number=20 smoothings=1 a1=0.50 a2=0.90 dt=20 edge=5")
        imgP = IJ.getImage()
        imgP.hide()
        IJ.run(imgC[ChMarker-1], "Anisotropic Diffusion 2D", "number=20 smoothings=1 a1=0.50 a2=0.90 dt=20 edge=5")
        imgCh = IJ.getImage()
        imgCh.hide()
    else:
        imgP = imgC[ChPOI-1]
        imgCh = imgC[ChMarker-1]
    # Mask the chromatin and POI
    imgP = ic.run("Multiply create stack", imgM, imgP)
    imgP.show()
    imgCh = ic.run("Multiply create stack", imgM, imgCh)
    imgCh.show()
    imgP.setTitle('POI')
    imgCh.setTitle('Chromatin')
    imgM.setTitle('Refined Mask')
    imgM.show()
    roim.runCommand('reset')
    for iroi, roi in enumerate(roiLines):
        imgP.setSlice(roi.getPosition())
        #roim.add(imgP, roi, iroi)
        roi.setColor(Color(255, 0, 0, 125))
        imgP.setRoi(roi)
        roim.addRoi(roi)
    # Generate output
    imgO = [imgP, imgCh, imgM]
    file_names = getfilenames(imgpath, aniDiff)
    writeoutput(imgO, rois = roiLines, fnames = file_names)
    IJ.saveAs(imgM, "Tiff", file_names[3])
    roim.runCommand("Save", file_names[4])
    imgM.show()
    roim.show()

if __name__ == "__builtin__":
    # Run pipeline
    od = OpenDialog('Select a FRAP database file', '', 'database_frap.txt')
    databasefile = os.path.join(od.getDirectory(), od.getFileName())
    wd = os.path.dirname(databasefile)
    with open(databasefile, 'rU') as csvfile:
        reader = csv.DictReader(csvfile, dialect = csv.excel_tab)
        allrows = []
        for row in reader:
            allrows.append(row)
    for irow, row in enumerate(allrows):
        if row['manualQC'] == '1' and row['segmentation'] == '0':
            print('%d %s' % (irow, row['path'] + '.lsm'))
            endslice = int(row['processuptoframe'])
            if endslice == 0:
                endslice = 60
            run(os.path.join(wd, row['path'] + '.lsm'), ChPOI = CHPOI, ChMarker = CHMARKER,
                nSlices = NSLICES, nFrames = [1, endslice], thr_method1 = row['method'],
                aniDiff = ANIDIFF, filter_size = FILTER_SIZE, exclude = int(row['exclude']))
            wd = WaitForUserDialog('Press OK if cells if correct')
            wd.show()
            if wd.escPressed():
                break