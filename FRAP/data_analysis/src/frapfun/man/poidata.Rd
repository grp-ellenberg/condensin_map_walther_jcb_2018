% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/poidata.R
\docType{data}
\name{poidata}
\alias{poidata}
\title{Profile along mitotic chromomsomes as extracted from the image analysis.}
\format{A data frame with several rows and 41 variables:
\describe{
    \item{V1}{image index number}
    \item{V2}{time point}
    \item{V3}{Z slice}
    \item{V4}{intensity at X position 1 along line}
    \item{V5}{intensity at X position 2 along line}
    \item{Vn}{intensity at X position n along line}}}
\usage{
poidata
}
\description{
Contains data intensity for the POI. Similar data is obtained for the chromatin and mask.
}
\keyword{datasets}
