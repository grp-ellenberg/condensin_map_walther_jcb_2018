%This scripts call 'fn_combine_results_all_regions function' that combines 
%results (overlaps between condensin1&2 and normalize correlation) of all 
%regions analyed into a single file and caculate mean and standard dev.

%Author: M. Julius Hossain, EMBL, Heidelberg (julius.hossain@embl.de)
%Last update: 2018_03_03: documentation added

clear all;
clc;
close all;
%Name(s) of sub-unit(s)
protName = {'NCAPH2gfpc67-chkGFPAF594_NCAPHhaloc29'};
%Name(s) of phase(s)
phaseName = {'prometa', 'ana_early'};
%Root directory containing results of individual regions
inDir  = fullfile('Z:\Julius\Test_Processing\Sted_double_color_data\Results', filesep);
%Output directory to store combined results
outDir = fullfile('Z:\Julius\Test_Processing\Sted_double_color_data\Combined_Parameters_2018_03_04', filesep);
%Process all types of data (marked by a flag - txt file is associated with
%each individual directory
procAll = 1; %Process all data if it set to 1. It overrides selRegFlag if it is set to 1
selRegFlag = 1; % Process particular region type (1 means it will only process all regions with type 1.
fn_combine_results_all_regions(inDir, outDir, protName, phaseName, procAll, selRegFlag);

