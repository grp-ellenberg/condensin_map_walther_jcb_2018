% This script performs colocalization analysis between two channels
% It first segments each channels by combining both local and global
% thresholding approaches. It computes percentage of overalpping condensin
% regions between two channels. It also calculate normalized correlation
% between two channels

% Author: Julius Hossain, EMBL Heidelberg (julius.hossain@embl.de)
% Last update: 2017_10_20

% This script takes condensin1 and condensin2 data (registered and shift 
% corrected) as input. It also takes the manual segmented region of interest
% as binary mask


clc;
clear all;

% Input root directory
inDirRoot = fullfile('Z:\Julius\Test_Processing\Sted_double_color_data\Results', filesep);
%Get the root directory of sour code
[src_root_dir,~,~]=fileparts(pwd);
%Add path of related common functions required
addpath(fullfile(src_root_dir, 'common_functions'));
addpath(fullfile(src_root_dir, 'external_functions'));

%Get option running option and parameters values
[options] = sted_double_color_options_params();

% Get the name of all condensin regions
regionDir = dir([inDirRoot '*Drift*']);
%Process all regions regions one by one
for regionDirIdx = 1: length(regionDir)
    if regionDir(regionDirIdx).isdir == 1
        inDirReg = fullfile(inDirRoot, regionDir(regionDirIdx).name, 'Registered', filesep);
        inDirSht = fullfile(inDirRoot, regionDir(regionDirIdx).name, 'Registered_ShiftCorr', filesep);
        inDirBinMask = fullfile(inDirRoot, regionDir(regionDirIdx).name, 'Segmented_BinMask', filesep);
        
        disp(['Processing_RegionID = ' num2str(regionDirIdx) ': ' inDirReg]);
        
        fnListCon1 = dir([inDirReg '*Cond1-H*.tif']);  %Name of the input file - Condensin1
        fnListCon2 = dir([inDirSht '*Cond2-H2*.tif']); %Name of the input file - Condensin2
        
        if isempty(fnListCon2) || (length(fnListCon2) ~= length(fnListCon1)) || ~exist([inDirBinMask fnListCon1(1).name])
            disp('Input files are missing or number of files are different');
            continue;
        end
        
        %Creaate directories to store segmentation visualization results
        outDirParams  = fullfile(inDirRoot, regionDir(regionDirIdx).name, 'Params_seg', filesep);
        outDirDispImg = fullfile(inDirRoot, regionDir(regionDirIdx).name, 'DisImg_seg', filesep);
        
        xlsFilename = [outDirParams regionDir(regionDirIdx).name '.xls'];
        
        if ~exist(outDirParams)
           mkdir(outDirParams);
        end
        %mkdir(outDirParams);
        if ~exist(outDirDispImg)
            mkdir(outDirDispImg);
        end
        %mkdir(outDirDispImg);

        if ~exist(xlsFilename) || options.reproc == 1
            [con1StackOrig]   = fn_open_tiff_stack(inDirReg, fnListCon1(1).name);
            [con2StackOrig]   = fn_open_tiff_stack(inDirSht, fnListCon2(1).name);
            [con1BinMask]     = fn_open_tiff_stack(inDirBinMask, fnListCon1(1).name);
            
            con1Stack = con1StackOrig;
            con2Stack = con2StackOrig;
           
            con1StackCrop = con1StackOrig;
            con2StackCrop = con2StackOrig;
            con1StackCrop(con1BinMask == 0) = 0;
            con2StackCrop(con1BinMask == 0) = 0;
            %Get global thresholds to guide local thresholding
            [thrCon1Glb, ~] = ls_otsu_3d_image(con1StackCrop, 1);
            [thrCon2Glb, ~] = ls_otsu_3d_image(con2StackCrop, 1);
            
            con1StackFilt = con1Stack;
            con2StackFilt = con2Stack;
            
            %Apply gaussian filter in 2d
            for zplane = 1: size(con1Stack,3)
                con1StackFilt(:,:,zplane) = imgaussian(con1Stack(:,:,zplane), options.sigma, options.hSize);
                con2StackFilt(:,:,zplane) = imgaussian(con2Stack(:,:,zplane), options.sigma, options.hSize);
            end

            con1Region = zeros(size(con1Stack));
            con2Region = zeros(size(con2Stack));
            % Binarize images using both 2d and 3d thresholding
            for i = 1:size(con1Stack,3)
                con1Region(:,:,i) = fn_bradley_adapted(con1StackFilt(:,:,i), [options.windSizeCon options.windSizeCon], options.supPer, thrCon1Glb, options.bFactorGlb);
                con2Region(:,:,i) = fn_bradley_adapted(con2StackFilt(:,:,i), [options.windSizeCon options.windSizeCon], options.supPer, thrCon2Glb, options.bFactorGlb);
            end
            %%Detect percentage of colocalized voxels
            con1Stack(con1BinMask == 0) = 0;
            con2Stack(con1BinMask == 0) = 0;
            
            con1Region(con1BinMask == 0) = 0;
            con2Region(con1BinMask == 0) = 0;
            combConRegion = double(or(con1Region, con2Region));
            
            perDetCon1 = sum(con1Region(:))   /sum(con1BinMask(:)) * 100;
            perDetCon2 = sum(con2Region(:))   /sum(con1BinMask(:)) * 100;
            perDetComb = sum(combConRegion(:))/sum(con1BinMask(:)) * 100;
             
            perVoxCol = sum(sum(sum(double(and(con1Region, con2Region)))))/sqrt(sum(sum(sum(con1Region)))*sum(sum(sum(con2Region))))*100;
            
            
            %% Get normalized correlation of intensity between two channels           
            con1 = con1Stack;
            con2 = con2Stack;
            
            con1(combConRegion == 0) = 0;
            con2(combConRegion == 0) = 0;
            totIntCon1 = sum(sum(sum(con1)));
            totPosPixCon1 = sum(sum(sum(con1>0)));
            meanCon1 = totIntCon1/totPosPixCon1;
            con1 = con1-meanCon1;
            con1(combConRegion == 0) = 0;
            
            totIntCon2 = sum(sum(sum(con2)));
            totPosPixCon2 = sum(sum(sum(con2>0)));
            meanCon2 = totIntCon2/totPosPixCon2;
            con2 = con2-meanCon2;
            con2(combConRegion == 0) = 0;
            
            num = sum(sum(sum(con1.*con2)));
            denom = sqrt(sum(sum(dot(con1,con1)))*sum(sum(dot(con2,con2))));
            
            normCorr = num/denom;
            %% Save extracted parameters in xls file
            xlswrite(xlsFilename, {'RegionName'}, 1, 'A1');
            xlswrite(xlsFilename, {'PerDetCon1'}, 1, 'B1');
            xlswrite(xlsFilename, {'PerDetCon2'}, 1, 'C1');
            xlswrite(xlsFilename, {'PerDetComb'}, 1, 'D1');
            xlswrite(xlsFilename, {'PerVoxColoc'}, 1, 'E1');
            xlswrite(xlsFilename, {'NormCorr'}, 1, 'F1');
            
            xlswrite(xlsFilename, {regionDir(regionDirIdx).name}, 1, 'A2');
            xlswrite(xlsFilename, perDetCon1, 1, 'B2');
            xlswrite(xlsFilename, perDetCon2, 1, 'C2');
            xlswrite(xlsFilename, perDetComb, 1, 'D2');
            xlswrite(xlsFilename, perVoxCol , 1, 'E2');
            xlswrite(xlsFilename, normCorr  , 1, 'F2');
            
            matFilename = [xlsFilename(1:end-3) 'mat'];
            save(matFilename, 'perDetCon1', 'perDetCon2', 'perDetComb', 'perVoxCol', 'normCorr');
            
            %% Save images displaying segmentation results
            con1OrigProj = max(con1Stack, [], 3);
            con1OrigFilename = [outDirDispImg regionDir(regionDirIdx).name '_con1_orig.tif'];
            imwrite(uint8(con1OrigProj), con1OrigFilename, 'tif');
            
            con2OrigProj = max(con2Stack, [], 3);
            con2OrigFilename = [outDirDispImg regionDir(regionDirIdx).name '_con2_orig.tif'];
            imwrite(uint8(con2OrigProj), con2OrigFilename, 'tif');
            con1 = con1Stack;
            con1(con1Region == 0) = 0;
            con1Proj = max(con1, [], 3);
            con1Filename = [outDirDispImg regionDir(regionDirIdx).name '_con1_det.tif'];
            imwrite(uint8(con1Proj), con1Filename, 'tif');
            
            con2 = con2Stack;
            con2(con2Region == 0) = 0;
            con2Proj = max(con2, [], 3);
            con2Filename = [outDirDispImg regionDir(regionDirIdx).name '_con2_det.tif'];
            imwrite(uint8(con2Proj), con2Filename, 'tif');
            close all;
        end
    end
end

