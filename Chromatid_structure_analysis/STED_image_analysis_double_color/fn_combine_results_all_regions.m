function fn_combine_results_all_regions(inDir, outDir, protName, phaseName, procAll, selRegFlag)
%This scripts combines results (overlaps between condensin1&2 and normalize
%correlation) of all regions analyed into a single file and caculates mean 
%and standard deviation.

%Author: M. Julius Hossain, EMBL, Heidelberg (julius.hossain@embl.de)
%Last update: 2018_03_03: documentation added

%inDir: Input directory containing results of individual regions
%outDir: output directory to save combined results
%protName: name(s) of the proteins
%phaseName: name(s) of the mitotic stages
%procAll: 1 process all regions, 0 process selected regions
%selRegFlag: select on this type of regions if procAll is set to 1

%Create output directory if not exist
if ~exist(outDir)
    mkdir(outDir);
end

%Process regions based on sub-unit and mitotic phase
for protIdx = 1: length(protName)
    for phaseIdx = 1: length(phaseName)
        perDetCon1= [];
        perDetCon2=[];
        perDetComb=[];
        perNonOvlVox= [];
        normCorr = [];
        %Select all regions belogning to particular sub-unit and mitotic
        %phase
        regionDir = dir([inDir '*' protName{protIdx} '*' phaseName{phaseIdx} '*']);
        numRegions = 0;
        for regionDirIdx = 1: length(regionDir)
            if regionDir(regionDirIdx).isdir == 1
                curRegionDir = fullfile(inDir, regionDir(regionDirIdx).name, filesep);
                
                %Open the region type flag to get the catogory of the
                %region
                fpRegFlag = fopen([curRegionDir 'region_type_flag.txt'],'r');
                if fpRegFlag == -1
                    continue;
                else
                    curLine = fgets(fpRegFlag);
                    regFlag = sscanf(curLine, '%d');
                end
                
                %Process particular region type if procAllFlag is set to 1.
                %Otherwise process all the regions
                if (procAll == 0 && regFlag ==selRegFlag) || (procAll == 1 && regFlag >0)
                    
                    disp(['Processing: ' curRegionDir]);
                    fnListMat = [regionDir(regionDirIdx).name '.mat'];
                                  
                    curMat = load([curRegionDir 'Params_seg' filesep fnListMat]);
                    numRegions = numRegions + 1;
                    perDetCon1(numRegions) = curMat.perDetCon1;
                    perDetCon2(numRegions) = curMat.perDetCon2;
                    perDetComb(numRegions) = curMat.perDetComb;
                    perNonOvlVox(numRegions) = 100 - curMat.perVoxCol; % This is just to get nor overlapping percentage of nonOverlapping voxels
                    normCorr(numRegions)   = curMat.normCorr;
                    
                end
            end
        end
        
        if numRegions>0
            meanPerDetCon1 = mean(perDetCon1);
            meanPerDetCon2 = mean(perDetCon2);
            meanPerDetComb = mean(perDetComb);
            meanPerNonOvlVox  = mean(perNonOvlVox);
            meanNormCorr   = mean(normCorr);
                        
            stdPerDetCon1 = std(perDetCon1);
            stdPerDetCon2 = std(perDetCon2);
            stdPerDetComb = std(perDetComb);
            stdPerNonOvlVox = std(perNonOvlVox);
            stdNormCorr   = std(normCorr);
        end
        
        %outDir = inDir;
        xlsFileName = [protName{protIdx} '_' phaseName{phaseIdx} '.xls'];
        
        %xlswrite([outDir xlsFileName], {'Mean_Params'}, 1, strcat('A', num2str(1)));
        xlswrite([outDir xlsFileName], {'Prot_Phase_Name'}, 1, 'A1');
        xlswrite([outDir xlsFileName], {'PerDetCon1'}, 1, 'B1');
        xlswrite([outDir xlsFileName], {'PerDetCon2'}, 1, 'C1');
        xlswrite([outDir xlsFileName], {'PerDetComb'}, 1, 'D1');
        xlswrite([outDir xlsFileName], {'PerVoxNonOvlVox'  }, 1, 'E1');
        xlswrite([outDir xlsFileName], {'NormCorrTop'  }, 1, 'F1');
        
        xlswrite([outDir xlsFileName], {'MeanPerDetCon1'}, 1, 'G1');
        xlswrite([outDir xlsFileName], {'MeanPerDetCon2'}, 1, 'H1');
        xlswrite([outDir xlsFileName], {'MeanPerDetComb'}, 1, 'I1');
        xlswrite([outDir xlsFileName], {'MeanPerNonOvlVox'}, 1, 'J1');
        xlswrite([outDir xlsFileName], {'MeanNromCorr'  }, 1, 'K1');
        
        xlswrite([outDir xlsFileName], {'StdevPerDetCon1'}, 1, 'L1');
        xlswrite([outDir xlsFileName], {'StdevPerDetCon2'}, 1, 'M1');
        xlswrite([outDir xlsFileName], {'StdevPerDetComb'}, 1, 'N1');
        xlswrite([outDir xlsFileName], {'StdevPerNonOvlVox'  }, 1, 'O1');
        xlswrite([outDir xlsFileName], {'StdevNormCorr'  }, 1, 'P1');
        xlswrite([outDir xlsFileName], {'NumRegions'  }, 1, 'Q1');
        
        xlswrite([outDir xlsFileName], {[protName(protIdx) phaseName(phaseIdx)]}, 1, 'A2');
        xlswrite([outDir xlsFileName], perDetCon1', 1, ['B2:B' num2str(numRegions + 1)]);
        xlswrite([outDir xlsFileName], perDetCon2', 1, ['C2:C' num2str(numRegions + 1)]);
        xlswrite([outDir xlsFileName], perDetComb', 1, ['D2:D' num2str(numRegions + 1)]);
        xlswrite([outDir xlsFileName], perNonOvlVox', 1, ['E2:E' num2str(numRegions + 1)]);
        xlswrite([outDir xlsFileName], normCorr',   1, ['F2:F' num2str(numRegions + 1)]);
        
        xlswrite([outDir xlsFileName], meanPerDetCon1, 1, 'G2');
        xlswrite([outDir xlsFileName], meanPerDetCon2, 1, 'H2' );
        xlswrite([outDir xlsFileName], meanPerDetComb, 1, 'I2' );
        xlswrite([outDir xlsFileName], meanPerNonOvlVox,  1, 'J2' );
        xlswrite([outDir xlsFileName], meanNormCorr,   1, 'K2' );
        
        xlswrite([outDir xlsFileName], stdPerDetCon1, 1, 'L2');
        xlswrite([outDir xlsFileName], stdPerDetCon2, 1, 'M2');
        xlswrite([outDir xlsFileName], stdPerDetComb, 1, 'N2');
        xlswrite([outDir xlsFileName], stdPerNonOvlVox, 1, 'O2');
        xlswrite([outDir xlsFileName], stdNormCorr,   1, 'P2');
        xlswrite([outDir xlsFileName], numRegions,   1, 'Q2');
        
        matFilename = [xlsFileName(1:end-3) 'mat'];
        save([outDir matFilename], 'meanPerDetCon1', 'meanPerDetCon2', 'meanPerDetComb', 'meanPerNonOvlVox', 'meanNormCorr', 'stdPerDetCon1', 'stdPerDetCon2', 'stdPerDetComb', 'stdPerNonOvlVox', 'stdNormCorr', 'numRegions');
        
    end
end
end