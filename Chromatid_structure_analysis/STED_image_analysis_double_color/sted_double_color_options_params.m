function [opt] = sted_double_color_options_params()
opt.reproc = 1;        % 1:reprocess the data and 0: not 
opt.hSize = 2;         % Sigma for Gaussian filter
opt.sigma = 1.5;       % Kernel size of the filter
opt.bFactorGlb = 0.35; % Contribution for 2D and 3D threshold
opt.windSizeCon = 9;   % window size for local thresholding
opt.supPer = -40;      % Percentage to suppress background intensity
end