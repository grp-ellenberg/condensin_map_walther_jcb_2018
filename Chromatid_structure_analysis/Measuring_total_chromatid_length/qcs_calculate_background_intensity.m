function [avgBackInt, embRegionSliceRim, maxZIndex] = qcs_calculate_background_intensity(embryo, cytStack, dil1, dil2)
%This function calculate the background intensity of embryo
embryo(embryo>0) = 1;

sumZSlice = sum(sum(embryo));
[~, maxZIndex] = max(sumZSlice);
%maxZIndex = 39;

embRegionSlice = embryo(:,:,maxZIndex);
embRegionSliceDil1 = imdilate(embRegionSlice, strel('diamond',dil1));
embRegionSliceDil2 = imdilate(embRegionSlice, strel('diamond',dil2));
embRegionSliceRim = imsubtract(embRegionSliceDil1, embRegionSliceDil2);

embImgSliceRim = cytStack(:,:,maxZIndex);
embImgSliceRim(embRegionSliceRim(:,:) == 0 ) = 0;

% totalInt = sum(sum(embImgSliceRim));
% totalPix = sum(sum(embRegionSliceRim>0));
% avgBackInt = totalInt/totalPix;

[xSize, ySize] = size(embImgSliceRim);
maxVal = max(max(embImgSliceRim));
histN = round(maxVal)+1;
histogram = zeros(1,histN);

for i = 1:xSize
    for j = 1:ySize
        hIndex = round(embImgSliceRim(i,j))+1;
        histogram(hIndex) = histogram(hIndex)+1;
    end
end
histogram(1) = 0;

totalPix = sum(histogram);
startIdx = 1;
for startIdx = 1:histN
    if(sum(histogram(1:startIdx))>= totalPix*0.1)
        break;
    end
end

for endIdx = histN:-1:1
    if(sum(histogram(endIdx:end))>= totalPix*0.5)
        break;
    end
end

totalPix = sum(histogram(startIdx:endIdx));
totalInt = 0;
for i = startIdx:endIdx
    totalInt = totalInt + histogram(i) *i;
end
avgBackInt = totalInt/totalPix;
end

