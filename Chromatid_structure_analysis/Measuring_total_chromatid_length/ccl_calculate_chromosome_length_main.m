%This script caculates total length of chromatid in a cell
%Author: M. Julius Hossain, EMBL
%Last update: 2017_09_14


%Get the source directory
[src_root_dir,~,~]=fileparts(pwd);

%Include directories containing necessary files
addpath(fullfile(src_root_dir, 'common_functions'));
addpath(fullfile(src_root_dir, 'external_functions'));
addpath(fullfile(src_root_dir, 'external_functions', 'imreadBF_2'));
%addpath(fullfile(src_root_dir, 'external_functions', 'bfmatlab'));
path = fullfile(src_root_dir, 'external_functions', 'bfmatlab','loci_tools.jar');
javaaddpath(path);

% Root directory containing input data
inDirRoot = 'Z:\Nike\LSM880AiryFast\20170418_HK_SMC4-mEGFP#82_68_50nMSiR-Hoechst_RT_live_40xWater\C-Apochromat 40x 1.2 W Korr FCS M27 NEW OBJECTIVE\RegionsToSegment\';
% Root directory to save the output files
outDirRoot = fullfile('Z:\Julius\Test_Processing\total_chromatid_length', filesep);

%Get running options and parameters
[opt_manseg] = manseg_options_params();

% Get the directories containing selected regions (directory name contains 
% (c)cell number and (r)region number
dirList = dir([inDirRoot '*_c*_r*']);

%For each region call function to calculate total chromatid length
dirIndex = 1;
for i = 1: length(dirList)
    if dirList(i).isdir == 1
        ccl_calculate_chromosome_length(fullfile(inDirRoot, dirList(i).name, filesep), outDirRoot, dirIndex, opt_manseg);
        dirIndex = dirIndex + 1;
    end
end