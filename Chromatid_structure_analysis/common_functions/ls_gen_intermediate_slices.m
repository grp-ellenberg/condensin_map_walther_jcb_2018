function intVolume= ls_gen_intermediate_slices(volume, zFactor)
%This function generates intermediate slices to convert an anisotripic 
%stack to isotropy. It uses simple linear interpolation

%volume: input volume
%zFactor: the number times plus one to be interpolated in between
%inVolume: interpolated volume

%Author: M. Julius Hossain, EMBL, Heidelberg

%Get the size
[dx, dy, dz] = size(volume);

%Get the number of slice in interpolated volume
newDz = dz * zFactor - zFactor + 1;
intVolume = zeros(dx,dy,newDz);

%Assign the original slices
for i = 1: dz
    intVolume(:,:,i*zFactor-zFactor +1) = volume(:,:,i);
end
%Generate the intermediate slices
for i = 1:dz-1
    for j = 2:zFactor
        intVolume(:,:,i*zFactor-zFactor +j) = intVolume(:,:,i*zFactor-zFactor +1) *(1-(j-1)/zFactor) + intVolume(:,:,i*zFactor +1) * (j-1)/zFactor; 
    end
end