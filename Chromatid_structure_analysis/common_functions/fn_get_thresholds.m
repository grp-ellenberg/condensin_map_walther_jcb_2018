function [thresh3D, thresh2D] = fn_get_thresholds(stack, masked)
% This function calculate 3D and 2D threshold of the stack
% stack: input stack
% masked: 1: exclude background intensity, 0 otherwise
Nz = size(stack, 3);
thresh2D = zeros(Nz,1);
[thresh3D, ~] = ls_otsu_3d_image(stack, masked);

%Update threshold for different slices
for i = 1:Nz
    thresh2D(i) = ls_otsu_2d_image(stack(:,:,i), masked);
end
thresh2D = smooth(thresh2D, 5); % smooth 2D threshold values
end