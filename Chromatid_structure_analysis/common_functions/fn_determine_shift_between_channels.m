function [xShift, yShift, zShift] = fn_determine_shift_between_channels(stackRef, stackVar, maxShiftXY, maxShiftZ)
%This function calculates the shift between two channels
%stackRef: Reference input stack
%stackVar: Input stack to be shifted
%maxShiftXY: maximum amount shift in xy to be checked 
%maxShiftZ: maximum amount of shift in z to be tested

%Author: M. Julius Hossain, EMBL, Heideblerg
%Last update: 2018-03-06

stackVarTemp = zeros(size(stackVar));
maxTotInt = 0;
xShift = 0;
yShift = 0;
zShift = 0;

for curXShift = -maxShiftXY:maxShiftXY
    for curYShift = -maxShiftXY:maxShiftXY
        for curZShift = -maxShiftZ:maxShiftZ
            stackVarTemp(:,:,:) = 0;
            if curXShift >= 0 && curYShift >=0 && curZShift>=0
                stackVarTemp(1:end-curXShift,1:end-curYShift,1:end-curZShift) = stackVar(1+curXShift:end,1+curYShift:end,1+curZShift:end);
            elseif curXShift < 0 && curYShift >=0 && curZShift>=0
                stackVarTemp(1-curXShift:end,1:end-curYShift,1:end-curZShift) = stackVar(1:end+curXShift,1+curYShift:end,1+curZShift:end);
            elseif curXShift < 0 && curYShift <0 && curZShift>=0
                stackVarTemp(1-curXShift:end,1-curYShift:end,1:end-curZShift) = stackVar(1:end+curXShift,1:end+curYShift,1+curZShift:end);
            elseif curXShift < 0 && curYShift <0 && curZShift<0
                stackVarTemp(1-curXShift:end,1-curYShift:end,1-curZShift:end) = stackVar(1:end+curXShift,1:end+curYShift,1:end+curZShift);
            elseif curXShift >=0 && curYShift <0 && curZShift<0
                stackVarTemp(1:end-curXShift,1-curYShift:end,1-curZShift:end) = stackVar(1+curXShift:end,1:end+curYShift,1:end+curZShift);
            elseif curXShift >=0 && curYShift >=0 && curZShift<0
                stackVarTemp(1:end-curXShift,1:end-curYShift,1-curZShift:end) = stackVar(1+curXShift:end,1+curYShift:end,1:end+curZShift);
            elseif curXShift >= 0 && curYShift <0 && curZShift>=0
                stackVarTemp(1:end-curXShift,1-curYShift:end,1:end-curZShift) = stackVar(1+curXShift:end,1:end+curYShift,1+curZShift:end);
            elseif curXShift < 0 && curYShift>0 && curZShift<0
                stackVarTemp(1-curXShift:end,1:end-curYShift,1-curZShift:end) = stackVar(1:end+curXShift,1+curYShift:end,1:end+curZShift);
            end
            multVol = immultiply(stackVarTemp, stackRef);
            curTotMultInt = sum(sum(sum(multVol)));
            
            if curTotMultInt > maxTotInt
                maxTotInt = curTotMultInt;
                xShift = curXShift;
                yShift = curYShift;
                zShift = curZShift;
            end
        end
    end
end
clear stackVarTemp;
end

