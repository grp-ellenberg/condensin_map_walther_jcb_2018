function [outStack] = ls_smooth_and_equalize(bwStack, refVol, voxelSize)
%This function smooth 3D binary stack and ruture a binary volume with
%similar volume

%bwStack: input binary stack
%refVol: reference volume of bwStack
%voxelSize: size of the voxel

%Author: M. Julius Hossain, EMBL, Heidelberg

smoothStack  = smooth3(bwStack(:,:,:) , 'box', 9);
outStack = double(smoothStack(:,:,:) >= 0.6);
outVol = sum(sum(sum(outStack))) *voxelSize;
thDist = 0.59;
while outVol <= refVol *0.995 && thDist >= 0.01
    outStack = double(smoothStack(:,:,:) >= thDist);
    outVol = sum(sum(sum(outStack))) *voxelSize;
    thDist = thDist -0.01;
end
end

