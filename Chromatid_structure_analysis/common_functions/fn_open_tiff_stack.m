function [imageStack] = fn_open_tiff_stack(fp, fn)
% This function loads a tiff stack
% fp: input directory of the tiff stack;
% fn: name of the tiff file

%Extract the number of frames from tiff file
fullFilename = fullfile(fp, fn);
imageInfo=imfinfo(fullFilename);
numSlices=length(imageInfo);

%Allocate memory for the stack
imSize=[imageInfo(1).Height, imageInfo(1).Width, numSlices];
imageStack=zeros(imSize);
%Load the stack
for zIdx=1:numSlices
    imageStack(:,:,zIdx) = imread(fullFilename, zIdx);
end
end