%This script combines the results from different regions and generate 
%radial profile per sub-unit and mitotic phase

%Author: Julis Hossain, EMBL, Heideblerg
%Last update: 2018-03-05

clear all;
clc;
close all;

%Get the root directory of sour code
[src_root_dir,~,~]=fileparts(pwd);
%Add path of related common functions required
addpath(fullfile(src_root_dir, 'common_functions'));
addpath(fullfile(src_root_dir, 'external_functions','fwhm'));

%mean FWHM width of DNA
meanFwhmWidthChr_nm = 1114.679501; %Considering the mean of all DNA - this
%value is obtained from the fwhm value of all chromosome imaged

%meanFwhmWidthChr_nm =1102.331578; %Considering only the mean of the DNA 
%belonging to the sub-units used in the manuscript

%Name of different proteins
protName = {'gfpTOP2Ac102', 'gfpKIF4Ac173', 'NCAPD2gfpc272c78', 'NCAPD3gfpc16', 'NCAPH2gfpc67', 'NCAPHgfpc86', 'SMC4gfpz82z68', 'gfpNCAPH2c1'};
%Name of different sub-units
phaseName = {'prometa', 'ana_early'};

%Input directory save the results
inDir  = fullfile('Z:\Julius\Test_Processing\Sted_single_color_data\Results', filesep);

%Root directory to save the results
outDirRoot = 'Z:\Julius\Test_Processing\Sted_single_color_data\Extracted_parameters\Results_rad_prof';

outDir = fullfile(outDirRoot, 'Regions_Flag4', filesep);
procAll = 0;
selRegFlag = 4;
%try
fn_combine_results_prot_phase_radial_profile(inDir, outDir, protName, phaseName, procAll, selRegFlag, meanFwhmWidthChr_nm);
% catch
%     disp('Terminated Flag 4');
% end

% Do the same if you want to process regions marked with different flag