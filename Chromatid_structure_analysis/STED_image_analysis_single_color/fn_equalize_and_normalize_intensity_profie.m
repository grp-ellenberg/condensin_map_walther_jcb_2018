function [outSig] = fn_equalize_and_normalize_intensity_profie(signal)

dx = length(signal);

sigCent = ceil(dx/2);

halfSig = zeros(sigCent,1);

subt = 0;
for i =sigCent:dx
    halfSig(i-sigCent+1) = (signal(i) + signal(sigCent-subt))/2;
    subt = subt + 1;
end

outSig = zeros(size(signal));
outSig(sigCent:dx) = halfSig;
outSig(1:sigCent) = fliplr(halfSig');

maxVal = max(outSig);
outSig = outSig/maxVal;
end