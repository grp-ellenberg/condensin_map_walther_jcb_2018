function [options] = reg_options()
%This contains the options/parameters of registration
options.bgInt = 32768; % To correct the offset of STED intensity
options.refZIdx = 11; % Change it to the middle of the slice if suits better
%Size of voxels in x and y
options.voxelSizeX = 20; % In nanometer
options.voxelSizeY = 20; 
options.overWrite = 1; % 1: reprocess and save the results, 0 otherwise
end

