%This script registers different slices of a stack with respect to a 
%reference slice 

clear all;

%Get the root directory of sour code
[src_root_dir,~,~]=fileparts(pwd);
[src_root_dir,~,~]=fileparts(src_root_dir); %Go one more stage up
%Add path of related common functions required
addpath(fullfile(src_root_dir, 'common_functions'));
%Get options/parameters
[options] = reg_options();

%Input directory
inDir = fullfile('Z:\Julius\Test_Processing', filesep);

driftDir = dir([inDir '*Drift*']);

for driftDirIdx = 1: length(driftDir)
    if driftDir(driftDirIdx).isdir == 1
        protDir  =  dir([inDir driftDir(driftDirIdx).name]);
        for protDirIdx = 1: length(protDir)
            if protDir(protDirIdx).isdir == 1 && length(protDir(protDirIdx).name)>=4
                regDir  =  dir([inDir driftDir(driftDirIdx).name filesep protDir(protDirIdx).name]);
                for regDirIdx = 1: length(regDir)
                    if regDir(regDirIdx).isdir == 1 && length(regDir(regDirIdx).name)>=4
                        fullInDir = fullfile(inDir, driftDir(driftDirIdx).name, protDir(protDirIdx).name, regDir(regDirIdx).name, filesep);
                         disp(['Processing: ' fullInDir]);
                        fnListChr = dir([fullInDir '*Chr*.tif']); %Name of the input file - Dna
                        fnListCon = dir([fullInDir '*Cond*.tif']); %Name of the input file - Condensin
                        
                        if isempty(fnListChr) || (length(fnListChr) ~= length(fnListCon))
                            disp('No input files or number of files are different for condensin and chromatin is different');
                            continue;
                        end
                        
                        fsIdx = find(fullInDir == filesep, 4, 'last');
                        outDir = fullfile(fullInDir(1:fsIdx(1)), 'Results', [fullInDir(fsIdx(1)+1:fsIdx(2)-1) '_' fullInDir(fsIdx(2)+1:fsIdx(3)-1) '_' fullInDir(fsIdx(3)+1:fsIdx(4))]);
                        outDirReg = fullfile(outDir, 'Registered', filesep);
                        
                        if ~exist(outDirReg)
                            mkdir(outDirReg);
                        else
                            if exist([outDirReg fnListCon(1).name]) && exist([outDirReg fnListChr(1).name]) && options.overWrite == 0
                                disp(['Already processed: ' fullInDir]);
                                continue;
                            end
                        end
                                               
                        [chrStackOrig] = fn_open_tiff_stack(fullInDir, fnListChr(1).name);
                        [conStackOrig] = fn_open_tiff_stack(fullInDir, fnListCon(1).name);
                        chrStackOrig = chrStackOrig - options.bgInt;
                        conStackOrig = conStackOrig - options.bgInt;
                        dx = size(conStackOrig, 1);
                        dy = size(conStackOrig, 2);
                        NzOrig=size(conStackOrig, 3); %number of Z slices in lsm file
                        %disp('Image stacks have been loaded');
                        try
                        [regConStack, regChrStack] = fn_register_all_slices(conStackOrig, chrStackOrig, options.refZIdx, options.voxelSizeX, options.voxelSizeY);
                        
                        flag = fn_write_tiff_stack(regConStack, outDirReg, fnListCon(1).name, 16);
                        flag = fn_write_tiff_stack(regChrStack, outDirReg, fnListChr(1).name, 16);
                        catch
                            disp('Could not register the region');
                        end
                    end
                end
            end
        end
    end
end
