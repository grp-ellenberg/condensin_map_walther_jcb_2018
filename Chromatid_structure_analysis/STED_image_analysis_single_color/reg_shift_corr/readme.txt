This folder contains codes related to shift correction between two 
channels and drift correction between different slices in a stack. The
codes follow the directory structure (for input and output) that has been
used to process the data for the manuscript 