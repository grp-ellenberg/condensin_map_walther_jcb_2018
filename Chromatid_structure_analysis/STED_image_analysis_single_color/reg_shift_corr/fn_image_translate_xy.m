function [tImgVol] = fn_image_translate_xy(imgVol, tVect)
%This function translate a volume by a given vector
%Version date: 2015-02-16
%Author: Julius Hossain, EMBL, Heideberg

tImgVol = zeros(size(imgVol));
xShift = round(tVect(1));
yShift = round(tVect(2));

if xShift <0
    tImgVol(1:end+xShift, :, :) = imgVol(1-xShift:end,:,:);
elseif xShift >0
    tImgVol(xShift+1:end, :, :) = imgVol(1:end-xShift,:,:);
end

if xShift ~= 0
    imgVol = tImgVol;
    tImgVol(:,:,:) = 0;
end
if yShift <0
    tImgVol(:, 1:end+yShift,:) = imgVol(:,1-yShift:end,:);
elseif yShift >0
    tImgVol(:, yShift+1:end,:) = imgVol(:, 1:end-yShift,:);
end
if yShift == 0 
    tImgVol = imgVol;
end
end