function fn_combine_results_prot_phase_radial_profile(inDir, outDir, protName, phaseName, procAll, selRegFlag, meanFwhmWidthChr_nm)
%This function combines the results from individual cell regions and
%caculate radial profile for each subunit per mitotic stage
%inDir: input directory
%outDir: output directoy
%protName: name of the protein
%phaseName: name of the mitotic phase
%procAll: 1: process all regions
%selRegFlag: Process region with particular flag type

%clear all;
close all;
crosRad = 80;
voxelSizeX = 20;
isoDx = 71;
isoDy = 71;
addUpLenPix = 2000/voxelSizeX; % number of pixels in two micrometer dist
refWidthChr_nm = 600; % Ref width to look at intensity distributions inside


if ~exist(outDir)
    mkdir(outDir);
end

for protIdx = 1: length(protName)
    for phaseIdx = 1: length(phaseName)
        meanTotInt = [];
        meanTotIntChr = [];
        meanTotIntSeg = [];
        meanWidthCon  = [];
        meanWidthConSeg = [];
        meanWidthChr = [];
        meanWidthConSecond = [];
          
        conCrosDist = zeros(2*crosRad+1, 2*crosRad+1);
        %chrCrosDist = zeros(2*crosRad+1, 2*crosRad+1);
        totProjCon = zeros(2*crosRad+1,1);
        totProjChr = zeros(2*crosRad+1,1);
        totProjConSeg = zeros(2*crosRad+1,1);
        hmfwChr = [];
        hmfwCon = [];
        regionDir = dir([inDir '*' protName{protIdx} '*' phaseName{phaseIdx} '*']);
        numRegions = 0;
        disp(['Processing flag:' num2str(selRegFlag) ' Prot_' protName{protIdx} ' Phase_' phaseName{phaseIdx}]);
        for regionDirIdx = 1: length(regionDir)
            if regionDir(regionDirIdx).isdir == 1
                curRegionDir = fullfile(inDir, regionDir(regionDirIdx).name, filesep);
                fpRegFlag = fopen([curRegionDir 'region_type_flag.txt'],'r');
                if fpRegFlag == -1
                    continue;
                else
                    curLine = fgets(fpRegFlag);
                    regFlag = sscanf(curLine, '%d');
                end
                
                if (procAll == 0 && regFlag ==selRegFlag) || (procAll == 1 && regFlag >0)
                    
                    fnListMat = dir([curRegionDir 'Extracted_Params' filesep regionDir(regionDirIdx).name '.mat']) ; %Name of the input file - chromatin
                    fnConListTif = dir([curRegionDir 'Disp_Stack_up' filesep '*Cond*.tif']) ; %Name of the input file - chromatin
                    fnChrListTif = dir([curRegionDir 'Disp_Stack_up' filesep '*Chr*.tif']) ; %Name of the input file - chromatin
                    
                    if length(fnListMat)>0 && (length(fnConListTif)>0 && length(fnChrListTif)>0)
                        curMat = load([curRegionDir 'Extracted_Params' filesep fnListMat(1).name]);
                        
                        numRegions = numRegions + 1;
                        meanTotInt(numRegions)    = curMat.meanTotInt;
                        meanTotIntChr(numRegions) = curMat.meanTotIntChr;
                        meanTotIntSeg(numRegions) = curMat.meanTotIntSeg;
                        meanWidthCon(numRegions)  = curMat.meanWidthCon;
                        meanWidthConSeg(numRegions) = curMat.meanWidthConSeg;
                        meanWidthChr(numRegions)  = curMat.meanWidthChr;
                        meanWidthConSecond(numRegions) = curMat.meanWidthConSecond;
                                               
                        %Shifting the values to the peak before adding up
                        [~,idxCon] = max(curMat.totProjCon);
                        shiftedCon = circshift(curMat.totProjCon, ceil(length(curMat.totProjCon)/2)-idxCon);
                        totProjCon = totProjCon + shiftedCon;
                        
                        [~,idxConSeg] = max(curMat.totProjConSeg);
                        shiftedConSeg = circshift(curMat.totProjConSeg, ceil(length(curMat.totProjConSeg)/2)-idxConSeg);
                        totProjConSeg = totProjConSeg + shiftedConSeg;
                        
                        [~,idxChr] = max(curMat.totProjChr);
                        shiftedChr = circshift(curMat.totProjChr, ceil(length(curMat.totProjChr)/2)-idxChr);
                        totProjChr = totProjChr + shiftedChr;
                        
                        %%Code portion related to periodicity has been removed
                        
                        [conStack]   = fn_open_tiff_stack([curRegionDir 'Disp_Stack_up' filesep], fnConListTif(1).name);
                        [chrStack]   = fn_open_tiff_stack([curRegionDir 'Disp_Stack_up' filesep], fnChrListTif(1).name);
                        
                        conSlice = squeeze(sum(conStack, 2));
                        conSlice = permute(conSlice, [2 1]);
                        conSliceNorm = conSlice * (2000/(voxelSizeX*size(conStack,2)));
                        
                        %initCentCon = centerOfMass(conSliceNorm);
                        %meanWidthCon = paramsMat.meanWidthCon;
                        %meanWidthCon = meanHmfwCon; %%modified on 2017_11_10
                        curRadConX = round((meanFwhmWidthChr_nm *0.6)/(2*voxelSizeX));
                        curRadConY = round((meanFwhmWidthChr_nm *0.4)/(2*voxelSizeX));

                        [xShift, yShift] = fn_determine_shift_for_refined_centre_of_mass(conSliceNorm, ceil(size(conSliceNorm,1)/2), ceil(size(conSliceNorm,2)/2), curRadConX, curRadConY, 8, 8);
                        conSliceShifted = circshift(conSliceNorm, -xShift);
                        conSliceShifted = circshift(conSliceShifted, -yShift, 2);
                        conCrosDist = conCrosDist + conSliceShifted;
                        %profCon = max(conSlice1);
                        profCon = sum(conSlice);
                        
                        chrSlice = squeeze(sum(chrStack, 2));
                        chrSlice = permute(chrSlice, [2 1]);
                        %chrSliceNorm = chrSlice * (2000/(voxelSizeX*size(chrStack,2)));
                        %chrCrosDist = chrCrosDist + chrSlice;
                        %profChr = max(chrSlice1);
                        profChr = sum(chrSlice);
                        
                        lenConPix = size(conStack,2);
                        if lenConPix < addUpLenPix
                            hmfwCon(numRegions) = fwhm(1:length(profCon),profCon);
                            hmfwChr(numRegions) = fwhm(1:length(profChr),profChr);
                        else
                            lenCon = 0;
                            lenChr = 0;
                            numSeg = 0;
                            for axIdx = 1: 10: lenConPix-addUpLenPix +1
%                                 disp(axIdx);
                                conSlicePerMic = squeeze(sum(conStack(:,axIdx:axIdx+addUpLenPix-1,:), 2));
                                conSlice1PerMic = permute(conSlicePerMic, [2 1]);
                                profConPerMic = sum(conSlice1PerMic);
                                lenCon = lenCon + fwhm(1:length(profConPerMic),profConPerMic);
                                
                                chrSlicePerMic = squeeze(sum(chrStack(:,axIdx:axIdx+addUpLenPix-1,:), 2));
                                chrSlice1PerMic = permute(chrSlicePerMic, [2 1]);
                                profChrPerMic = sum(chrSlice1PerMic);
                                lenChr = lenChr + fwhm(1:length(profChrPerMic),profChrPerMic);
                                numSeg = numSeg + 1;
                            end
                            lenCon = lenCon/numSeg;
                            lenChr = lenChr/numSeg;
                            hmfwCon(numRegions) = lenCon;
                            hmfwChr(numRegions) = lenChr;
                        end
                    end
                end
            end
        end
        
        if numRegions>0
                 
            meanMeanWidthCon = mean(meanWidthCon);
            stdMeanWidthCon  = std(meanWidthCon);
            
            meanMeanWidthConSeg = mean(meanWidthConSeg);
            stdMeanWidthConSeg = std(meanWidthConSeg);
            
            meanMeanWidthChr = mean(meanWidthChr);
            stdMeanWidthChr  = std(meanWidthChr);
            
            
            totProjCon = totProjCon/numRegions;
            totProjConEq = fn_equalize_and_normalize_intensity_profie(totProjCon);
            
            totProjConSeg = totProjConSeg/numRegions;
            totProjConSegEq = fn_equalize_and_normalize_intensity_profie(totProjConSeg);
            
            totProjChr = totProjChr/numRegions;
            totProjChrEq = fn_equalize_and_normalize_intensity_profie(totProjChr);
            
            %%Detecting peak and then cut it off
            
            %%
            lenProf = length(totProjConEq);
            x = -floor(lenProf/2):floor(lenProf/2);
            x = x * voxelSizeX;
            h = figure('NumberTitle','off', 'Visible','off'); plot(x,totProjConEq, 'LineWidth',2);
            xlabel('Distance from condensin axis (nm)');
            ylabel('Normalized intensity (au)');
            filename = [outDir  protName{protIdx} '_' phaseName{phaseIdx} '_norm_sum_int_prof.tif'];
            saveas(h, filename, 'tif');
            close all;
            
            %conMaxWidth_nm = 591.5056769; %gfpTOP2Ac102-ana-early
            %halfRefWidthChr_pix = round((meanFwhmWidthChr_nm*0.6)/(voxelSizeX*2)); 
            halfRefWidthChr_pix = round((refWidthChr_nm)/(voxelSizeX*2)); 
            %conMinWidth_nm = 311.593054; %NCAPD3gfpc16-prometa
            %halfConMinWidth_pix = round(conMinWidth_nm/(voxelSizeX*2));
            refPerInt = 90;
            perIntInsideRefChrWidth = sum(totProjCon(ceil(lenProf/2)-halfRefWidthChr_pix:ceil(lenProf/2)+halfRefWidthChr_pix))/sum(totProjCon);
            
            
            for curHalfWidth_pix = 1: ceil(lenProf/2)
                totInt = sum(totProjCon(ceil(lenProf/2)-curHalfWidth_pix:ceil(lenProf/2)+curHalfWidth_pix));
                if totInt >= sum(totProjCon) * refPerInt/100
                    widthCapRefPerInt_nm = curHalfWidth_pix * voxelSizeX * 2;
                    break;
                end
            end
            
            %Renaming the variables before saving into the mat files

            meanWidthCon = meanMeanWidthCon;
            stdWidthCon = stdMeanWidthCon;
            
            meanWidthConSeg = meanMeanWidthConSeg;
            stdWidthConSeg = stdMeanWidthConSeg;
            
            meanWidthChr = meanMeanWidthChr;
            stdWidthChr  = stdMeanWidthChr;
            
            %outDir = inDir;
            xlsFileName = [protName{protIdx} '_' phaseName{phaseIdx} '.xls'];
            
            xlswrite([outDir xlsFileName], {'Cond_MeanWidth'}, 1, strcat('A', num2str(1)));
            xlswrite([outDir xlsFileName], {'Cond_StdWidth'}, 1, strcat('B', num2str(1)));
            xlswrite([outDir xlsFileName], {'Cond_MeanWidthSeg'}, 1, strcat('C', num2str(1)));
            xlswrite([outDir xlsFileName], {'Cond_StdWidthSeg'}, 1, strcat('D', num2str(1)));
            xlswrite([outDir xlsFileName], {'Chr_MeanWidth'}, 1, strcat('E', num2str(1)));
            xlswrite([outDir xlsFileName], {'Chr_StdWidth'}, 1, strcat('F', num2str(1)));
            
            xlswrite([outDir xlsFileName], {'Chr_RadInt_sum'}, 1, strcat('G', num2str(1)));
            xlswrite([outDir xlsFileName], {'Con_RadInt_sum'} , 1, strcat('H', num2str(1)));
            xlswrite([outDir xlsFileName], {'Con_RadIntSeg_sum'} , 1, strcat('I', num2str(1)));
            
            xlswrite([outDir xlsFileName], {'PercentIntInside600NmChrWidth_sum'}, 1, strcat('J', num2str(1)));
            xlswrite([outDir xlsFileName], {'WidthOccuping90PerInt_sum_nm'} , 1, strcat('K', num2str(1)));
            %xlswrite([outDir xlsFileName], {'Num_Regions_Combined'} , 1, strcat('L', num2str(1)));
            
            xlswrite([outDir xlsFileName], meanWidthCon, 1, ['A' num2str(2) ': A' num2str(2)]);
            xlswrite([outDir xlsFileName], stdWidthCon, 1, ['B' num2str(2) ': B' num2str(2)]);
            xlswrite([outDir xlsFileName], meanWidthConSeg, 1, ['C' num2str(2) ': C' num2str(2)]);
            xlswrite([outDir xlsFileName], stdWidthConSeg, 1, ['D' num2str(2) ': D' num2str(2)]);          
            xlswrite([outDir xlsFileName], meanWidthChr, 1, ['E' num2str(2) ': E' num2str(2)]);
            xlswrite([outDir xlsFileName], stdWidthChr, 1, ['F' num2str(2) ': F' num2str(2)]);
                      
            xlswrite([outDir xlsFileName], totProjChrEq, 1, ['G' num2str(2) ': G' num2str(2*crosRad+2)]);
            xlswrite([outDir xlsFileName], totProjConEq, 1, ['H' num2str(2) ': H' num2str(2*crosRad+2)]);
            xlswrite([outDir xlsFileName], totProjConSegEq, 1, ['I' num2str(2) ': I' num2str(2*crosRad+2)]);
            
            xlswrite([outDir xlsFileName], perIntInsideRefChrWidth, 1, ['J' num2str(2) ': J' num2str(2)]);
            xlswrite([outDir xlsFileName], widthCapRefPerInt_nm, 1, ['K' num2str(2) ': K' num2str(2)]);
            %xlswrite([outDir xlsFileName], foldTotProjConSeg, 1, ['T' num2str(2) ': T' num2str(crosRad+2)]);
            %xlswrite([outDir xlsFileName], numRegions, 1, ['L' num2str(2) ': L' num2str(2)]);
                   
            conCrosDist = conCrosDist/numRegions;
            %chrCrosDist = chrCrosDist/numRegions;
            hmfwCon = hmfwCon * voxelSizeX;
            hmfwChr = hmfwChr * voxelSizeX;
            meanHmfwCon = mean(hmfwCon);
            stdHmfwCon  = std(hmfwCon);
            meanHmfwChr = mean(hmfwChr);
            stdHmfwChr  = std(hmfwChr);
 
            meanHalfWidthChr_pix = meanFwhmWidthChr_nm /(voxelSizeX * 2); %%modified on 2017_11_10
            %curRadCon = round(meanWidthCon/2/voxelSizeX);
            upSampleFactor = 10;
            [conCrosDistIso, conChrMask, xLineCon] = fn_get_isotropic_dist(conCrosDist, [ceil(size(conSliceNorm,1)/2) ceil(size(conSliceNorm,2)/2)], round(meanHalfWidthChr_pix), isoDx, isoDy, upSampleFactor);
            xLineCon = xLineCon/max(xLineCon);
            
            lenProf = length(xLineCon);
            x = 1:lenProf;
            x = x * voxelSizeX;
            h = figure('NumberTitle','off', 'Visible','off'); plot(x,xLineCon, 'LineWidth',2);
            xlabel('Distance from condensin axis (nm)');
            ylabel('Normalized intensity (au)');
            filename = [outDir  protName{protIdx} '_' phaseName{phaseIdx} '_norm_int_prof_ball.tif'];
            saveas(h, filename, 'tif');
            close all;
            
            perIntInsideRefChrRad_ball = sum(xLineCon(1:halfRefWidthChr_pix))/sum(xLineCon);       
            for curHalfWidth_pix = 1: length(xLineCon)
                totInt = sum(xLineCon(1:curHalfWidth_pix));
                if totInt >= sum(xLineCon) * refPerInt/100
                    widthCapRefPerInt_ball_nm = curHalfWidth_pix * voxelSizeX * 2;
                    break;
                end
            end
            %xLineCon_4nmPix = conCrosDistIso(ceil(isoDx/2),:);
            filename = [outDir  protName{protIdx} '_' phaseName{phaseIdx} '.mat'];
            save(filename, 'meanWidthCon', 'meanWidthConSeg', 'meanWidthChr', 'stdWidthCon', 'stdWidthConSeg', 'stdWidthChr', 'totProjConEq', 'totProjConSegEq', 'totProjChrEq', 'perIntInsideRefChrWidth', 'widthCapRefPerInt_nm', 'perIntInsideRefChrRad_ball', 'widthCapRefPerInt_ball_nm', 'numRegions');
            
            xlswrite([outDir xlsFileName], {'Con_RadProf_ball'} , 1, strcat('L', num2str(1)));
            xlswrite([outDir xlsFileName], {'PercentIntInside300NmChrRadius_ball'}, 1, strcat('M', num2str(1)));
            xlswrite([outDir xlsFileName], {'WidthOccuping90PerInt_ball_nm'} , 1, strcat('N', num2str(1)));
            xlswrite([outDir xlsFileName], {'Num_Regions_Combined'} , 1, strcat('O', num2str(1)));
            
            
            xlswrite([outDir xlsFileName], xLineCon', 1, ['L' num2str(2) ': L' num2str(length(xLineCon)+1)]);
            xlswrite([outDir xlsFileName], perIntInsideRefChrRad_ball, 1, ['M' num2str(2) ': M' num2str(2)]);
            xlswrite([outDir xlsFileName], widthCapRefPerInt_ball_nm, 1, ['N' num2str(2) ': N' num2str(2)]);
            xlswrite([outDir xlsFileName], numRegions, 1, ['O' num2str(2) ': O' num2str(2)]);
            
            conCrosDistIso = conCrosDistIso/max(conCrosDistIso(:));
            h = figure('Name','Mean cross section intensity distribution of condensin - isotropic', 'NumberTitle','off', 'Visible','off'); imagesc(conCrosDistIso); axis equal; axis tight; colorbar; colormap(jet);
            ax=gca;
            set(ax, 'XTickLabel', { '-500', '-300', '-100', '100', '300', '500', '700'});
            set(ax, 'YTickLabel', { '-500', '-300', '-100', '100', '300', '500', '700'});
            fnColCon = [protName{protIdx} '_' phaseName{phaseIdx} '_Cond.tif'];
            saveas(h,[outDir  fnColCon], 'tif');
            
            h = figure('Name','Mean cross section intensity distribution of condensin - isotropic','NumberTitle','off', 'Visible','off'); imagesc(conChrMask); axis equal; axis tight; colorbar; colormap(flag);
            ax=gca;
            set(ax, 'XTickLabel', { '-500', '-300', '-100', '100', '300', '500', '700'});
            set(ax, 'YTickLabel', { '-500', '-300', '-100', '100', '300', '500', '700'});
            fnColChr = [protName{protIdx} '_' phaseName{phaseIdx} '_Chr.tif'];
            saveas(h,[outDir  fnColChr], 'tif');
            fnConComb = [protName{protIdx} '_' phaseName{phaseIdx} '.tif'];
            fn_save_combined_distribution_image(outDir, fnColCon, fnColChr, fnConComb);
            delete ([outDir  fnColChr]);
            delete ([outDir  fnColCon]);  
   
                   
%             filename = [outDir  protName{protIdx} '_' phaseName{phaseIdx} '_HmfwCon.mat'];
%             save(filename, 'meanHmfwCon', 'meanHmfwChr',  'stdHmfwCon', 'stdHmfwChr', 'numRegions');
%             xlsFileName = [protName{protIdx} '_' phaseName{phaseIdx} '_HmfwCon.xls'];
%             
%             xlswrite([outDir xlsFileName], {'Cond_Indiv_HMFW'}, 1, strcat('A', num2str(1)));
%             xlswrite([outDir xlsFileName], {'Cond_Mean_HMFW'}, 1, strcat('B', num2str(1)));
%             xlswrite([outDir xlsFileName], {'Cond_Stdev_HMFW'},  1, strcat('C', num2str(1)));
%             xlswrite([outDir xlsFileName], {'Chr_Indiv_HMFW'}, 1, strcat('D', num2str(1)));
%             xlswrite([outDir xlsFileName], {'Chr_Mean_HMFW'}, 1, strcat('E', num2str(1)));
%             xlswrite([outDir xlsFileName], {'Chr_Stdev_HMFW'},  1, strcat('F', num2str(1)));
%             xlswrite([outDir xlsFileName], {'Num_Regions'},  1, strcat('G', num2str(1)));
%             
%             xlswrite([outDir xlsFileName], hmfwCon', 1, ['A' num2str(2) ': A' num2str(length(hmfwCon))]);
%             xlswrite([outDir xlsFileName], meanHmfwCon, 1, ['B' num2str(2) ': B' num2str(2)]);
%             xlswrite([outDir xlsFileName], stdHmfwCon,  1, ['C' num2str(2) ': C' num2str(2)]);
%             xlswrite([outDir xlsFileName], hmfwChr', 1, ['D' num2str(2) ': D' num2str(length(hmfwChr))]);
%             xlswrite([outDir xlsFileName], meanHmfwChr, 1, ['E' num2str(2) ': E' num2str(2)]);
%             xlswrite([outDir xlsFileName], stdHmfwChr,  1, ['F' num2str(2) ': F' num2str(2)]);
%             xlswrite([outDir xlsFileName], numRegions,  1, ['G' num2str(2) ': G' num2str(2)]);

        end
        
        clear meanTotInt;
        clear meanTotIntChr;
        clear meanTotIntSeg;
        clear meanWidthCon;
        clear meanWidthConSeg;
        clear meanWidthChr;
        clear meanWidthConSecond;
        clear conCrosDist;
        clear chrCrosDist;
        clear totProjCon;
        clear totProjChr;
        clear totProjConSeg;
        fclose all;
        close all;
    end
end