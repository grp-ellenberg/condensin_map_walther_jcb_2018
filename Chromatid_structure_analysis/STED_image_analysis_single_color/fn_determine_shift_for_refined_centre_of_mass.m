function [xShift, yShift] = fn_determine_shift_for_refined_centre_of_mass(stackRef, centX, centY, curRadX, curRadY, maxShiftX, maxShiftY)
%This function tries to determine shift between two channels
% stackVarTemp = zeros(size(stackRef));
% stackVar = zeros(size(stackRef));
% stackVar(round(centX)-curRadX:round(centX)+curRadX, round(centY)-curRadY:round(centY)+curRadY) = 1;
maxTotInt = 0;
xShift = 0;
yShift = 0;
%shiftRangeXY = 3;
centX = round(centX);
centY = round(centY);
for curXShift = -maxShiftX:maxShiftX
    for curYShift = -maxShiftY:maxShiftY
        
        croppedImg = stackRef(centX+curXShift-curRadX:centX+curXShift+curRadX, centY+curYShift-curRadY:centY+curYShift+curRadY);  
        curTotInt = sum(sum(croppedImg));
        %volOvlHomVar = sum(sum(sum(ovlHomVar))) * voxelSize;
        %volTot = volVar + volHom - volOvlHomVar;
        
        if curTotInt > maxTotInt
            maxTotInt = curTotInt;
            xShift = curXShift;
            yShift = curYShift;
        end
    end
end
end

