This module performs structural analyses on condensin regions based on 2D STED z-stacks. It detects the width of condensin and DNA. It generates a radial profile of each condensin subunit per mitotic phase.

Condensin and DNA regions need to be segmented manually first. The shift between the two channels (if any) and the drift between different z-slices (if any) need to be corrected.

%Execution order:
%Process individual region
1. analyse_condensin_structure_indiv_region_main.m
%Calculate width of condensin and DNA
2. combine_results_prot_phase_fwhm_width_main.m
%Generate radial distributions
3. combine_results_prot_phase_radial_profile_main.m

Manual segmentation of condensin and DNA has to be done before using the 'fn_manual_segmentation_main.m' script. It will save the binary mask. 
Registration (drift correction) of different slices within a z-stack and shift correction between two channels have to be done.