function [outDist, chrMask, halfXLineOrig] = fn_nonistropic_to_isotrpic_distribution(inDist, centX, centY, dx, dy, upSampleFactor, chrRad)
%This function creates an isotropic distribution from non isotropic one
%Author: M. Julius Hossain, EMBL Heidelberg
%Last update: 2017_10_18
%upSampleFactor = 5;
centX = round(centX);
centY = round(centY);
cropInDist  = inDist(centX-floor(dx/2):centX+floor(dx/2),centY-floor(dy/2):centY+floor(dy/2));

centX = ceil(dx/2);
centY = ceil(dy/2);

xLine = cropInDist(centX,:);
halfXLineOrig = (xLine(centY:end) + fliplr(xLine(1:centY)))/2;


upDx = size(cropInDist,1)*upSampleFactor;
upDy = size(cropInDist,2)*upSampleFactor;
upCentX = ceil(upDx/2);
upCentY = ceil(upDy/2);

halfXLine = imresize(halfXLineOrig, [1,upCentX]);
halfXLine(halfXLine <0) = 0;
outDist = zeros(upDx, upDy);
chrMask = zeros(upDx, upDy);
chrRad = chrRad *upSampleFactor;
for i=1:upDx
    for j=1:upDy
        %if outDist(i,j) >0
            dist = round(sqrt((i-upCentX)^2 + (j-upCentY)^2));
            if dist>=0 && dist< upCentX
                outDist(i,j) = halfXLine(dist+1);
                if dist<= chrRad
                    chrMask(i,j) = 1;
                end
            end
        %end
    end
end
end

