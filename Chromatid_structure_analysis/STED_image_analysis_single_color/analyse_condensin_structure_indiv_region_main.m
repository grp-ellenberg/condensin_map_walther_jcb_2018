% This script performs structural analysis on single color condensin image
% stacks. In addtion to the raw input image stack of both condensin and DNA
% it needs their segmented segmented binary masks as input.
% Any shift between two channels or drift between individual slices need to 
% be corrected   

% Author: Julius Hossain, EMBL Heidelberg (julius.hossain@embl.de)
% Last update: 2018_03_04 - documentation added


clear all;
close all;
%Inut directory
inDir = fullfile('Z:\Julius\Test_Processing\Sted_single_color_data\Results', filesep);
%Get the root directory of sour code
[src_root_dir,~,~]=fileparts(pwd);
%Add path of related common functions required
addpath(fullfile(src_root_dir, 'common_functions'));
addpath(fullfile(src_root_dir, 'external_functions'));

%Get option running option and parameters values
[options] = sted_single_color_options_params();

%Get the name of individual regions
regionDir = dir([inDir '*Drift*']);
voxelSize = options.voxelSizeX * options.voxelSizeY * options.voxelSizeZ;
%Process individual regions one by one
for regionDirIdx =1:length(regionDir)
    if regionDir(regionDirIdx).isdir == 1
        %Current region direcotory
        curRegionDir = fullfile(inDir, regionDir(regionDirIdx).name, filesep);
        %Input directory containing isotropic data
        inDirRegIso     = fullfile(curRegionDir, 'Registered_Iso', filesep);
        %Input directory containing isotropic segmented binary mask
        inDirBinMaskIso = fullfile(curRegionDir, 'Segmented_BinMask_Iso', filesep);
        %Output directory to store straightened condensin and dna region  
        outDirDisp      = fullfile(curRegionDir, 'Disp_Stack_up', filesep);
        %Output directory to store extracted parameters
        outDirParams      = fullfile(curRegionDir, 'Extracted_Params', filesep);
        
        %Create directories
        if ~exist(outDirDisp)
            mkdir(outDirDisp);
        end
                
        if ~exist(outDirParams)
            mkdir(outDirParams);
        end
        
        disp(['Processing_RegionID = ' num2str(regionDirIdx) ': ' curRegionDir]);
        
        fnListChr = dir([inDirBinMaskIso '*Chr*.tif']) ; %Name of the input file - chromatin
        fnListCon = dir([inDirBinMaskIso '*Cond*.tif']); %Name of the input file - Condensin
        
        if isempty(fnListCon) || (length(fnListChr) ~= length(fnListCon))
            disp('No input files or number of files for Chr and Con are different');
            continue;
        end
        fnList = dir([outDirDisp '*.tif']);
        if length(fnList)<4 || options.reproc == 1
            %Open images images and binary mask
            [conStackIso]   = fn_open_tiff_stack(inDirRegIso,     fnListCon(1).name);
            [conBinMaskIso] = fn_open_tiff_stack(inDirBinMaskIso, fnListCon(1).name);
            
            [chrStackIso]   = fn_open_tiff_stack(inDirRegIso,     fnListChr(1).name);
            [chrBinMaskIso] = fn_open_tiff_stack(inDirBinMaskIso, fnListChr(1).name);
            
            [dx, dy, Nz] = size(conStackIso);
            %Filter condensin images
            conStackIso  = imgaussian(conStackIso, options.sigma,options.hsize);
            conStackIso(conBinMaskIso==0) = 0;
            chrStackIso(chrBinMaskIso==0) = 0;
            
            
            [thrConGlb, ~] = ls_otsu_3d_image(conStackIso, 1);
            
            [x,y,z]=ls_threed_coord(find(conBinMaskIso),dx,dy);
            avgZ = round(sum(z)/length(z));
            z = avgZ;
            [vCon, rCon, xgCon, ygCon, zgCon] = ls_eigen_vectors_3d(x, y, z, 1, 1, 1);
            conAxis = vCon(:,3);
            
            tRange  = round(sqrt(rCon(3,3))*options.eigValPortion); %Processing of chromosome tip is avoided
            
            t = -tRange:options.stepSizeInit:tRange;
            xConAxis = round((t*conAxis(1)+xgCon)/1);
            yConAxis = round((t*conAxis(2)+ygCon)/1);
            zConAxis = round((t*conAxis(3)+zgCon)/1);
            
            %Extract slices along chromosome axis
            if options.chrInitRef == 1
                [refCentX, refCentY, refCentZ] = extract_centroids_along_chromosome_axis(chrStackIso, xConAxis, yConAxis, zConAxis, conAxis(1), conAxis(2), conAxis(3), options.crosRad);
            else
                [refCentX, refCentY, refCentZ] = extract_centroids_along_chromosome_axis(conStackIso, xConAxis, yConAxis, zConAxis, conAxis(1), conAxis(2), conAxis(3), options.crosRad);
            end
            
            refCentX = smooth(refCentX,5)';
            refCentY = smooth(refCentY,5)';
            refCentZ = smooth(refCentZ,5)';
            intPos = 1:1/(options.stepSizeInit*options.upSample):size(refCentX,2);
            intRefCentX = interp1(refCentX, intPos);
            intRefCentY = interp1(refCentY, intPos);
            intRefCentZ = interp1(refCentZ, intPos);
            %intRefCentX = interp1(refCentX, inPos);
            %%
            %Generate local vectors along which slicing will be done in the
            %second phase
            centCount = 0;
            curCentStart = 1;
            finCentX = refCentX(curCentStart);
            finCentY = refCentY(curCentStart);
            finCentZ = refCentZ(curCentStart);
            vectX = refCentX(curCentStart+1) - refCentX(curCentStart);
            vectY = refCentY(curCentStart+1) - refCentY(curCentStart);
            
            for i = curCentStart+1:size(intRefCentX,2)
                if sqrt((intRefCentX(curCentStart) - intRefCentX(i))^2+(intRefCentY(curCentStart) - intRefCentY(i))^2) >= options.stepSizeFin
                    finCentX = [finCentX intRefCentX(i)];
                    finCentY = [finCentY intRefCentY(i)];
                    finCentZ = [finCentZ intRefCentZ(i)];
                    curRefIdx = floor(i/(options.stepSizeInit*options.upSample))+1;
                    if curRefIdx == 0
                        curRefIdx = 1;
                    elseif curRefIdx == size(refCentX,2)
                        curRefIdx = curRefIdx - 1;
                    end
                    
                    vectX = [vectX refCentX(curRefIdx+1) - refCentX(curRefIdx)];
                    vectY = [vectY refCentY(curRefIdx+1) - refCentY(curRefIdx)];
                    curCentStart = i;
                end
            end
            %Allocate memory to store different parameters
            nFinSlices = size(finCentX,2);
            totInt = zeros(nFinSlices, 1);
            totIntChr = zeros(nFinSlices, 1);
            totIntSeg = zeros(nFinSlices, 1);
            widthCon  = zeros(nFinSlices, 1);
            widthConSecond  = zeros(nFinSlices, 1);
            widthConSegSecond  = zeros(nFinSlices, 1);
            widthChr  = zeros(nFinSlices, 1);
            totIntConMask = zeros(nFinSlices, 1);
            totIntConMask1 = zeros(nFinSlices, 1);
            totIntConMask2 = zeros(nFinSlices, 1);
            totIntConMask3 = zeros(nFinSlices, 1);
            totIntConMask4 = zeros(nFinSlices, 1);
            totIntConMaskLeft = zeros(nFinSlices, 1);
            totIntConMaskMiddle = zeros(nFinSlices, 1);
            totIntConMaskRight = zeros(nFinSlices, 1);
            
            
            widthConSeg  = zeros(nFinSlices, 1);
            convHullArea  = zeros(nFinSlices, 1);
            totProjCon = zeros(2*options.crosRad+1,1);
            totProjChr = zeros(2*options.crosRad+1,1);
            totProjConSeg = zeros(2*options.crosRad+1,1);
            
            conStackStr = zeros(2*options.crosRad+1, nFinSlices, 2*options.crosRad+1);
            chrStackStr = zeros(2*options.crosRad+1, nFinSlices, 2*options.crosRad+1);
            %%
            intIdx = 0;
            for axIdx = 1:nFinSlices
                %Reslice again with along local vectors determined already                  
                [conSlice, chrSlice, subX, subY, subZ] = extract_slice_con_chr(conStackIso, chrStackIso, finCentX(axIdx), finCentY(axIdx), finCentZ(axIdx), vectX(axIdx), vectY(axIdx), 0, options.crosRad);
                conSlice1 = permute(conSlice, [2 1]);
                conStackStr(:,axIdx,:) = conSlice1;
                chrSlice1 = permute(chrSlice, [2 1]);
                chrStackStr(:,axIdx,:) = chrSlice1;
                intIdx = intIdx + 1;
                totInt(intIdx) = sum(sum(conSlice));
                totIntChr(intIdx) = sum(sum(chrSlice));
                if totInt(intIdx)>0
                    %Get width based on eigen value of segmented volume
                    [x,y,z]=ls_threed_coord(find(conSlice),size(conSlice,1),size(conSlice,2));
                    [vCon, rCon, xgCon, ygCon, zgCon] = ls_eigen_vectors_3d(x, y, z, 1, 1, 1);
                    %Exract 3d coordinates of extracted slice
                    curWidth = 4* sqrt(rCon(2,2))*options.voxelSizeX;
                    widthCon(intIdx) = curWidth;
                    widthConSecond(intIdx) = 4* sqrt(rCon(3,3))*options.voxelSizeX;
                    
                    [x,y,z]=ls_threed_coord(find(chrSlice),size(chrSlice,1),size(chrSlice,2));
                    [vChr, rChr, xgChr, ygChr, zgChr] = ls_eigen_vectors_3d(x, y, z, 1, 1, 1);
                    curWidth = 4* sqrt(rChr(2,2))*options.voxelSizeX;
                    widthChr(intIdx) = curWidth;
                    
                    thrConLcl = ls_otsu_2d_image(conSlice, 1);
                    thrCon = thrConGlb * options.factGlb + thrConLcl * (1-options.factGlb);
                    binCurSlice = double(conSlice>=thrCon);
                    highConSlice = conSlice;
                    highConSlice(binCurSlice==0) = 0;
                    
                    totIntSeg(intIdx) = sum(sum(highConSlice));
                    %convHull = bwconvhull(binCurSlice);
                    %convHullArea(intIdx) = sum(sum(convHull>0)) *options.voxelSizeX*options.voxelSizeY;
                    
                    [x,y,z]=ls_threed_coord(find(binCurSlice),size(binCurSlice,1),size(binCurSlice,2));
                    if length(x) == 0
                        continue;
                    end
                    [vCon, rCon, xgCon, ygCon, zgCon] = ls_eigen_vectors_3d(x, y, z, 1, 1, 1);
                    curWidth = 4* sqrt(rCon(2,2))*options.voxelSizeX;
                    widthConSeg(intIdx) = curWidth;
                    widthConSegSecond(intIdx) = 4* sqrt(rCon(3,3))*options.voxelSizeX;
                    
                    %figure; imagesc(conSlice);
                    %pause;
                    projConSeg = sum(highConSlice);
                    projCon = sum(conSlice);
                    projChr = sum(chrSlice);
                    
                    chrCentY = round(ygChr);
                    shift = (options.crosRad + 1) - chrCentY;
                    totProjCon = totProjCon + circshift(projCon',shift);
                    totProjConSeg = totProjConSeg + circshift(projConSeg',shift);
                    totProjChr = totProjChr + circshift(projChr',shift);
                end
            end
            
            totProjCon = totProjCon * (1000/(options.voxelSizeX*nFinSlices));
            totProjConSeg = totProjConSeg *(1000/(options.voxelSizeX*nFinSlices));
            totProjChr = totProjChr *(1000/(options.voxelSizeX*nFinSlices));
            
            meanTotInt = sum(totInt)/length(totInt);
            %medTotInt = median(totInt);
            
            meanTotIntChr = sum(totIntChr)/length(totIntChr);
            %medTotIntChr = median(totIntChr);
            
            meanTotIntSeg = sum(totIntSeg)/length(totIntSeg);
            %medTotIntSeg = median(totIntSeg);
            
            meanWidthCon = sum(widthCon)/length(widthCon);
            %medWidthCon = median(widthCon);
            
            meanWidthChr = sum(widthChr)/length(widthChr);
            %medWidthChr  = median(widthChr);
            
            meanWidthConSeg = sum(widthConSeg)/length(widthConSeg);
            %medWidthConSeg = median(widthConSeg);
            
            meanWidthConSegSecond = sum(widthConSegSecond)/length(widthConSegSecond);
            meanWidthConSecond = sum(widthConSecond)/length(widthConSecond);
            %meanConvHullArea = sum(convHullArea)/length(convHullArea);
            %medConvHullArea  = median(convHullArea);
            
            outDir = [curRegionDir filesep];
            xlsFileName = [regionDir(regionDirIdx).name '.xls'];
            
            radEll = round((meanWidthCon + meanWidthConSeg)/(4*options.voxelSizeX));
            [mask, mask1,mask2,mask3,mask4] = fn_detect_intensity_four_quarters(2*options.crosRad+1, nFinSlices, 2*options.crosRad+1, radEll);
            
            conStackStrMask = conStackStr;
            conStackStrMask(mask == 0) = 0;
            conStackStrMask1 = conStackStr;
            conStackStrMask1(mask1 == 0) = 0;
            conStackStrMask2 = conStackStr;
            conStackStrMask2(mask2 == 0) = 0;
            conStackStrMask3 = conStackStr;
            conStackStrMask3(mask3 == 0) = 0;
            conStackStrMask4 = conStackStr;
            conStackStrMask4(mask4 == 0) = 0;
            
            [maskLeft, maskRight, maskMiddle] = fn_detect_intensity_three_regions(2*options.crosRad+1, nFinSlices, 2*options.crosRad+1, radEll);
            
            conStackStrMaskLeft = conStackStr;
            conStackStrMaskLeft(maskLeft == 0) = 0;
            conStackStrMaskRight = conStackStr;
            conStackStrMaskRight(maskRight == 0) = 0;
            conStackStrMaskMiddle = conStackStr;
            conStackStrMaskMiddle(maskMiddle == 0) = 0;
            
            for i = 1: nFinSlices
                totIntConMask(i) = sum(sum(conStackStrMask(:,i,:)));
                totIntConMask1(i) = sum(sum(conStackStrMask1(:,i,:)));
                totIntConMask2(i) = sum(sum(conStackStrMask2(:,i,:)));
                totIntConMask3(i) = sum(sum(conStackStrMask3(:,i,:)));
                totIntConMask4(i) = sum(sum(conStackStrMask4(:,i,:)));
                totIntConMaskLeft(i) = sum(sum(conStackStrMaskLeft(:,i,:)));
                totIntConMaskRight(i) = sum(sum(conStackStrMaskRight(:,i,:)));
                totIntConMaskMiddle(i) = sum(sum(conStackStrMaskMiddle(:,i,:)));
                
            end
            
            [conPeak1, conPeak2] = fn_compute_condensin_peaks(conStackStr, options.spotRad);
            conPeak = zeros(size(conPeak1));
            conPeak(conPeak1>0) = 1;
            conPeak(conPeak2>0) = 2;
            h = figure('Name','Displaying position of the peaks','NumberTitle','off', 'Visible', 'off'); imagesc(conPeak)
            saveas(h,[outDirParams filesep regionDir(regionDirIdx).name '_Peaks.tif'], 'tif');
            
            h = figure('Name','Displaying Total Intensity on Mask','NumberTitle','off', 'Visible', 'off'); plot(totIntConMask)
            saveas(h,[outDirParams filesep regionDir(regionDirIdx).name '_Tot_Int_Entire_circle.tif'], 'tif');
            h = figure('Name','Displaying Total Intensity on Mask1','NumberTitle','off', 'Visible', 'off'); plot(totIntConMask1)
            saveas(h,[outDirParams filesep regionDir(regionDirIdx).name '_Tot_Int_Quarter1.tif'], 'tif');
            h = figure('Name','Displaying Total Intensity on Mask2','NumberTitle','off', 'Visible', 'off'); plot(totIntConMask2)
            saveas(h,[outDirParams filesep regionDir(regionDirIdx).name '_Tot_Int_Quarter2.tif'], 'tif');
            h = figure('Name','Displaying Total Intensity on Mask3','NumberTitle','off', 'Visible', 'off'); plot(totIntConMask3)
            saveas(h,[outDirParams filesep regionDir(regionDirIdx).name '_Tot_Int_Quarter3.tif'], 'tif');
            h = figure('Name','Displaying Total Intensity on Mask4','NumberTitle','off', 'Visible', 'off'); plot(totIntConMask4)
            saveas(h,[outDirParams filesep regionDir(regionDirIdx).name '_Tot_Int_Quarter4.tif'], 'tif');
            
            h = figure('Name','Displaying Total Intensity on MaskLeft','NumberTitle','off', 'Visible', 'off'); plot(totIntConMaskLeft)
            saveas(h,[outDirParams filesep regionDir(regionDirIdx).name '_Tot_Int_Left.tif'], 'tif');
            h = figure('Name','Displaying Total Intensity on MaskMiddle','NumberTitle','off', 'Visible', 'off'); plot(totIntConMaskMiddle)
            saveas(h,[outDirParams filesep regionDir(regionDirIdx).name '_Tot_Int_Middle.tif'], 'tif');
            h = figure('Name','Displaying Total Intensity on MaskMiddle','NumberTitle','off', 'Visible', 'off'); plot(totIntConMaskRight)
            saveas(h,[outDirParams filesep regionDir(regionDirIdx).name '_Tot_Int_Right.tif'], 'tif');
            
            xlswrite([outDirParams filesep xlsFileName], {'Cond_TotInt'   }, 1, strcat('A', num2str(1)));
            xlswrite([outDirParams filesep xlsFileName], {'Cond_TotIntSeg'}, 1, strcat('B', num2str(1)));
            xlswrite([outDirParams filesep xlsFileName], {'Cond_Width'}, 1, strcat('C', num2str(1)));
            xlswrite([outDirParams filesep xlsFileName], {'Cond_WidthSeg'}, 1, strcat('D', num2str(1)));
            
            xlswrite([outDirParams filesep xlsFileName], {'Cond_MeanTotInt'}, 1, strcat('E', num2str(1)));
            %xlswrite([outDirParams filesep xlsFileName], {'Cond_MedTotInt'}, 1, strcat('F', num2str(1)));
            xlswrite([outDirParams filesep xlsFileName], {'Cond_MeanTotIntSeg'}, 1, strcat('F', num2str(1)));
            %xlswrite([outDirParams filesep xlsFileName], {'Cond_MedTotIntSeg'}, 1, strcat('H', num2str(1)));
            
            xlswrite([outDirParams filesep xlsFileName], {'Cond_MeanWidth'}, 1, strcat('G', num2str(1)));
            %xlswrite([outDirParams filesep xlsFileName], {'Cond_MedWidth'}, 1, strcat('J', num2str(1)));
            xlswrite([outDirParams filesep xlsFileName], {'Cond_MeanWidthSeg'}, 1, strcat('H', num2str(1)));
            %xlswrite([outDirParams filesep xlsFileName], {'Cond_MedWidthSeg'}, 1, strcat('L', num2str(1)));
            
            xlswrite([outDirParams filesep xlsFileName], {'Chr_Width'}, 1, strcat('I', num2str(1)));
            xlswrite([outDirParams filesep xlsFileName], {'Chr_MeanWidth'}, 1, strcat('J', num2str(1)));
            %xlswrite([outDirParams filesep xlsFileName], {'Chr_MedWidth'} , 1, strcat('O', num2str(1)));
            
            xlswrite([outDirParams filesep xlsFileName], {'Chr_RadIntDist_MicroMetre'}, 1, strcat('K', num2str(1)));
            xlswrite([outDirParams filesep xlsFileName], {'Con_RadIntDist_MicroMetre'} , 1, strcat('L', num2str(1)));
            xlswrite([outDirParams filesep xlsFileName], {'Con_RadIntDistSeg_MicroMetre'} , 1, strcat('M', num2str(1)));
            
            xlswrite([outDirParams filesep xlsFileName], {'Con_RadIntDistEntireCircle' } , 1, strcat('N', num2str(1)));
            xlswrite([outDirParams filesep xlsFileName], {'Con_RadIntDistQuarter1'} , 1, strcat('O', num2str(1)));
            xlswrite([outDirParams filesep xlsFileName], {'Con_RadIntDistQuarter2'} , 1, strcat('P', num2str(1)));
            xlswrite([outDirParams filesep xlsFileName], {'Con_RadIntDistQuarter3'} , 1, strcat('Q', num2str(1)));
            xlswrite([outDirParams filesep xlsFileName], {'Con_RadIntDistQuarter4'} , 1, strcat('R', num2str(1)));
            
            xlswrite([outDirParams filesep xlsFileName], {'Con_RadIntDistLeft'} , 1, strcat('S', num2str(1)));
            xlswrite([outDirParams filesep xlsFileName], {'Con_RadIntDistMiddle'} , 1, strcat('T', num2str(1)));
            xlswrite([outDirParams filesep xlsFileName], {'Con_RadIntDistRight'} , 1, strcat('U', num2str(1)));
            
            xlswrite([outDirParams filesep xlsFileName], {'Cond_MeanWidthSecond'}, 1, strcat('V', num2str(1)));
            xlswrite([outDirParams filesep xlsFileName], {'Cond_MeanWidthSegSecond'}, 1, strcat('W', num2str(1)));
            
            xlswrite([outDirParams filesep xlsFileName], {'Chr_TotInt'   }, 1, strcat('X', num2str(1)));
            xlswrite([outDirParams filesep xlsFileName], {'Chr_MeanTotInt'}, 1, strcat('Y', num2str(1)));
            %xlswrite([outDirParams filesep xlsFileName], {'Chr_MedTotInt'}, 1, strcat('AE', num2str(1)));
            
            xlswrite([outDirParams filesep xlsFileName], totInt, 1, ['A' num2str(2) ': A' num2str(nFinSlices+1)]);
            xlswrite([outDirParams filesep xlsFileName], totIntSeg, 1, ['B' num2str(2) ': B' num2str(nFinSlices+1)]);
            xlswrite([outDirParams filesep xlsFileName], widthCon, 1, ['C' num2str(2) ': C' num2str(nFinSlices+1)]);
            xlswrite([outDirParams filesep xlsFileName], widthConSeg, 1, ['D' num2str(2) ': D' num2str(nFinSlices+1)]);
            
            xlswrite([outDirParams filesep xlsFileName], meanTotInt, 1, ['E' num2str(2) ': E' num2str(2)]);
            %xlswrite([outDirParams filesep xlsFileName], medTotInt, 1, ['F' num2str(2) ': F' num2str(2)]);
            xlswrite([outDirParams filesep xlsFileName], meanTotIntSeg, 1, ['F' num2str(2) ': F' num2str(2)]);
            %xlswrite([outDirParams filesep xlsFileName], medTotIntSeg, 1, ['H' num2str(2) ': H' num2str(2)]);
            
            xlswrite([outDirParams filesep xlsFileName], meanWidthCon, 1, ['G' num2str(2) ': G' num2str(2)]);
            %xlswrite([outDirParams filesep xlsFileName], medWidthCon, 1, ['J' num2str(2) ': J' num2str(2)]);
            xlswrite([outDirParams filesep xlsFileName], meanWidthConSeg, 1, ['H' num2str(2) ': H' num2str(2)]);
            %xlswrite([outDirParams filesep xlsFileName], medWidthConSeg, 1, ['L' num2str(2) ': L' num2str(2)]);
            
            xlswrite([outDirParams filesep xlsFileName], widthChr, 1, ['I' num2str(2) ': I' num2str(nFinSlices+1)]);
            xlswrite([outDirParams filesep xlsFileName], meanWidthChr, 1, ['J' num2str(2) ': J' num2str(2)]);
            %xlswrite([outDirParams filesep xlsFileName], medWidthChr, 1, ['O' num2str(2) ': O' num2str(2)]);
            
            xlswrite([outDirParams filesep xlsFileName], totProjChr, 1, ['K' num2str(2) ': K' num2str(2*options.crosRad+1+1)]);
            xlswrite([outDirParams filesep xlsFileName], totProjCon, 1, ['L' num2str(2) ': L' num2str(2*options.crosRad+1+1)]);
            xlswrite([outDirParams filesep xlsFileName], totProjConSeg, 1, ['M' num2str(2) ': M' num2str(2*options.crosRad+1+1)]);
            
            xlswrite([outDirParams filesep xlsFileName], totIntConMask,  1, ['N' num2str(2) ': N' num2str(nFinSlices+1)]);
            xlswrite([outDirParams filesep xlsFileName], totIntConMask1, 1, ['O' num2str(2) ': O' num2str(nFinSlices+1)]);
            xlswrite([outDirParams filesep xlsFileName], totIntConMask2, 1, ['P' num2str(2) ': P' num2str(nFinSlices+1)]);
            xlswrite([outDirParams filesep xlsFileName], totIntConMask3, 1, ['Q' num2str(2) ': Q' num2str(nFinSlices+1)]);
            xlswrite([outDirParams filesep xlsFileName], totIntConMask4, 1, ['R' num2str(2) ': R' num2str(nFinSlices+1)]);
            
            xlswrite([outDirParams filesep xlsFileName], totIntConMaskLeft, 1, ['S' num2str(2) ': S' num2str(nFinSlices+1)]);
            xlswrite([outDirParams filesep xlsFileName], totIntConMaskMiddle, 1, ['T' num2str(2) ': T' num2str(nFinSlices+1)]);
            xlswrite([outDirParams filesep xlsFileName], totIntConMaskRight, 1, ['U' num2str(2) ': U' num2str(nFinSlices+1)]);
            
            xlswrite([outDirParams filesep xlsFileName], meanWidthConSecond, 1, ['V' num2str(2) ': V' num2str(2)]);
            xlswrite([outDirParams filesep xlsFileName], meanWidthConSegSecond, 1, ['W' num2str(2) ': W' num2str(2)]);
            
            xlswrite([outDirParams filesep xlsFileName], totIntChr, 1, ['X' num2str(2) ': X' num2str(nFinSlices+1)]);
            xlswrite([outDirParams filesep xlsFileName], meanTotIntChr, 1, ['Y' num2str(2) ': Y' num2str(2)]);
            %xlswrite([outDirParams filesep xlsFileName], medTotIntChr, 1, ['AE' num2str(2) ': AE' num2str(2)]);
            
            
            fn_write_tiff_stack(conStackIso, outDirDisp, [fnListCon(1).name(1:end-4) '_ManSeg.tif'], 16);
            fn_write_tiff_stack(chrStackIso, outDirDisp, [fnListChr(1).name(1:end-4) '_ManSeg.tif'], 16);
            fn_write_tiff_stack(conStackStr, outDirDisp, [regionDir(regionDirIdx).name '_Cond.tif'], 16);
            fn_write_tiff_stack(chrStackStr, outDirDisp, [regionDir(regionDirIdx).name '_Chr.tif'], 16);
            
            filename = [outDirParams filesep filesep regionDir(regionDirIdx).name '.mat'];
            
            save(filename, 'totInt', 'totIntChr', 'totIntSeg', 'widthCon', 'widthConSeg', 'meanTotInt', 'meanTotIntChr', 'meanTotIntSeg', 'meanWidthCon', 'meanWidthConSeg', 'meanWidthConSecond','meanWidthConSegSecond', 'widthChr', 'meanWidthChr', 'totProjChr', 'totProjCon', 'totProjConSeg', 'totIntConMaskLeft', 'totIntConMaskMiddle', 'totIntConMaskRight', 'conPeak1', 'conPeak2');
            close all;
        end
    end
end