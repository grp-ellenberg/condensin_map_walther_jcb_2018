%This scripts combines the results from individual cell regions and
%caculate FWHM width of each subunit per mitotic stage

%Author: Julius Hossain, EMBL, Heidelberg
%Last update: 2018_03_05

clear all;
clc;
close all;
%Get the root directory of sour code
[src_root_dir,~,~]=fileparts(pwd);
%Add path of related common functions required
addpath(fullfile(src_root_dir, 'common_functions'));
addpath(fullfile(src_root_dir, 'external_functions','fwhm'));
%Name of sub units
protName = {'gfpTOP2Ac102', 'gfpKIF4Ac173', 'NCAPD2gfpc272c78', 'NCAPD3gfpc16', 'NCAPH2gfpc67', 'NCAPHgfpc86', 'SMC4gfpz82z68', 'gfpNCAPH2c1'};
%Name of mitotic phase
phaseName = {'prometa', 'ana_early'};

%Input directory containing results for individual regions
inDir  = fullfile('Z:\Julius\Test_Processing\Sted_single_color_data\Results', filesep);

%Output directory prefix - Parms or Dist is added to it to save the results
outDirRootPref = 'Z:\Julius\Test_Processing\Sted_single_color_data\Extracted_parameters\Results_fwhm';

% 1 compute avg int width etc. 2 - compute condensin cross section distribution; 3 both
for computeFlag = 1:2
    if computeFlag == 1
        outDirRoot = fullfile([outDirRootPref '_Parm'], filesep); % Saves all the parameters.
        paramsDir = outDirRoot;
    elseif computeFlag == 2
        outDirRoot = fullfile([outDirRootPref '_Dist'], filesep); % Saves results with width measurements
    end
    %paramsDir = 'Z:\Julius\Test_Processing\Sted_single_color_data\Extracted_parameters\Results_fwhm_Parm\';
    
    outDir = fullfile(outDirRoot, 'Regions_All', filesep);
    procAll = 1;
    selRegFlag = 100;
    %try
    fn_combine_results_prot_phase_fwhm_width(inDir, outDir, protName, phaseName, procAll, selRegFlag, computeFlag, paramsDir);
    % catch
    %     disp('Terminated Flag All');
    % end
    
    outDir = fullfile(outDirRoot, 'Regions_Flag1', filesep);
    procAll = 0;
    selRegFlag = 1;
    %try
    fn_combine_results_prot_phase_fwhm_width(inDir, outDir, protName, phaseName, procAll, selRegFlag, computeFlag, paramsDir);
    % catch
    %     disp('Terminated Flag 1');
    % end
    
    outDir = fullfile(outDirRoot, 'Regions_Flag2', filesep);
    procAll = 0;
    selRegFlag = 2;
    % try
    fn_combine_results_prot_phase_fwhm_width(inDir, outDir, protName, phaseName, procAll, selRegFlag, computeFlag, paramsDir);
    % catch
    %     disp('Terminated Flag 2');
    % end
    
    outDir = fullfile(outDirRoot, 'Regions_Flag3', filesep);
    procAll = 0;
    selRegFlag = 3;
    % try
    fn_combine_results_prot_phase_fwhm_width(inDir, outDir, protName, phaseName, procAll, selRegFlag, computeFlag, paramsDir);
    % catch
    %     disp('Terminated Flag 3');
    % end
    
    outDir = fullfile(outDirRoot, 'Regions_Flag4', filesep);
    procAll = 0;
    selRegFlag = 4;
    % try
    fn_combine_results_prot_phase_fwhm_width(inDir, outDir, protName, phaseName, procAll, selRegFlag, computeFlag, paramsDir);
    % catch
    %     disp('Terminated Flag 4');
    % end
    outDir = fullfile(outDirRoot, 'Regions_Flag5', filesep);
    procAll = 0;
    selRegFlag = 5;
    % try
    fn_combine_results_prot_phase_fwhm_width(inDir, outDir, protName, phaseName, procAll, selRegFlag, computeFlag, paramsDir);
    % catch
    %     disp('Terminated Flag 5');
    % end
end