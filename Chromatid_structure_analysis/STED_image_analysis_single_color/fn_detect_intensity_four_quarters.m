function [mask, mask1,mask2,mask3,mask4] = fn_detect_intensity_four_quarters(dxVol, dyVol, NzVol, radEll)
%radEll = 35;
inImage = zeros(dxVol,dxVol,NzVol);
dy = size(inImage, 2);
[outImage] = fn_DrawSphere(inImage, ceil(dxVol/2), ceil(dy/2), ceil(NzVol/2), radEll, 1,1, 1, 0);
mask = zeros(dxVol, dyVol, NzVol);
mask(:,1,:) = outImage(:,ceil(dy/2),:);
for i = 2:dyVol
    mask(:,i,:) = mask(:,1,:);
end
mask1 = zeros(dxVol,dyVol,NzVol);
mask1(1:ceil(dxVol/2),:,ceil(NzVol/2):end) = mask(1:ceil(dxVol/2),:,ceil(NzVol/2):end);
%figure; imagesc(squeeze(mask1));

mask2 = zeros(dxVol,dyVol,NzVol);
mask2(1:ceil(dxVol/2),:,1:ceil(NzVol/2)) = mask(1:ceil(dxVol/2),:,1:ceil(NzVol/2));
%figure; imagesc(squeeze(mask2));

mask3 = zeros(dxVol,dyVol,NzVol);
mask3(ceil(dxVol/2):end,:,1:ceil(NzVol/2)) = mask(ceil(dxVol/2):end,:,1:ceil(NzVol/2));
%figure; imagesc(squeeze(mask3));

mask4 = zeros(dxVol,dyVol,NzVol);
mask4(ceil(dxVol/2):end,:,ceil(NzVol/2):end) = mask(ceil(dxVol/2):end,:,ceil(NzVol/2):end);
%figure; imagesc(squeeze(mask4));
