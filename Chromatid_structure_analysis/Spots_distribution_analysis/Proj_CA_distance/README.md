This module projects spots onto the central axis and calculates the nearest neighbor distances of the projected spots (AS). It then combines the results per condensin subunit and mitotic phase.

To run the pipeline
*Set input output directories
*Execute Calculate_spot_projected_distance_main.m
*Execute Combine_results_spot_ca_projected_dist.m