function fn_generate_max_projected_condensin(inDirRoot, outDirRoot)
%This function generates maximum projected image for individual condensin region
%Author: M. Julius Hossain, EMBL
%Last update: 2017_09_14

close all;
fpRegFlag = fopen(fullfile(inDirRoot, 'region_type_flag.txt'),'r');
if fpRegFlag == -1
    return;
else
    curLine = fgets(fpRegFlag);
    regFlag = sscanf(curLine, '%d');
    if regFlag <=0
        return;
    end
end


inDirSeg = fullfile(inDirRoot, 'Segmented_BinMask', filesep);
inDirReg = fullfile(inDirRoot, 'Registered', filesep);

%filenames = dir([inDirSeg '*.tif']); %Name of the maternal chr file
filenames = dir([inDirSeg '*Cond*.tif']);

%for fIndex = 1:length(filenames)
fn = filenames(1).name;
sIdx = find(inDirSeg == filesep, 3,'last');

outDir    = [outDirRoot 'Processed_cond_volume' filesep];

%xlsFileName = [outDir 'Length_measurments.xls'];

%regionName = inDirSeg(sIdx(1)+1:sIdx(2)-1);

outDirReg = [outDir 'Registered' filesep];
outDirRegMaxProj = [outDir 'Reg_Max_Proj' filesep];
outDirRegCrop = [outDir 'Registered_Crop' filesep];
outDirBinMask = [outDir 'Bin_Mask' filesep];

if ~exist(outDirReg)
    mkdir(outDirReg);
end
if ~exist(outDirRegCrop)
    mkdir(outDirRegCrop);
end
if ~exist(outDirBinMask)
    mkdir(outDirBinMask);
end
if ~exist(outDirRegMaxProj)
    mkdir(outDirRegMaxProj);
end

outFilename = [fn(1:end-4) '_flag_' num2str(regFlag, '%02d'), '.tif'];
    
tic
[conRegionSegOrig] = fn_open_tiff_stack(inDirSeg, fn);
[conRegionRegOrig] = fn_open_tiff_stack(inDirReg, fn);
fn_write_tiff_stack(conRegionRegOrig, outDirReg, outFilename, 16);
fn_write_tiff_stack(conRegionSegOrig, outDirBinMask, outFilename, 16);

conRegionRegOrig(conRegionSegOrig(:,:,:) == 0) = 0;
maxProjReg = max(conRegionRegOrig, [], 3);
fn_write_tiff_stack(conRegionRegOrig, outDirRegCrop, outFilename, 16);
imwrite(uint16(maxProjReg), [outDirRegMaxProj outFilename], 'tif');
end
%end