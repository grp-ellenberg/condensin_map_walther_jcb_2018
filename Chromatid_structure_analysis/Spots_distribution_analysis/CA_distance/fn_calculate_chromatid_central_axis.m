function [procStat] = fn_calculate_chromatid_central_axis(inDirRoot, outDirRoot, outDirMaxProj, fIndex, voxelSizeX, voxelSizeY, voxelSizeZ)
%This function caculates central axis of chromatid for individual regions

%inDirRoot: root directory for input data
%outDirRoot: root directory for storing results
%outDirMaxProj: output directory to save maximumProjected image with
%central axis
%fIndex: index of the current region to be procesed
%voxelSizeX: x pixel size in micrometer
%voxelSizeY: y pixel size in micrometer
%voxelSizeZ: z pixel size in micrometer
%Author: M. Julius Hossain, EMBL
%Last update: 2017_09_14
%clc;
close all;
procStat = 0;
inDirSeg = fullfile(inDirRoot, 'Segmented_BinMask', filesep);
inDirReg = fullfile(inDirRoot, 'Registered', filesep);

se3d = zeros(3,3,3);
se3d(2,2,1) = 1; se3d(2,2,2) = 1; se3d(2,1,2) = 1; se3d(1,2,2) = 1; se3d(2,3,2) = 1; se3d(3,2,2) = 1; se3d(2,2,3) = 1;
if ~exist(inDirSeg)|| ~exist(inDirReg)
    disp('BinMask and/or Registered directory are missing');
    return;
end

se3d = zeros(3,3,3);
se3d(:,:,1) = [0 0 0; 0 0 0; 0 0 0];
se3d(:,:,2) = [0 1 0; 1 1 1; 0 1 0];
se3d(:,:,3) = [0 0 0; 0 0 0; 0 0 0];

%filenames = dir([inDirSeg '*.tif']); %Name of the maternal chr file
filenames = dir([inDirSeg '*Cond*.tif']);

%for fIndex = 1:length(filenames)
fn = filenames(1).name;
fpRegFlag = fopen(fullfile(inDirRoot, 'region_type_flag.txt'),'r');
if fpRegFlag == -1
    disp('region_type_flag.txt is missing');
    return;
else
    curLine = fgets(fpRegFlag);
    regFlag = sscanf(curLine, '%d');
    if regFlag <=0
        disp('region type flag is 0 or negative');
        return;
    end
end
outFilename = [fn(1:end-4) '_flag_' num2str(regFlag, '%02d'), '.tif'];

sIdx = find(inDirSeg == filesep, 3,'last');
%outDir    = [inDirSeg(1:sIdx(1)) 'Results_' dateStr(8:11) '_' dateStr(4:6) '_' dateStr(1:2) filesep];
%outDirParams = [outDirRoot 'Chr_length' filesep];

%xlsFileName = [outDirParams 'Length_measurments.xls'];

%regionName = inDirSeg(sIdx(1)+1:sIdx(2)-1);
% outDirSelVis = [outDir 'SelVis' filesep];
% outDirAllVis = [outDir 'AllVis' filesep];
%if ~exist(outDirParams)
%    mkdir(outDirParams);
%end

tic
[conRegionSegOrig] = fn_open_tiff_stack(inDirSeg, fn);
toc
[dx, dy, NzOrig] = size(conRegionSegOrig);
%disp(['Processing: ' inDirSeg]);

voxelSizeOrig = voxelSizeX*voxelSizeY*voxelSizeZ;
conRegionSegOrig(conRegionSegOrig>0) = 1;
conRegionSegOrig(conRegionSegOrig < 0) = 0;


%zFactor = round(voxelSizeZ/voxelSizeX);
%     [dx, dy, ~] = size(nucStackLow);
%Nz = NzOrig * zFactor - zFactor + 1;
%voxelSizeZ = voxelSizeX/zFactor;
%voxelSize = voxelSizeX*voxelSizeY*voxelSizeZ;
%conRegionSeg = fn_PC_GenIntermediateSlices(conRegionSegOrig, zFactor);

%conRegionSeg = conRegionSegOrig;
conRegionSeg = conRegionSegOrig;
conRegionSeg(conRegionSeg>0) = 1;
conRegionSegBound = imsubtract(conRegionSeg, imerode(conRegionSeg, se3d));
conRegionSegBound(conRegionSegBound>0) = 1;
[x,y,z] = ls_threed_coord(find(conRegionSegBound),dx,dy);
x = x* voxelSizeX;
y = y* voxelSizeY;
z = z* voxelSizeZ;
boundPoints = [x y z];
intDist = ipdm(boundPoints);
%maxDist = max(max(intDist));
%inPixDist = ipdm([x1 y1]);
[maxDist, linIdx] = max(intDist(:));
[ptIdx1, ptIdx2] = ind2sub(size(intDist),linIdx);

conVect = [x(ptIdx1) - x(ptIdx2) y(ptIdx1) - y(ptIdx2) z(ptIdx1) - z(ptIdx2)];
normConVect = norm(conVect);
conAxis = conVect/normConVect;
numPoints = floor(normConVect/voxelSizeX);
xConPoints = zeros(numPoints+1,1);
yConPoints = zeros(numPoints+1,1);
zConPoints = zeros(numPoints+1,1);
i = 1;
zFactor = round(voxelSizeZ/voxelSizeX); % zFactor-1 intermediate slice(s) will be generated
voxelSizeZCrop = voxelSizeZ/zFactor;
for alpha = 0:1/numPoints:1
    xConPoints(i) = x(ptIdx1)/voxelSizeX*(1-alpha) + x(ptIdx2)/voxelSizeX*alpha;
    yConPoints(i) = y(ptIdx1)/voxelSizeY*(1-alpha) + y(ptIdx2)/voxelSizeY*alpha;
    zConPoints(i) = z(ptIdx1)/voxelSizeZCrop*(1-alpha) + z(ptIdx2)/voxelSizeZCrop*alpha;
    i = i + 1;
end

maxX = round(max(x)/voxelSizeX);
minX = round(min(x)/voxelSizeX);
maxY = round(max(y)/voxelSizeY);
minY = round(min(y)/voxelSizeY);
maxZ = round(max(z)/voxelSizeZ);
minZ = round(min(z)/voxelSizeZ);

dxCrop = maxX-minX+1;
dyCrop = maxY-minY+1;
NzCropOrig = maxZ-minZ+1;
cropVolOrig = zeros(dxCrop, dyCrop, NzCropOrig);

cropVolOrig = conRegionSegOrig(minX:maxX,minY:maxY,minZ:maxZ);

NzCrop = NzCropOrig * zFactor -zFactor +1;
cropVol   = ls_gen_intermediate_slices(cropVolOrig, zFactor);
refVol = sum(sum(sum(cropVol >0))) *voxelSizeOrig;
voxelSize = voxelSizeX* voxelSizeY * voxelSizeZCrop;
cropVol = ls_smooth_and_equalize(cropVol, refVol, voxelSize);
cropVol(cropVol > 0) = 1;
cropVol(cropVol < 0) = 0;
xConPoints = round(xConPoints - minX + 1);
yConPoints = round(yConPoints - minY + 1);
minZ = round(min(z)/voxelSizeZCrop);
zConPoints = round(zConPoints - minZ + 1);
[refCentX, refCentY, refCentZ] = extract_centroids_along_chromosome_axis(cropVol, xConPoints, yConPoints, zConPoints, conAxis(1), conAxis(2), conAxis(3), 75);
centAxisX_pix = refCentX + minX-1;
centAxisY_pix = refCentY + minY-1;
centAxisZ_pix = refCentZ + minZ-1;
h = figure('NumberTitle','off', 'Visible','off'); imagesc(max(conRegionSegOrig, [], 3)); axis equal; axis tight;  colorbar
hold on; plot(round(centAxisY_pix), round(centAxisX_pix), 'r');
filename = [outDirRoot outFilename];
saveas(h, filename, 'tif');  
filename = [outDirRoot outFilename(1:end-4) '.txt'];

tableCentAxis = table(centAxisX_pix, centAxisY_pix, centAxisZ_pix);
writetable(tableCentAxis,filename, 'Delimiter','\t');

filename = [outDirMaxProj outFilename(1:end-4) '.tif'];
imwrite(uint16(max(conRegionSegOrig, [], 3)),filename, 'tif'); 
procStat = 1;
close all;
end
%end