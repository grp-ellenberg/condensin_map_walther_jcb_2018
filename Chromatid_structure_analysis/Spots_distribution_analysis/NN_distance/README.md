This module calculates nearest neighbors distance of condensin spots (NN). It then combines the results per condensin subunit and mitotic phase.

To run the pipeline
*Set input output directories
*Execute calculate_spot_nn_distance_indiv_region.m
*Execute combine_spot_nn_distance_results_all_regions.m
