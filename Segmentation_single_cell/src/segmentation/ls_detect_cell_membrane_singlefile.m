function [procStat] = ls_detect_cell_membrane_singlefile(imgfile,  curOutDir, chrChanIdx, cellChanIdx, ...
    varargin)
% segment outer boundary
% INPUT:
%   imgfile - an image file
%
% curInDir: Input data directory
% curOutDir: Output data directory
% curOutDirMarker: Directory containing chromosome markers saved by ls_detect_chromosomes
% exParamsFn: Name of the file storing the extracted parameters.
% cellChanIdx: Channel index of dextran
% chrChanIdx: Channel index of chromosome
% nRemLowerSlices: Number of lower slices to be removed
% chrSigInDxt: Set 1 if chromosome signal is also prominent in dextral channel

% Author: Julius Hossain, EMBL, Heidelberg, Germany, julius.hossain@embl.de
% Created: 2014-11-13
% Last update: 2017-06-13
% This function detects cell boundary and extracts different parameters
par = inputParser;
addRequired(par, 'imgfile', @ischar);               % name of image file
addRequired(par, 'curOutDir', @ischar);             % name of output directory
addRequired(par, 'chrChanIdx',  @isnumeric);        % channel containing chromatin signal
addRequired(par, 'cellChanIdx',  @isnumeric);        % channel containing chromatin signal
addParameter(par, 'segParamsDir', fullfile(curOutDir, 'SegParams'), @ischar);          % Directory where to save segmentation parameters
addParameter(par, 'exParamsFn', 'Extracted_parameters.txt', @ischar);            % File to save of the extraceted parameters
addParameter(par, 'dispDir',  fullfile(curOutDir, 'DispSegMass'),  @ischar);    % Set 1 to save 3d reconstructed chromosome as jpg
addParameter(par, 'nRemLowerSlices', 0, @isnumeric); % number of lower slices to exclude from segmentation
addParameter(par, 'curOutDirMarker', fullfile(curOutDir, 'ChrMarker'), @ischar); % directory where marker (chromatin signal) for watershed are saved
addParameter(par, 'chrSigInDxt', 0, @isnumeric)     % Set 1 if chromosome signal is also prominent in dextral channel
parse(par, imgfile, curOutDir, chrChanIdx, cellChanIdx,  varargin{:});
par = par.Results;
[path, fname] = fileparts(par.imgfile);


matMarkerFn = fullfile(par.curOutDirMarker, [fname '.mat']);

if ~exist(matMarkerFn)
    error([imgfile '\n No mat file for chromatin marker has been found']);
end


paramsName = {'ChrVolMic', 'CellVolMic', 'NumChr'};
paramsTable = cell2table(cell(1,length(paramsName)));
paramsTable.Properties.VariableNames = paramsName;

%Structuring element
se = strel(ones(3,3,3));
%Define the maximum number of subplots to disply intermediate results
rCRegion = 1;
bFactorRatio = 0.20;
procStat = 0;

%Filtering parameters
sigmaBlur = 5; %Sigma for negative staining channel
hSizeBlur = 7; %Kernel size for negative staining channel

sigmaRatio=3; %Sigma for gaussian blur in ratio image.
hSizeRatio=5; %Kernel size for gaussian blur in ratio image

sigmaChSeg=3; %Sigma for gaussian blur in chromosome segmentation
hSizeChSeg=5; %Kernel size for gaussian blur in chromosome segmentation
bFactorCh2D = 0.33;
minVol = 500;

color = {[0.05 0.5 0.15], [0.55 0.1 0.1]};
thresh = [0.5 0.5];
alpha = [0.45 0.99];

%% read image using bioformats
[reader, dim] = getBioformatsReader(par.imgfile);
% extract image dimensions
dx = dim.Nx; %Image height
dy = dim.Ny; % Image width
NzOrig = dim.Nz; %Number of Z slices in lsm file
Nc = dim.Nc; %Number of channels
lsminf = lsminfo(imgfile); %lsm header

tinit = 1; % Set higher than one if you do not want to process some timepoints before tinit
zinit = 1; % Set higher than one if you do not want to process some lower slices

zinit = 1; %Set higher than one if you want to remove some of the lower slices
%Size of voxels in x, y and z in micro meter
voxelSizeX = dim.voxelSize(1);
voxelSizeY =  dim.voxelSize(2);
voxelSizeZ =  dim.voxelSize(3);

zFactor = round(voxelSizeZ/voxelSizeX); % zFactor-1 intermediate slice(s) will be generated
voxelSizeZ = voxelSizeZ/zFactor; % Update voxelSizeZ based on the number of intermediate slices
voxelSize = voxelSizeX * voxelSizeY * voxelSizeZ; %Volume of a voxel in cubic micro meter
Nz = (NzOrig-zinit+1) * zFactor - zFactor + 1; %Number of z slice after intepolation

if Nc >= chrChanIdx && Nc >= cellChanIdx
    chStackOrig = getTPointBioFormats(reader, dim, tinit, chrChanIdx);
    negStackOrig = getTPointBioFormats(reader, dim, tinit, cellChanIdx);
else
    error('Stack contains less than three channels')
end

chrPack = load(matMarkerFn);

tpoint = tinit;

if length(par.imgfile) > 100
    disp(['Processing cell seg. : ...' par.imgfile(end-100:end)]);
else
    disp(['Processing cell seg. : ' par.imgfile]);
end
chStack = ls_gen_intermediate_slices(chStackOrig, zFactor);
negStack = ls_gen_intermediate_slices(negStackOrig, zFactor);

chStackBack  = chStack;
negStackBack = negStack;

if tpoint == tinit
    tempVol = zeros(dx,dy,Nz);   %Store current segmented volume  as a referece for next/prev time point
    cRegion = zeros(dx,dy,Nz);   %Store the detected cytoplasm region
    chRegion = zeros(dx,dy,Nz);  %Store the detected chromosome region
end

chStack = imgaussian(chStack,sigmaBlur,hSizeBlur);
negStack = imgaussian(negStack, sigmaBlur, hSizeBlur);

%% Detection of chromosome
chStackSeg = imgaussian(chStackBack, sigmaChSeg, hSizeChSeg);
%[chThresh3D, histCh] = Otsu_3D_Img(chStackSeg, 0);
chThresh2D = chrPack.chrThresh2D;
chThresh3D = chrPack.chrThresh3D;
if par.chrSigInDxt == 1
    chRegion(:,:,:) = 0;
    %Update threshold for different slices
    for i = 1:Nz
        chRegion(:,:,i) = double(chStackSeg(:,:,i) >= (chThresh3D * (1-bFactorCh2D) + chThresh2D(i) * bFactorCh2D));
    end
    
    chStackNorm = chStack;
    chStackNorm(chRegion(:,:,:) == 1) = 0;
    sumBgAvgInt = 0;
    for i = 1: Nz
        totalBgInt = sum(sum(chStackNorm(:,:,i)));
        totalBgPix = sum(sum(chRegion(:,:,i)==0));
        avgBgInt = totalBgInt/totalBgPix;
        tempFrame = chStack(:,:,i);
        tempFrame(chStackSeg(:,:,i) < avgBgInt) = avgBgInt;
        chStackNorm(:,:,i) = tempFrame;
        sumBgAvgInt = sumBgAvgInt + avgBgInt;
    end
    gAvgBgInt = sumBgAvgInt/Nz;
    ratioImage = (gAvgBgInt * negStack)./chStackNorm;
    
    ratioImage = imgaussian(ratioImage, sigmaRatio, hSizeRatio);
    clear chStackNorm;
    clear tempFrame;
else
    ratioImage = negStack;
end

[cThresh3D, histRatio] = ls_otsu_3d_image(ratioImage, 0);
negNumVoxels = dx*dy*Nz*0.01;
sumVoxels = 0;
for i = length(histRatio):-1: 1
    sumVoxels = sumVoxels + histRatio(i);
    if sumVoxels >= negNumVoxels
        break;
    end
end

ratioImage = i - ratioImage;
ratioImage(ratioImage(:,:,:) <0) = 0;

if par.nRemLowerSlices > 0
    ratioImage(:,:,1:nRemLowerSlices*zFactor) = 0;
    [cThresh3D, histRatio] = ls_otsu_3d_image(ratioImage, 1);
else
    [cThresh3D, histRatio] = ls_otsu_3d_image(ratioImage, 0);
end


cRegion(:,:,:) = 0;
for i = 1:Nz
    cThresh2D = ls_otsu_2d(ratioImage(:,:,i));
    cRegion(:,:,i) = double(ratioImage(:,:,i) >= (cThresh3D * (1-bFactorRatio) + cThresh2D * bFactorRatio));
end

bgMaskInt = cRegion;
threeDLabel = bwconncomp(cRegion,18);
numPixels = cellfun(@numel,threeDLabel.PixelIdxList);
for i = 1 : numel(numPixels)
    if numPixels(i) * voxelSize < minVol
        cRegion(threeDLabel.PixelIdxList{i}) = 0;
    end
end

%% Filling of thresholded image
for zplane = 1: Nz
    cRegion(:,:,zplane)=imdilate(cRegion(:,:,zplane),strel('disk',rCRegion,0));
    cRegion(:,:,zplane)=imfill(cRegion(:,:,zplane),'holes');
    bgMaskInt(:,:,zplane)=imdilate(bgMaskInt(:,:,zplane),strel('disk',rCRegion,0));
    bgMaskInt(:,:,zplane)=imfill(bgMaskInt(:,:,zplane),'holes');
    for i=1:rCRegion
        cRegion(:,:,zplane)=imerode(cRegion(:,:,zplane),strel('diamond',1));
    end
end

bgMaskInt = 1-bgMaskInt;
bgStack = negStackBack;
bgStack(bgMaskInt(:,:,:) == 0) = 0;
totBgInt = sum(sum(sum((bgStack(:,:,round(Nz*0.25):round(Nz*0.75))))));
totBgPix = sum(sum(sum((bgMaskInt(:,:,round(Nz*0.25):round(Nz*0.75))))));

%% Combining distance transform and watershed to identify cell region of interest
distImage = bwdistsc(~cRegion,[1 1 voxelSizeZ/voxelSizeX]);

cRegionEroded = imerode(cRegion, se);

chrPack.chrMarker(cRegionEroded(:,:,:) == 0) = 0;
chrPack.chrMarker(:,1,round(Nz/20):round(Nz/2)) = 1;
chrPack.chrMarker(1,:,round(Nz/20):round(Nz/2)) = 1;
chrPack.chrMarker(:,dy,round(Nz/20):round(Nz/2)) = 1;
chrPack.chrMarker(dx,:,round(Nz/20):round(Nz/2)) = 1;

clear cRegionEroded;

distImage = -distImage; % invert distance image
%displaySubplots(distImage, 'DT of inverse', disRow, disCol, Nz, zFactor, 2);
distImage = imimposemin(distImage,chrPack.chrMarker, 18); %suppress local minima other than markers
%fn_PC_DisplaySubplots(distImage, 'DT after suppression', disRow, disCol, Nz, zFactor, 2);
distImage(~cRegion) = -Inf; % Set outside of cell region with infinity
%fn_PC_DisplaySubplots(distImage, 'DT after suppression - inf', disRow, disCol, Nz, zFactor, 2);
wsLabel = watershed_old(distImage); %Apply watershed algorithm

wsLabel(~cRegion) = 0;
%fn_PC_DisplaySubplots(wsLabel, 'Watershed on distance image - background removed', disRow, disCol, Nz, zFactor, 2);

threeDLabel = bwconncomp(wsLabel,18);
wsLabel(:,:,:) = 0;
for i = 1: threeDLabel.NumObjects
    wsLabel(threeDLabel.PixelIdxList{i}) = i;
end

if chrPack.numChr == 1
    labSum = sum(sum(sum(wsLabel(chrPack.chromosomes(:,:,:) == 1))));
    labCount = sum(sum(sum(and(chrPack.chromosomes(:,:,:) ==1,  wsLabel(:,:,:) >=1))));
    lbl1 = round(labSum/labCount);
    wsLabel(wsLabel(:,:,:) ~= lbl1) = 0;
else
    tempVol(:,:,:) = 0;
    labSum = sum(sum(sum(wsLabel(chrPack.chromosomes(:,:,:) == 1))));
    labCount = sum(sum(sum(and(chrPack.chromosomes(:,:,:) == 1, wsLabel(:,:,:) >=1))));
    lbl1 = round(labSum/labCount);
    
    tempVol(wsLabel(:,:,:) == lbl1) = 1;
    labSum = sum(sum(sum(wsLabel(chrPack.chromosomes(:,:,:) == 2))));
    labCount = sum(sum(sum(and(chrPack.chromosomes(:,:,:) == 2, wsLabel(:,:,:) >=1))));
    lbl2 = round(labSum/labCount);
    tempVol(wsLabel(:,:,:) == lbl2) = 1;
    
    wsLabel = tempVol;
end
wsLabel(wsLabel(:,:,:)>0) = 1;

%fn_PC_DisplaySubplots(wsLabel, 'Watershed on distance image - background removed', disRow, disCol, Nz, zFactor, 2);
wsLabel = imclose(wsLabel, se);
%To deal with the filtering
wsLabel = imclose(wsLabel,se);
wsLabel = imdilate(wsLabel,se);

%Store  initial detected cell region as 'cRegion'
cRegion = wsLabel;
%displaySubplots(cRegion, 'Selected blob using Watershed + DT raw', disRow, disCol, Nz, zFactor, 2);

cRegionDis = cRegion;
cRegion(:,:,:) = 0;
for zplane=1:Nz
    curBlob = ls_find_biggest_object(cRegionDis(:,:,zplane),8);
    if sum(sum(curBlob))*voxelSizeX*voxelSizeY <=10
        continue;
    end
    
    cRegionDis(:,:,zplane) = imsubtract(cRegionDis(:,:,zplane), curBlob);
    cRegion(:,:,zplane) = curBlob;
    
    curBlob = ls_find_biggest_object(cRegionDis(:,:,zplane),8);
    if sum(sum(curBlob))*voxelSizeX*voxelSizeY <=10
        continue;
    end
    cRegion(:,:,zplane) = or(cRegion(:,:,zplane), curBlob);
end
threeDLabel = bwconncomp(cRegion,18);
numPixels = cellfun(@numel,threeDLabel.PixelIdxList);
[~, idx] = max(numPixels);
cRegion(:,:,:) = 0;
if threeDLabel.NumObjects > 0
    cRegion(threeDLabel.PixelIdxList{idx}) = 1;
    numPixels(idx) = 0;
    [biggest, idx] = max(numPixels);
    if biggest *voxelSizeX * voxelSizeY * voxelSizeZ >=1000
        cRegion(threeDLabel.PixelIdxList{idx}) = 1;
    end
end
%% End of DT + WS
refVol = sum(sum(sum(cRegion))) * voxelSizeX * voxelSizeY * voxelSizeZ;
cRegion = ls_smooth_and_equalize(cRegion, refVol, voxelSizeX*voxelSizeY*voxelSizeZ);

for i=1:Nz
    cRegion(:,:,i) = imfill(cRegion(:,:,i), 'holes');
end

for i = round(Nz*0.7):Nz-1
    cRegion(:,:,i+1) = double(and(cRegion(:,:,i+1), cRegion(:,:,i)));
end
chrPack.chromosomes(cRegion(:,:,:) == 0) = 0;

%% Save results in mat file
chrVolMic  = sum(chrPack.chromosomes(:)>0) * voxelSize;
cellVolMic = sum(cRegion(:)>0) * voxelSize;
cellMass = ls_remove_intermediate_slices(cRegion, zFactor);
chrMass = ls_remove_intermediate_slices(chrPack.chromosomes, zFactor);
numChr = chrPack.numChr;

savefile =  fullfile(curOutDir, [fname '.mat']);
save(savefile, 'cellMass', 'chrMass', 'chrVolMic', 'cellVolMic', 'numChr');

newTable = cell2table({chrVolMic,cellVolMic, numChr});
newTable.Properties.VariableNames = paramsName;
paramsTable = cat(1,paramsTable,newTable);

if exist(par.dispDir)
    hVol = figure('Name', 'Segmentation', 'NumberTitle','off', 'Visible', 'off');
    [X,Y,Z]=meshgrid((1:dy)*voxelSizeY,(1:dx)*voxelSizeX,(1:Nz)*voxelSizeZ);
    p1=patch(isosurface(X, Y, Z,cRegion));
    
    p2 = patch(isosurface(X, Y, Z, chrPack.chromosomes));
    isonormals(X, Y, Z,cRegion, p1);
    isonormals(X, Y, Z,chrPack.chromosomes, p2);
    set(p1,...
        'FaceColor','blue',...
        'EdgeColor','none', 'FaceAlpha', 0.2);
    set(p2,...
        'FaceColor','red',...
        'EdgeColor','none',  'FaceAlpha', 0.8);
    daspect([1 1 1])
    view(3)
    camlight;
    lighting phong;
    savefile = fullfile(par.dispDir, [fname  '.jpg']);
    saveas(hVol, savefile, 'jpg');
    delete(hVol);
end

clear cellVolume;
clear nucVolume;
clear cellRegion;
clear chrRegion;
clear numChr;
clear wsLabel;
clear distImage;
clear chrMarker;
clear hist;
close all;
clear chrPack;


paramsFn = fullfile(curOutDir,  par.exParamsFn);
paramsTable(1,:) = [];
writetable(paramsTable,paramsFn,'Delimiter','\t');
procStat = 1;
end