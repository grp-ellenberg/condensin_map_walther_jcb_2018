function ls_refine_chromosome_segmentation(curCellDir, fpOutRoot, expDatabase, cellIdx, constPor)
%This function resegment chomosome mass obtained by mitosys pipeline
%curCellDir: Directory of the cell to be procecessed
%fpOutRoot: Root directory of the output folder
%expDatabase: name of the database file
%cellIdx: row number of current cell in the database
%constPor: portion of the total intensity is taken as reference intensity

%Author: M. Julius Hossain, EMBL Heidelberg
%Last update: 2017-04-07

disp(curCellDir);

sigma = 1.2;
hsize = 2;
thInc = 25;
fpOut = fullfile(fpOutRoot, expDatabase.filepath{cellIdx}(26:end), 'Preprocessing', 'Segmentation_RefChr', filesep);
if ~exist(curCellDir, 'dir')
    disp('Input directory is missing');
    return;
else
    if ~exist(fpOut, 'dir')
        mkdir(fpOut);
    end
end

fn = dir([curCellDir '*.mat']);
if isempty(fn) == 1
    return;
end

mPhase = {'G2','Pro', 'Prometa', 'Meta', 'Ana', 'Telo'};

%Starting from the middle - finds the time point of the last metaphase to take as a reference
tpoint = round(length(fn)/2);
prevStageIdx = -1;
selTpoint = -1;
while tpoint <= length(fn) && tpoint>=1
    curMat = load([curCellDir fn(tpoint).name]);
    stageIdx = strmatch(curMat.mitoTime, mPhase);
    
    if prevStageIdx == 4 && stageIdx == 5
        selTpoint = tpoint - 1;
        break;
    end
    if prevStageIdx == 5 && stageIdx == 4
        selTpoint = tpoint;
        break;
    end
    
    if stageIdx <= 4
        tpoint = tpoint + 1;
    else
        tpoint = tpoint - 1;
    end
    prevStageIdx = stageIdx;
end

if selTpoint == -1
    return;
end

%To avoid the last metaphase being very early anaphase!
if selTpoint >1
    selTpoint = selTpoint - 1;
end

Nz = size(curMat.nucVolume,3);
tpoint = selTpoint;
dirFlag = -1;
while tpoint <= length(fn)
    disp(['Processing tpoint: ' num2str(tpoint)]);
    curMat = load([curCellDir fn(tpoint).name]);
    nucVolume = curMat.nucVolume;
    nuc = curMat.nuc;
    
    for zplane = 1: Nz
        nuc(:,:,zplane) = imgaussian(nuc(:,:,zplane), sigma, hsize);
    end
    
    [avgBackInt, ~, ~] = ls_calculate_background_intensity(nucVolume, nuc, 4, 2);
    nucVolDil = nucVolume;
    for zplane = 1: Nz
        nucVolDil(:,:,zplane) = imdilate(nucVolume(:,:,zplane), strel('diamond', 2));
    end
    
    %Resegment using histogram inside the nuc region
    chopNucDil = nuc;
    chopNucDil(nucVolDil==0)=0;
    [nucThresh3D, ~] = ls_otsu_3d_image(chopNucDil, 1);
    nucVolHist = double(chopNucDil>=nucThresh3D);
    
    %constrained segmentation
    chopNuc = nuc;
    chopNuc(nucVolume==0)=0;
    numPix = sum(sum(sum(nucVolume>0)));
    totInt = sum(sum(sum(chopNuc))) - avgBackInt*numPix;
    nucVolCons = nucVolume; %nucVolHist;
    if tpoint == selTpoint
        refTotInt = totInt*constPor; %sum(sum(sum(chopNuc(nucVolHist>0))));
    end
    curTotInt = totInt; %sum(sum(sum(chopNuc(nucVolume>0))));
    nucThresh3D = min(min(min(chopNuc(chopNuc>0))));
    while(curTotInt > refTotInt)
        nucThresh3D = nucThresh3D + thInc;
        nucVolCons = double(chopNuc>=nucThresh3D);
        curNumPix = sum(sum(sum(nucVolCons>0)));
        curTotInt = sum(sum(sum(chopNuc(nucVolCons>0))))-avgBackInt * curNumPix;
    end
    
    i = 0;
    while(curTotInt < refTotInt && i < thInc)
        nucThresh3D = nucThresh3D - 1;
        nucVolCons = double(chopNuc>=nucThresh3D);
        curNumPix = sum(sum(sum(nucVolCons>0)));
        curTotInt = sum(sum(sum(chopNuc(nucVolCons>0))))-avgBackInt * curNumPix;
        i = i+1;
    end
    
    nucVolCons = nucVolume.*nucVolCons;
    nucVolHist = nucVolume.*nucVolHist;
%     nucCons = nuc;
%     nucCons(nucVolCons == 0) = 0;
%     nucHist = nuc;
%     nucHist(nucVolHist == 0) = 0;
%     nuc = chopNuc;
    savefile = [fpOut fn(tpoint).name];
    save(savefile,'nucVolCons','nucVolHist');
    
    tpoint = tpoint+dirFlag;
    if tpoint == 0
        tpoint = selTpoint + 1;
        dirFlag = 1;
    end
end
end