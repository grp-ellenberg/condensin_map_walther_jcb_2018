function ls_segment_landmarks( expDir, expDatabaseFilename, reproc )
% Author: M. Julius Hossain, EMBL Heidelberg.
% Created: 2016-06-20
% Last update: 2016-06-21
% This function segments lanmarks (cell and chromosome volumes) and extracts
% different parameters from the segmented data

preprocDir = ['Preprocessing' filesep 'Segmentation' filesep];
preprocDirMarker = ['Preprocessing' filesep 'Segmentation_Chr' filesep];
%preprocDirMarker saves chromosome markers temporarily
%newDataStructure = 0;
nRemLowerSlices = 0;
removeMarkerDir = 0;
overWriteMarker = 1;
saveIsotropicMatFiles = 1;
outDirIsoRoot = 'Y:\Julius\Data\NewPipelineIsoSegmentation\';

expDatabase = readtable([expDir filesep expDatabaseFilename],'Delimiter','\t');
numCells = size(expDatabase,1);
%lastProcessedSystem = '';
for cellIdx = 1: numCells
    disp(cellIdx);
    if expDatabase.manualQC(cellIdx) == 1 && (expDatabase.segmentation(cellIdx) == -1 || reproc == 1)
%         curProcessedSystem = [expDatabase.poi{cellIdx} num2str(expDatabase.system(1))];
%         if strcmp(lastProcessedSystem, curProcessedSystem) == 1
%             continue;
%         end
%         
        
        hrString = expDatabase.HR{cellIdx};
        if length(hrString)>=5 && isstrprop(hrString(5), 'digit')
            newDataStructure = 1;
        else
            newDataStructure = 0;
        end
        cellChanIdx = expDatabase.Ccell(cellIdx);
        chrChanIdx  = expDatabase.Cdna(cellIdx);
        protChanIdx = expDatabase.Cpoi(cellIdx);
        tMax = expDatabase.tmax(cellIdx);
        curHrImage = expDatabase.imgexp{cellIdx};
        fileSepIdx = find(curHrImage == filesep, 1,'last');
        curCellInDir = expDatabase.imgexp{cellIdx};
        curCellInDir = [expDir filesep curCellInDir(1:fileSepIdx)];
        curCellOutDir = [expDir filesep expDatabase.filepath{cellIdx} filesep preprocDir];
        curCellOutDirMarker = [expDir filesep expDatabase.filepath{cellIdx} filesep preprocDirMarker];
        hrLsmFilename = curHrImage(fileSepIdx+1:end);
        %try
            chrProcStat = 0;
            if exist (curCellOutDirMarker, 'dir') && overWriteMarker == 0
                chMatFN = dir([curCellOutDirMarker, '*.mat']);
                if numel(chMatFN) == tMax %Chromosomes segmented completely?
                    chrProcStat = 1;
                end
            end
            if overWriteMarker == 1 || chrProcStat == 0
                chrProcStat = ls_detect_chromosomes(curCellInDir, curCellOutDirMarker, hrLsmFilename, tMax, chrChanIdx, removeMarkerDir);
            end
%         catch
%             disp('Cant process this cell');
%         end
        
        if chrProcStat == 1
%              try
                fileSepIdx = find(curCellOutDir == filesep, 4,'last');
                curResDir =  curCellOutDir(1:fileSepIdx(1));
                fpSliceStart = fopen([curResDir 'slicestart.txt'],'r');
                if fpSliceStart == -1
                    nRemLowerSlices = 0;
                else
                    curLine = fgets(fpSliceStart);
                    sliceStart = sscanf(curLine, '%d');
                    nRemLowerSlices = max(sliceStart-1, 0);
                end
                    
                cellProcStat = ls_detect_cell_membrane(curCellInDir, curCellOutDirMarker, curCellOutDir, outDirIsoRoot, hrLsmFilename, tMax, cellChanIdx, chrChanIdx, protChanIdx, newDataStructure, saveIsotropicMatFiles, nRemLowerSlices, 0);
                expDatabase.segmentation(cellIdx) = cellProcStat;
%             catch
%                 disp('Cant process this cell');
%             end            
        end
%         if cellProcStat == 1
%             lastProcessedSystem = curProcessedSystem;
%         end
        writetable(expDatabase,[expDir filesep expDatabaseFilename],'Delimiter','\t'); 
        if removeMarkerDir == 1
%             try
                rmdir(curCellOutDirMarker, 's');
%             catch
%                 disp('Can not remove the directory');
%             end
        end
    end
end

