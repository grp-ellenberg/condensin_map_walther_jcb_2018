function [stackHom] = ls_inhomogeneous_to_homogeneous(stack)
%Replace very bright intensities with average 
[threshold, ~] = ls_otsu_3d_image(stack, 0);
%histogram(1:threshold) = 0;

%[threshold] = Otsu_3D_Hist(histogram,length(histogram), 0);
stackHom = stack;
stackHom(stack(:,:,:)>threshold) = threshold;
end