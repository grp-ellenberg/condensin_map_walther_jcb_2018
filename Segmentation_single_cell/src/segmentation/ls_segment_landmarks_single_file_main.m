function ls_segment_landmarks_single_file_main(curInDir, outDir)
% This function segments lanmarks - chromosome  and cell (optional)volumes and extracts
% different parameters from the segmented data process all files in folder
% Author: M. Julius Hossain, EMBL Heidelberg.
% Created: 2017-06-09
% Last update: 2017-06-13
clc;
close all;

global lastdir;
currdir = fileparts(mfilename('fullpath'));
% TODO this double the entry from  the batch_segment file but also allows to execute the file on its own
addpath(currdir, '-end');
addpath(fullfile(fileparts(currdir), 'segmentation_external'), '-end');

segCellBoundary = 1; % Set 0 to segment chromosome only
dispSegMass = 1; % Set 1 to save 3d reconstructed chromosome.
nRemLowerSlices = 0; % number of lower slices to exclude from segmentation
chrSigInDxt = 0; % Set 1 if chromosome signal is also prominent in dextral channel
cellChanIdx = 2; % Channel index of dextran
chrChanIdx  = 3; % Channel index of chromatin
exParamsFn  = 'Extracted_parameters.txt'; % Name of the file to save the extracted parameters
if nargin < 1
    
    if lastdir
        curInDir = uigetdir(lastdir,'Select the root directory of your data (expDirRoot)');
    else
        curInDir = uigetdir('','Select the root directory of your data (expDirRoot)');
    end
end
lastdir = curInDir;
if nargin < 2
    outDir = uigetdir(lastdir, 'Select the root directory to store the results (outDirRoot)');
end
lsmfiles = dir(fullfile(curInDir, '*.lsm'));
imgfiles = {};
for i = 1:length(lsmfiles)
    imgfiles = {imgfiles{:}, fullfile(curInDir, lsmfiles(i).name)};
end
for imgfile = imgfiles(1:end)
    % process chromosomes
    [fpath, fname] = fileparts(imgfile{1});
    if ~exist(fullfile(outDir, [fname '.mat']), 'file')
        if segCellBoundary == 1
            chrProcStat = ls_detect_chromosomes_singlefile(imgfile{1}, outDir, chrChanIdx, 'segParamsDir' , fullfile(outDir, 'SegParams', filesep), ...
                'exParamsFn', 'Extracted_parameters.txt',  'dispDir', fullfile(outDir, 'DispSegMass'),  'curOutDirMarker', fullfile(outDir, 'ChrMarker'), 'saveMarker', 1, 'nRemLowerSlices', 0);
        else
            chrProcStat = ls_detect_chromosomes_singlefile(imgfile{1}, outDir, chrChanIdx, 'segParamsDir' , fullfile(outDir, 'SegParams', filesep), ...
                'exParamsFn', 'Extracted_parameters.txt',  'dispDir', fullfile(curOutDir, 'DispSegMass'), 'nRemLowerSlices', 0, 'curOutDirMarker', fullfile(outDir, 'ChrMarker'), 'saveMarker', 0);
        end
    else
        
      if length(imgfile{1}) > 100
        disp(['Chromatin seg. has already been performed: ...' imgfile{1}(end-100:end)]);
      else
        disp(['Chromatin seg. has already been performed: ...' imgfile{1}(end:end)]);
      end
        chrProcStat = 1;
    end
    
    if  segCellBoundary && chrProcStat == 1
        variable = who('-file',fullfile(outDir, [fname '.mat']),'cellMass');
        if isempty(variable)
            cellProcStat = ls_detect_cell_membrane_singlefile(imgfile{1}, outDir, chrChanIdx, cellChanIdx, 'segParamsDir' , fullfile(outDir, 'SegParams', filesep), ...
                'exParamsFn', 'Extracted_parameters.txt',  'dispDir', fullfile(outDir, 'DispSegMass'),  'curOutDirMarker', fullfile(outDir, 'ChrMarker'),  'nRemLowerSlices', 0);
        else
          if length(imgfile{1}) > 100
                disp(['Cell seg. has already been performed: ...' imgfile{1}(end-100:end)]);
          else
             disp(['Cell seg. has already been performed: ...' imgfile{1}(end:end)]);
           end
    end
end
end

