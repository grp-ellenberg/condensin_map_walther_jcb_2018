function [procStat] = ls_detect_chromosomes_singlefile(imgfile, curOutDir, chrChanIdx, varargin)
% LS_DETECT_CHROMOSOMES
% This function segments chromosome from chrChan and save chromsome chrMarker for cell boundary detection
% INPUT:
%   imgfile: a czi or lsm file to be read and segmented
% OPTIONAL INPUTS:
%   chchr: chromatin channel
%   chout: outer boundary/dextran channel
%   chpoi: poi channel
% A single lms or czi file is passed to the code
% curInDir: full path to images containing time lapse
% curOutDir: full path to output directory where to save chromosome markers
% chrChaIdx: Index of chromosome channel
% exParamsFn: Name of the file storing the extracted parameters.

% Author: Julius Hossain, EMBL, Heidelberg, Germany: julius.hossain@embl.de
% Created: 2014-07-25
% Last update: 2017-06-12
% Last update: 2017-07-17 Antonio Politi
close all;
global lastdir;
if nargin < 1
    [imgfile, pathname] = uigetfile( {'*.lsm;*.czi', 'Image files'; '*.*', 'All files'},  'Specify an image file', lastdir);
   
    imgfile = fullfile(pathname,imgfile);
    if ~exist(imgfile)
        
        warning(sprintf('%s does not exists', imgfile));
        return
    else
         lastdir = pathname;
    end
end

if nargin < 2
    curOutDir = uigetdir(lastdir,  'Specify outputdir');
    if ~exist(curOutDir)
        return
    end
end
if nargin < 3
    x = inputdlg({'Chromosome Channel:'}, 'Specify channel', 1, {'3'})
    
    chrChaIdx = str2num(x{:});
end

par = inputParser;
addRequired(par, 'imgfile', @ischar);               % name of image file
addRequired(par, 'curOutDir', @ischar);             % name of output directory
addRequired(par, 'chrChanIdx',  @isnumeric);        % channel containing chromatin signal 

addParameter(par, 'segParamsDir', fullfile(curOutDir, 'SegParams'), @ischar);          % Directory where to save segmentation results 
addParameter(par, 'exParamsFn', 'Extracted_parameters.txt', @ischar);            % File to save of the extraceted parameters
addParameter(par, 'dispDir',  fullfile(curOutDir, 'DispSegMass'),  @ischar);    % Set 1 to save 3d reconstructed chromosome as jpg
addParameter(par, 'nRemLowerSlices', 0, @isnumeric); % number of lower slices to exclude from segmentation 
addParameter(par, 'curOutDirMarker', fullfile(curOutDir, 'ChrMarker'), @ischar);
addParameter(par, 'saveMarker', 1, @isnumeric);

%Filtering parameters
addParameter(par, 'sigmaBlur', 3, @isnumeric);         % Sigma for Gaussian filter
addParameter(par, 'hSizeBlur', 5, @isnumeric);         % Kernel size for Gaussian filter
addParameter(par, 'bFactor2D', 0.33, @isnumeric);      % Contribution of 2D threshold
addParameter(par, 'splitVol', 2000, @isnumeric);       % Minimum volume for to be considered for splitting
addParameter(par, 'mergeVol',150, @isnumeric);         % Maximum volume of a blob that is considered for merging
addParameter(par, 'minProphaseVol', 500, @isnumeric); % minimal volume to be inclded??
addParameter(par, 'maxCentDisp',  11, @isnumeric);     % ??
addParameter(par, 'maxNoInitObj', 450, @isnumeric);    % ??
addParameter(par, 'inHomCount', 0, @isnumeric);        % Defines the number of times we need to apply fn_InhomToHom(stk)
addParameter(par, 'inHomCheck', 1, @isnumeric);        % Flag whether to check homogeinity or not
parse(par, imgfile, curOutDir, chrChanIdx,  varargin{:});
tinit = 1;


par = par.Results;
[path, fname] = fileparts(par.imgfile);
parfilename = fullfile(par.segParamsDir, [fname(1:end-6) '_PAR.txt']); % stores segmentation parameters

procStat = 0; %Should go through the last part of the code to be set as 1

% create directory if necessary
celldir = {par.curOutDirMarker, par.dispDir, par.segParamsDir};
for adir = celldir
    if ~isempty(adir{1})
        if ~exist(adir{1})
            mkdir(adir{1});
        end
    end
end
          
if isempty(par.curOutDirMarker)
    par.saveMarker = 0;
end



%% read in the image
[reader, dim] = getBioformatsReader(par.imgfile);
% extract image dimensions
dx = dim.Nx; %Image height
dy = dim.Ny; % Image width
NzOrig = dim.Nz; %Number of Z slices in lsm file
Nc = dim.Nc; %Number of channels

zinit = 1; %Set higher than one if you want to remove some of the lower slices
%Size of voxels in x, y and z in micro meter
voxelSizeX = dim.voxelSize(1);
voxelSizeY =  dim.voxelSize(2);
voxelSizeZ =  dim.voxelSize(3);

zFactor = round(voxelSizeZ/voxelSizeX); % zFactor-1 intermediate slice(s) will be generated
voxelSizeZ = voxelSizeZ/zFactor; % Update voxelSizeZ based on the number of intermediate slices
voxelSize = voxelSizeX * voxelSizeY * voxelSizeZ; %Volume of a voxel in cubic micro meter
Nz = (NzOrig-zinit+1) * zFactor - zFactor + 1; %Number of z slice after intepolation


paramsName = {'ChrVolMic', 'NumChr'};
paramsTable = cell2table(cell(1,length(paramsName)));
paramsTable.Properties.VariableNames = paramsName;

tpoint = tinit;

%% Reads input stack from chromosome
if Nc <= par.chrChanIdx
    error(sprintf('Chromosome channel %d have not been found for file %s\n',par.chrChanIdx, par.imgfile ))
end
chrStackOrig = getTPointBioFormats(reader, dim, tinit, par.chrChanIdx);

%% Processing file 
if length(par.imgfile) > 100
    disp(['Processing chromatin seg. : ...' par.imgfile(end-100:end)]);
else
    disp(['Processing chromatin seg. : ' par.imgfile]);
end
chrStack = ls_gen_intermediate_slices(chrStackOrig, zFactor); % this creates isotropic sampled image
clear chrStackOrig;

chrRegion = zeros(dx,dy,Nz); % Store the detected chromosome region
chrRegionCur1 = zeros(dx,dy,Nz);
chrRegionCur2 = zeros(dx,dy,Nz);
chrRegionPrev1 = zeros(dx,dy,Nz);
bordImg = zeros(dx,dy,Nz);
bordImg(1,:,:) = 1; bordImg(end,:,:) = 1;
bordImg(:,1,:) = 1; bordImg(:,end,:) = 1;
imgCentX = (dx+1)/2;
imgCentY = (dy+1)/2;
chromosomes = zeros(dx,dy,Nz);

%% Detection of chromosome
chrStack = imgaussian(chrStack, par.sigmaBlur, par.hSizeBlur);

% This part deals with inhomogeinity where intensity of chr of interest is dim
for iter = 1:par.inHomCount
    chrStack = ls_inhomogeneous_to_homogeneous(chrStack);
end

% find threshold in 3D and 2D 
[chrThresh3D, chrThresh2D] = ls_get_thresholds(chrStack);

% combine threhsolds
for i = 1:Nz
    chrRegion(:,:,i) = double(chrStack(:,:,i) >= (chrThresh3D * (1-par.bFactor2D) + chrThresh2D(i) * par.bFactor2D));
    chrRegion(:,:,i) = imfill(chrRegion(:,:,i), 'holes');
end

% connected components
threeDLabel = bwconncomp(chrRegion,18);
nCompI = threeDLabel.NumObjects;

%Check whether it went too low
if par.inHomCheck == 1 &&  nCompI >= par.maxNoInitObj && par.inHomCount >=1
    par.inHomCount = par.inHomCount - 1;
    par.inHomCheck = 0;
    fileID = fopen(parfilename,'a'); 
    fprintf(fileID,'%s %02d,   %s %05.f,   %s %04.f\n\r', 'tpoint:', tpoint, 'th3D:', chrThresh3D, 'nCompI:', nCompI);
    fclose(fileID);
    return
end

if (par.inHomCheck == 0 || tpoint == tinit) && nCompI >= par.maxNoInitObj
    [chrRegion, chrThresh3D, chrThresh2D, threeDLabel]= ls_adjust_thresholds_up(chrStack, chrThresh3D, chrThresh2D, maxNoInitObj, bFactor2D, threeDLabel);
end

nCompF = threeDLabel.NumObjects;
numPixels = cellfun(@numel,threeDLabel.PixelIdxList);
[initVol, idx] = max(numPixels);
if threeDLabel.NumObjects > par.maxNoInitObj * 0.10 %This is just to speed up the process
    chrRegion(:,:,:) = 0;
    for i=1:min(round(par.maxNoInitObj * 0.10), threeDLabel.NumObjects)
        [~, idx] = max(numPixels);
        chrRegion(threeDLabel.PixelIdxList{idx}) = 1;
        numPixels(idx) = 0;
    end
end

initVol = initVol * voxelSize;

if par.inHomCheck == 1 && tpoint == tinit && initVol < 3*par.mergeVol
    if par.inHomCount == 0
        fileID = fopen(parfilename,'w'); % name of file withoyt timepoint
    else
        fileID = fopen(parfilename,'a');
    end
    fprintf(fileID,'%s %02d,   %s %05.f,   %s %04.f\n\r', 'tpoint:', tpoint, 'th3D:', chrThresh3D, 'initVol:', initVol);
    fclose(fileID);
    par.inHomCount = par.inHomCount + 1;
    return
end

%fn_PC_DisplaySubplots(chrRegion, 'Detected chromosome', disRow, disCol, Nz, zFactor, 2);
%disp(sprintf('chrThresh3D = %g', chrThresh3D));

chrMarker = ls_split_connected_chromosomes(chrRegion, voxelSizeX, voxelSizeY, voxelSizeZ, bordImg, par.splitVol, par.mergeVol);
%displaySubplots(chrMarker, 'Chromosome - initial chrMarker after split', disRow, disCol, Nz, zFactor, 2);

chrMarker = ls_merge_small_regions(chrRegion, chrMarker, bordImg, voxelSizeX, voxelSizeY, voxelSizeZ, par.mergeVol);
%displaySubplots(chrMarker, 'chrMarker image - merged', disRow, disCol, Nz, zFactor, 2);

chrMarker = ls_remove_small_objects(chrMarker, 18, par.mergeVol, bordImg, voxelSize);
%Display chrMarker image - refined
%fn_PC_DisplaySubplots(chrMarker, 'chrMarker image - refined', disRow, disCol, Nz, zFactor, 2);

%%
threeDLabel = bwconncomp(chrMarker,18);
numPixels = cellfun(@numel,threeDLabel.PixelIdxList);
%Use location information to select the nucleus of interest
stat = regionprops(threeDLabel,'Centroid');

if tpoint == tinit
    %Normalize the number of pixels based on three properties
    [chrRegionCur1, curCentX1, curCentY1, curCentZ1] = ls_detect_chromosome_in_first_stack(threeDLabel, stat, imgCentX, imgCentY, numPixels, dx, dy, Nz);
    
    distCent = sqrt((imgCentX-curCentX1)^2 + (imgCentY-curCentY1)^2) * voxelSizeX;
    ncVolume1 = sum(sum(sum(chrRegionCur1))) * voxelSize;
    %disp(sprintf('distCent = %g', distCent));
    
    if par.inHomCount == 0 && par.inHomCheck == 1
        fileID = fopen(parfilename, 'w');
    else
        fileID = fopen(parfilename, 'a');
    end
    fprintf(fileID,'%s %02d,   %s %05.f,   %s %04.f   %s %04.f   %s %04.f\n\r', 'tpoint:', tpoint, 'th3D:', chrThresh3D, 'volChr: ', ncVolume1, 'nCompI: ', nCompI, 'nCompF: ', nCompF);
    fclose(fileID);
    
    if par.inHomCheck == 1
        if ncVolume1 > par.minProphaseVol * 2 && par.inHomCount >= 1
            par.inHomCount = par.inHomCount - 1;
            par.inHomCheck = 0;
            return
        elseif distCent > par.maxCentDisp || ncVolume1 < par.minProphaseVol
            par.inHomCount = par.inHomCount + 1;
            return
        else
            par.inHomCheck = 0;
        end
    end
    
    numChr = 1;
else
    if numChr == 1
        prevCentX2 = prevCentX1;
        prevCentY2 = prevCentY1;
    end
    [chrRegionCur1, chrRegionCur2, curCentX1, curCentY1, curCentZ1, curCentX2, curCentY2, curCentZ2, numChr] = ls_detect_chromosome_in_later_stacks(threeDLabel, stat, chrRegionPrev1, prevCentX1, prevCentY1, prevCentX2, prevCentY2, numPixels, numChr, voxelSizeX, voxelSizeY, voxelSizeZ);
    
    ncVolume1 = sum(sum(sum(chrRegionCur1)))*voxelSize;
    ncVolume2 = sum(sum(sum(chrRegionCur2)))*voxelSize;
    fileID = fopen([segParamsDir lsmFn(tpoint).name(1:end-10) '_PAR.txt'],'a');
    fprintf(fileID,'%s %02d,   %s %05.f,   %s %04.f   %s %04.f   %s %04.f\n\r', 'tpoint:', tpoint, 'th3D:', chrThresh3D, 'volChr: ', ncVolume1+ncVolume2, 'nCompI: ', nCompI, 'nCompF: ', nCompF);
    fclose(fileID);
end

prevCentX1 = curCentX1;
prevCentY1 = curCentY1;
%prevCentZ1 = curCentZ1;

chrRegionPrev1 = chrRegionCur1;

if numChr == 2
    prevCentX2 = curCentX2;
    prevCentY2 = curCentY2;
    %prevCentZ2 = curCentZ2;
    %chrRegionPrev2 = chrRegionCur2;
end

chromosomes(:,:,:) = 0;
chromosomes(chrRegionCur1(:,:,:) == 1) = 1;
chromosomes(chrRegionCur2(:,:,:) == 1) = 2;
if par.nRemLowerSlices > 0
    chromosomes(:,:,1:par.nRemLowerSlices*zFactor) = 0;
end
chrVolMic  = sum(sum(sum(chromosomes(:,:,:)>0))) * voxelSize;
savefile = fullfile(curOutDir, [fname '.mat']);
chrMass = ls_remove_intermediate_slices(chromosomes, zFactor);

save(savefile, 'chrMass', 'chrVolMic', 'numChr'); %Set 1 and 2 for first and second chromosome masses, res.

newTable = cell2table({chrVolMic,numChr});
newTable.Properties.VariableNames = paramsName;
paramsTable = cat(1,paramsTable,newTable);

savefile = fullfile(par.curOutDirMarker, [fname '.mat']);
if par.saveMarker == 1
    save(savefile, 'chrMarker', 'chromosomes', 'numChr', 'chrThresh3D', 'chrThresh2D');
end

% 8 corner points are set to 1 in order to keep effective volume same
% in all timepoints
if exist(par.dispDir)
    chrRegionCur1(1,1,1) = 1;     chrRegionCur1(1,1,end) = 1; chrRegionCur1(1,end,1)   = 1;
    chrRegionCur1(1,end,end) = 1; chrRegionCur1(end,1,1) = 1; chrRegionCur1(end,1,end) = 1;
    chrRegionCur1(end,end,1) = 1; chrRegionCur1(end,end,end) = 1;
    
    [X,Y,Z]=meshgrid((1:dy)*voxelSizeY,(1:dx)*voxelSizeX,(1:Nz)*voxelSizeZ);
    
    hVol = figure('Name', strcat('Frame: ', num2str(tpoint, '%0.3f')), 'NumberTitle','off', 'Visible', 'off');
    isosurface(X,Y,Z,chrRegionCur1,0.9);
    alpha(0.5)
    isosurface(X,Y,Z,chrRegionCur2,0.7);
    alpha(0.7)
    axis equal
    
    savefile = fullfile(par.dispDir, [fname '.jpg']);
    saveas(hVol, savefile, 'jpg');
    delete(hVol);
end


clear maskedchrStack;
clear wsLabel;
clear distImage;
clear chrMarker;
clear X;
clear Y;
clear Z;
clear contourCRegionThreeD;
clear dumCol;
clear hist;
close all;

paramsFn = fullfile(curOutDir,  par.exParamsFn);
paramsTable(1,:) = [];
writetable(paramsTable,paramsFn,'Delimiter','\t');
procStat = 1;
end