function [cRegion] = ls_process_upper_cell_region(cRegion, zgCh, Nz, voxelSizeZ)
%Process upper slices of cell region to correct segmentation error
se = [0 1 0;1 1 1; 0 1 0];
stIdx = round((zgCh+1.5)/voxelSizeZ);
refSlice = cRegion(:,:,stIdx);
times = 6;
count = times;
for i = stIdx+1:Nz
    cRegion(:,:,i) = double(and(refSlice, cRegion(:,:,i)));
    count = max(count - 1, 1);
    if count == 1
        refSlice = imerode(cRegion(:,:,i),se);
        times = max(times - 1, 1);
        count = times;
    else
        refSlice = cRegion(:,:,i);
    end
end
end

