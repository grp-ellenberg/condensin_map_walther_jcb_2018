# -*- coding: utf-8 -*-
"""
Created on Mon Jul 17 14:53:00 2017
Script to move files with a certain ending to a global directory
@author: Antonio Politi
"""

import os
import re
import glob
import fnmatch
import shutil

expdirs = ['20170502_CondensinClones','20170523_CondensinClones_ExpB', '20170524_CondensinClones_ExpC', '20170526_CondensinClones_ExpD_KIF4A', 
        '20170526_CondensinClones_ExpD_TOP2A']
        
expdirs =['20170522_CondensinClones_ExpA']
#for expdir  in expdirs:        
#    indir = ['Z:\\Nike\\CondensinsClones_FCS_DiffC_Inter_Meta\\' + expdir + '\\Calibration\\POI_meta\\']
#    outdir = ['Z:\\Nike\\CondensinsClones_FCS_DiffC_Inter_Meta\\Results\\' + expdir + '\\Calibration\\POI_meta\\']
#    root, dirnames, fnames = os.walk(indir[0]).next()
#    print(dirnames)
#    pname = [y for y in dirnames if ('gfp' in y) or ('halo' in y)]
#    
#    indirs =  [indir[0] + x for x in pname]
#    outdirs = [outdir[0] + x for x in pname]
#    
#    for i, indir in enumerate(indirs):
#        print(indir)
#        if not os.path.exists(outdirs[i]):
#            os.makedirs(outdirs[i])
#        
#        imgfiles = list()
#        for root, dirnames, filenames in os.walk(indir):
#                for filename in fnmatch.filter(filenames, 'TR1_2_*.lsm'):
#                    # duplicate entry
#                    if filename in imgfiles:
#                        print('duplicate')
#                        shutil.copy(os.path.join(root, filename),os.path.join(outdirs[i], 'rnd2_' + filename) )
#                    else:
#                        shutil.copy(os.path.join(root, filename),os.path.join(outdirs[i], filename) )
#                    imgfiles.append(filename)
                    
# create table file 

for expdir  in expdirs:        
    indirg = ['Z:\\Nike\\CondensinsClones_FCS_DiffC_Inter_Meta\\Results\\' + expdir + '\\Calibration\\POI_meta\\']
    fid = open(os.path.join(indirg[0], 'qc_file.txt'), 'w')
    fid.close()
    fid = open(os.path.join(indirg[0], 'qc_file.txt'), 'a')
    fid.write('path\tfname\tqc\n')
    #fid.close()
   
    root, dirnames, fnames = os.walk(indirg[0]).next()
    pname = [y for y in dirnames if ('gfp' in y) or ('halo' in y)]
    indirs =  [indirg[0] + x for x in pname]
    for i, indir in enumerate(indirs):
        filenames = glob.glob(os.path.join(indir, '*.lsm'))
        for filen in sort(filenames):
            path = os.path.basename(os.path.dirname(filen))
            name = os.path.basename(filen)
            print('%s %s' % (path, name))
            fid.write('%s\t%s\t-1\n' % (path, name))
    fid.close()