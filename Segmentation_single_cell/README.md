# MATLAB Code for segmenting chromosome and cell boundary
Chromosomes are recognized by a DNA stain. Cell boundary is identified by negative staining.

The code is meant to process single cells using a similar segmentation pipeline as for the MitoSys project.

> **An experimental and computational framework to build a dynamic protein atlas of human cell 
division** Yin Cai, M. Julius Hossain, 
Jean-Karim Heriche, Antonio Z. Politi, Nike Walther, Birgit Koch, Malte 
Wachsmuth, Bianca Nijmeijer, Moritz Kueblbeck, Marina Martinic-Kavur, Rene Ladurner, Jan-Michael Peters, and Jan Ellenberg (2017) bioRxiv 227751, doi: https://doi.org/10.1101/227751 

## Usage
To run the example data set (a cell in metaphase), execute in a MATLAB command window the script `src\batch_segment.m`. In the folder `results`, the script creates several files from the segmentation pipeline and a jpg file for the user to quality control the results.

<p align = "center">
<img src="./resources/example_data_segmentation.jpg" width = "500px">
<p/>
The file `results\image_name.mat` contains the binary masks.

* `cellMass` binary mask of outer cell boundary
* `chrMass` binary mask of chromosomes
* `numChr` the number of chromosomes
* `chrVolMic` cell volume  in um3
* `cellVolMic` chromosome volume in um3

## Author

M. Julius Hossain, EMBL, Heidelberg
